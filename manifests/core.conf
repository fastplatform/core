### Global configs

# Domain
fastplatform.global.domain=
# web application URL
fastplatform.global.web-app-url=

### Main databases

## 'external' Postgres database
# Backrest repository
fastplatform.core.db.external.backrest.storage.class=standard
fastplatform.core.db.external.backrest.storage.size=1Gi
# Backup
fastplatform.core.db.external.backup.repo-path=reviews/core/external/external-backrest-shared-repo
fastplatform.core.db.external.backup.s3.access-key.b64=
fastplatform.core.db.external.backup.s3.bucket-name=
fastplatform.core.db.external.backup.s3.ca-cert.b64=
fastplatform.core.db.external.backup.s3.endpoint=
fastplatform.core.db.external.backup.s3.region=
fastplatform.core.db.external.backup.s3.secret-key.b64=
fastplatform.core.db.external.backup.s3.uri-style=host
fastplatform.core.db.external.backup.s3.verify-tls=true
fastplatform.core.db.external.backup.storage-types=posix
# Docker image
fastplatform.core.db.external.image.prefix=registry.developers.crunchydata.com/crunchydata
fastplatform.core.db.external.image.tag=centos8-12.7-2.5-4.7.0
# Pooler
fastplatform.core.db.external.pooler.resources.requests.cpu=100m
fastplatform.core.db.external.pooler.resources.requests.memory=200Mi
# Primary
fastplatform.core.db.external.primary.storage.class=standard
fastplatform.core.db.external.primary.storage.size=20Gi
# Replica
fastplatform.core.db.external.replica.count=0
fastplatform.core.db.external.replica.storage.class=standard
fastplatform.core.db.external.replica.storage.size=20Gi
# Resources
fastplatform.core.db.external.resources.requests.cpu=100m
fastplatform.core.db.external.resources.requests.memory=500Mi
# Users
fastplatform.core.db.external.user.fastplatform.password.b64=
fastplatform.core.db.external.user.postgres.password.b64=

## 'fastplatform' Postgres database
# Backrest repository
fastplatform.core.db.fastplatform.backrest.storage.class=standard
fastplatform.core.db.fastplatform.backrest.storage.size=1Gi
# Backup
fastplatform.core.db.fastplatform.backup.repo-path=reviews/core/fastplatform/fastplatform-backrest-shared-repo
fastplatform.core.db.fastplatform.backup.s3.access-key.b64=
fastplatform.core.db.fastplatform.backup.s3.bucket-name=
fastplatform.core.db.fastplatform.backup.s3.ca-cert.b64=
fastplatform.core.db.fastplatform.backup.s3.endpoint=
fastplatform.core.db.fastplatform.backup.s3.region=
fastplatform.core.db.fastplatform.backup.s3.secret-key.b64=
fastplatform.core.db.fastplatform.backup.s3.uri-style=host
fastplatform.core.db.fastplatform.backup.s3.verify-tls=true
fastplatform.core.db.fastplatform.backup.storage-types=posix
# Docker image
fastplatform.core.db.fastplatform.image.prefix=registry.developers.crunchydata.com/crunchydata
fastplatform.core.db.fastplatform.image.tag=centos8-12.7-2.5-4.7.0
# Pooler
fastplatform.core.db.fastplatform.pooler.resources.requests.cpu=100m
fastplatform.core.db.fastplatform.pooler.resources.requests.memory=200Mi
# Primary
fastplatform.core.db.fastplatform.primary.storage.class=standard
fastplatform.core.db.fastplatform.primary.storage.size=20Gi
# Replica
fastplatform.core.db.fastplatform.replica.count=0
fastplatform.core.db.fastplatform.replica.storage.class=standard
fastplatform.core.db.fastplatform.replica.storage.size=20Gi
# Resources
fastplatform.core.db.fastplatform.resources.requests.cpu=100m
fastplatform.core.db.fastplatform.resources.requests.memory=500Mi
# Users
fastplatform.core.db.fastplatform.user.fastplatform.password.b64=
fastplatform.core.db.fastplatform.user.postgres.password.b64=


### Services

## api-gateway/fastplatform
# Hasura
fastplatform.core.services.api-gateway.fastplatform.hasura-admin-key.b64=
fastplatform.core.services.api-gateway.fastplatform.hasura-enable-allow-list=false
fastplatform.core.services.api-gateway.fastplatform.hasura-enable-console=true
# Hasura databases
fastplatform.core.services.api-gateway.fastplatform.db.default.hasura-pg-max-connections=10
fastplatform.core.services.api-gateway.fastplatform.db.default.hasura-pg-idle-timeout=180
fastplatform.core.services.api-gateway.fastplatform.db.default.hasura-pg-conn-lifetime=600
fastplatform.core.services.api-gateway.fastplatform.db.external.hasura-pg-max-connections=10
fastplatform.core.services.api-gateway.fastplatform.db.external.hasura-pg-idle-timeout=180
fastplatform.core.services.api-gateway.fastplatform.db.external.hasura-pg-conn-lifetime=600
# Knative
fastplatform.core.services.api-gateway.fastplatform.knative.public-name=api-gateway-fastplatform
fastplatform.core.services.api-gateway.fastplatform.knative.minscale=1
fastplatform.core.services.api-gateway.fastplatform.knative.target=30
# Resources
fastplatform.core.services.api-gateway.fastplatform.resources.requests.cpu=100m
fastplatform.core.services.api-gateway.fastplatform.resources.requests.memory=500Mi

## api-gateway/fastplatform-out
# Hasura
fastplatform.core.services.api-gateway.fastplatform-out.hasura-admin-key.b64=
fastplatform.core.services.api-gateway.fastplatform-out.hasura-enable-console=false
# Knative
fastplatform.core.services.api-gateway.fastplatform-out.knative.public-name=api-gateway-fastplatform-out
fastplatform.core.services.api-gateway.fastplatform-out.knative.minscale=1
fastplatform.core.services.api-gateway.fastplatform-out.knative.target=50
# Resources
fastplatform.core.services.api-gateway.fastplatform-out.resources.requests.cpu=100m
fastplatform.core.services.api-gateway.fastplatform-out.resources.requests.memory=500Mi

## event/farm
# api-gateway service key
fastplatform.core.services.event.farm.api-gateway-service-key.b64=
fastplatform.core.services.event.farm.api-gateway-timeout=120
fastplatform.core.services.event.farm.surface-water-search-distance=0.0002
fastplatform.core.services.event.farm.water-course-search-distance=0.0002
fastplatform.core.services.event.farm.public-soil-site-search-distance=0.15
# Knative
fastplatform.core.services.event.farm.knative.public-name=event-farm
fastplatform.core.services.event.farm.knative.minscale=1
fastplatform.core.services.event.farm.knative.target=30
# Logging
fastplatform.core.services.event.farm.log-level=info
# OpenTelemetry
fastplatform.core.services.event.farm.opentelemetry-exporter-zipkin-endpoint=
fastplatform.core.services.event.farm.opentelemetry-sampling-ratio=0.1
fastplatform.core.services.event.farm.opentelemetry-service-name=event-farm
# Resources
fastplatform.core.services.event.farm.resources.requests.cpu=100m
fastplatform.core.services.event.farm.resources.requests.memory=500Mi

## event/messaging
# api-gateway service key
fastplatform.core.services.event.messaging.api-gateway-service-key.b64=
# Knative
fastplatform.core.services.event.messaging.knative.public-name=event-messaging
fastplatform.core.services.event.messaging.knative.minscale=1
fastplatform.core.services.event.messaging.knative.target=30
# Locale
fastplatform.core.services.event.messaging.locale=en
# Logging
fastplatform.core.services.event.messaging.log-level=info
# OpenTelemetry
fastplatform.core.services.event.messaging.opentelemetry-exporter-zipkin-endpoint=
fastplatform.core.services.event.messaging.opentelemetry-sampling-ratio=0.1
fastplatform.core.services.event.messaging.opentelemetry-service-name=event-farm
# Resources
fastplatform.core.services.event.messaging.resources.requests.cpu=50m
fastplatform.core.services.event.messaging.resources.requests.memory=200Mi
# SMTP
fastplatform.core.services.event.messaging.smtp-host=
fastplatform.core.services.event.messaging.smtp-login=
fastplatform.core.services.event.messaging.smtp-password.b64=
fastplatform.core.services.event.messaging.smtp-port=
fastplatform.core.services.event.messaging.smtp-sender=do-not-reply@fastplatform.eu

## extra/pgo-backup
# CronJob
fastplatform.core.services.extra.pgo-backup.cronjob.differential.retention=7
fastplatform.core.services.extra.pgo-backup.cronjob.differential.schedule=0 3 * * *
fastplatform.core.services.extra.pgo-backup.cronjob.full.retention=4
fastplatform.core.services.extra.pgo-backup.cronjob.full.schedule=0 1 * * 1
fastplatform.core.services.extra.pgo-backup.cronjob.incremental.schedule=0 0,4-23 * * *
# Pgcluster to backup
fastplatform.core.services.extra.pgo-backup.pgclusters=
# Resources
fastplatform.core.services.extra.pgo-backup.resources.cpu=100m
fastplatform.core.services.extra.pgo-backup.resources.memory=200Mi

## mapproxy/api
# api-gateway service key
fastplatform.core.services.mapproxy.api.api-gateway-service-key.b64=
fastplatform.core.services.mapproxy.api.api-gateway-timeout=120
# Configs
fastplatform.core.services.mapproxy.api.conf-cache-s3-bucket-name=
fastplatform.core.services.mapproxy.api.conf-cache-s3-location=cache/mapproxy
fastplatform.core.services.mapproxy.api.conf-cache-s3-url=
fastplatform.core.services.mapproxy.api.conf-cache-type=s3
fastplatform.core.services.mapproxy.api.conf-default-proj-epsg-code=
fastplatform.core.services.mapproxy.api.conf-default-proj-bbox=
# Knative
fastplatform.core.services.mapproxy.api.knative.public-name=mapproxy-api
fastplatform.core.services.mapproxy.api.knative.minscale=1
fastplatform.core.services.mapproxy.api.knative.target=30
# Logging
fastplatform.core.services.mapproxy.api.log-level=info
# OpenTelemetry
fastplatform.core.services.mapproxy.api.opentelemetry-exporter-zipkin-endpoint=
fastplatform.core.services.mapproxy.api.opentelemetry-sampling-ratio=0.1
fastplatform.core.services.mapproxy.api.opentelemetry-service-name=mapproxy-api
# Resources
fastplatform.core.services.mapproxy.api.resources.requests.cpu=50m
fastplatform.core.services.mapproxy.api.resources.requests.memory=200Mi

## mapproxy/server
# Configs
fastplatform.core.services.mapproxy.server.conf-cache-s3-access-key=
fastplatform.core.services.mapproxy.server.conf-cache-s3-secret-key.b64=
# Knative
fastplatform.core.services.mapproxy.server.knative.public-name=map
fastplatform.core.services.mapproxy.server.knative.maxscale=5
fastplatform.core.services.mapproxy.server.knative.minscale=1
fastplatform.core.services.mapproxy.server.knative.target=10
# Resources
fastplatform.core.services.mapproxy.server.resources.requests.cpu=100m
fastplatform.core.services.mapproxy.server.resources.requests.memory=500Mi

## mobile/push-notification
# APNs
fastplatform.core.services.mobile.push-notification.apns-key-id.b64=
fastplatform.core.services.mobile.push-notification.apns-team-id.b64=
fastplatform.core.services.mobile.push-notification.apns-encryption-key.b64=
# FCM
fastplatform.core.services.mobile.push-notification.fcm-api-key.b64=
# Knative
fastplatform.core.services.mobile.push-notification.knative.public-name=mobile-push-notification
fastplatform.core.services.mobile.push-notification.knative.minscale=1
fastplatform.core.services.mobile.push-notification.knative.target=30
# Redis
fastplatform.core.services.mobile.push-notification.redis.password.b64=
fastplatform.core.services.mobile.push-notification.redis.master.storage.class=standard
fastplatform.core.services.mobile.push-notification.redis.master.storage.size=1Gi
# Resources
fastplatform.core.services.mobile.push-notification.resources.requests.cpu=50m
fastplatform.core.services.mobile.push-notification.resources.requests.memory=100Mi

## web/authentication
# api-gateway service key to authorize
fastplatform.core.services.web.authentication.api-gateway-service-key.b64=
# Django
fastplatform.core.services.web.authentication.django-debug=false
fastplatform.core.services.web.authentication.django-log-level=info
# Knative
fastplatform.core.services.web.authentication.knative.public-name=web-authentication
fastplatform.core.services.web.authentication.knative.minscale=1
fastplatform.core.services.web.authentication.knative.target=30
# Memcached
fastplatform.core.services.web.authentication.memcached.password.b64=
fastplatform.core.services.web.authentication.memcached.storage.class=standard
fastplatform.core.services.web.authentication.memcached.storage.size=1Gi
fastplatform.core.services.web.authentication.memcached.user=memcached
# OpenTelemetry
fastplatform.core.services.web.authentication.opentelemetry-exporter-zipkin-endpoint=
fastplatform.core.services.web.authentication.opentelemetry-sampling-ratio=0.1
fastplatform.core.services.web.authentication.opentelemetry-service-name=web-authentication
# Resources
fastplatform.core.services.web.authentication.resources.requests.cpu=100m
fastplatform.core.services.web.authentication.resources.requests.memory=500Mi

## web/backend
# addons
fastplatform.core.services.web.backend.addons-callback-jwt-signing-algorithm=HS256
fastplatform.core.services.web.backend.addons-callback-token-validity-period=300
# api-gateway service key
fastplatform.core.services.web.backend.api-gateway-service-key.b64=
# Django
fastplatform.core.services.web.backend.django-axes-cooloff-time=30
fastplatform.core.services.web.backend.django-axes-failure-limit=3
fastplatform.core.services.web.backend.django-axes-lock-out-by-combination-user-and-ip=true
fastplatform.core.services.web.backend.django-axes-reset-on-success=true
fastplatform.core.services.web.backend.django-csrf-cookie-secure=true
fastplatform.core.services.web.backend.django-databases-disable-server-side-cursors=true
fastplatform.core.services.web.backend.django-databases-postgres-conn-max-age=60
fastplatform.core.services.web.backend.django-debug-toolbar=false
fastplatform.core.services.web.backend.django-debug=false
fastplatform.core.services.web.backend.django-log-level=info
fastplatform.core.services.web.backend.django-secret-key.b64=
fastplatform.core.services.web.backend.django-secure-hsts-seconds=0
fastplatform.core.services.web.backend.django-session-cookie-age=86400
fastplatform.core.services.web.backend.django-session-cookie-secure=true
fastplatform.core.services.web.backend.django-storage-s3-access-key=
fastplatform.core.services.web.backend.django-storage-s3-bucket-name=
fastplatform.core.services.web.backend.django-storage-s3-location=media/reviews/core/default
fastplatform.core.services.web.backend.django-storage-s3-secret-key.b64=
fastplatform.core.services.web.backend.django-storage-s3-url=
fastplatform.core.services.web.backend.django-storage-static-s3-access-key=
fastplatform.core.services.web.backend.django-storage-static-s3-bucket-name=
fastplatform.core.services.web.backend.django-storage-static-s3-cache-max-age=86400
fastplatform.core.services.web.backend.django-storage-static-s3-is-gzipped=true
fastplatform.core.services.web.backend.django-storage-static-s3-location=static/reviews/core/default
fastplatform.core.services.web.backend.django-storage-static-s3-querystring-auth=false
fastplatform.core.services.web.backend.django-storage-static-s3-secret-key.b64=
fastplatform.core.services.web.backend.django-storage-static-s3-url=
fastplatform.core.services.web.backend.django-storage-upload-max-memory-size=5242880
# Features
fastplatform.core.services.web.backend.enable-broadcast=true
fastplatform.core.services.web.backend.enable-custom-fertilizer=true
fastplatform.core.services.web.backend.enable-external-agri-plot=true
fastplatform.core.services.web.backend.enable-external-management-restriction-or-regulation_zone=true
fastplatform.core.services.web.backend.enable-external-protected-site=true
fastplatform.core.services.web.backend.enable-external-soil-site=true
fastplatform.core.services.web.backend.enable-external-surface-water=true
fastplatform.core.services.web.backend.enable-external-water-course=true
fastplatform.core.services.web.backend.enable-geotagged-photo=true
fastplatform.core.services.web.backend.enable-insert-demo-holding-from-admin=true
fastplatform.core.services.web.backend.enable-multiple-regions=true
fastplatform.core.services.web.backend.enable-registered-fertilizer=true
fastplatform.core.services.web.backend.enable-soil-site=true
fastplatform.core.services.web.backend.enable-sync-holding-from-admin=false
fastplatform.core.services.web.backend.enable-sync-holdings-from-admin=false
fastplatform.core.services.web.backend.enable-ticket=true
fastplatform.core.services.web.backend.enable-user-preferred-languages=true
# Knative
fastplatform.core.services.web.backend.knative.public-name=web-backend
fastplatform.core.services.web.backend.knative.minscale=1
fastplatform.core.services.web.backend.knative.target=30
# Locale
fastplatform.core.services.web.backend.locale=en
# Memcached
fastplatform.core.services.web.backend.memcached.password.b64=
fastplatform.core.services.web.backend.memcached.storage.class=standard
fastplatform.core.services.web.backend.memcached.storage.size=1Gi
fastplatform.core.services.web.backend.memcached.user=memcached
# OpenTelemetry
fastplatform.core.services.web.backend.opentelemetry-exporter-zipkin-endpoint=
fastplatform.core.services.web.backend.opentelemetry-sampling-ratio=0.1
fastplatform.core.services.web.backend.opentelemetry-service-name=web-backend
# Resources
fastplatform.core.services.web.backend.resources.requests.cpu=100m
fastplatform.core.services.web.backend.resources.requests.memory=500Mi
# SMTP
fastplatform.core.services.web.backend.smtp-host=
fastplatform.core.services.web.backend.smtp-login=
fastplatform.core.services.web.backend.smtp-password.b64=
fastplatform.core.services.web.backend.smtp-port=
fastplatform.core.services.web.backend.smtp-sender=do-not-reply@fastplatform.eu
# Time Zone
fastplatform.core.services.web.backend.time-zone=UTC

## web/media
# Knative
fastplatform.core.services.web.media.knative.public-name=web-media
fastplatform.core.services.web.media.knative.minscale=1
fastplatform.core.services.web.media.knative.target=30
# Resources
fastplatform.core.services.web.media.resources.requests.cpu=100m
fastplatform.core.services.web.media.resources.requests.memory=500Mi
# S3
fastplatform.core.services.web.media.s3-url=

## web/pdf-generator
# Context
fastplatform.core.services.web.pdf-generator.context.european-commission-name=
fastplatform.core.services.web.pdf-generator.context.european-commission-logo.b64=
fastplatform.core.services.web.pdf-generator.context.region-name=
fastplatform.core.services.web.pdf-generator.context.region-logo.b64=
# Logging
fastplatform.core.services.web.pdf-generator.log-level=info
# Knative
fastplatform.core.services.web.pdf-generator.knative.public-name=web-pdf-generator
fastplatform.core.services.web.pdf-generator.knative.minscale=1
fastplatform.core.services.web.pdf-generator.knative.target=30
# OpenTelemetry
fastplatform.core.services.web.pdf-generator.opentelemetry-exporter-zipkin-endpoint=
fastplatform.core.services.web.pdf-generator.opentelemetry-sampling-ratio=0.1
fastplatform.core.services.web.pdf-generator.opentelemetry-service-name=web-pdf-generator
# Resources
fastplatform.core.services.web.pdf-generator.resources.requests.cpu=100m
fastplatform.core.services.web.pdf-generator.resources.requests.memory=500Mi
# SMTP
fastplatform.core.services.web.pdf-generator.smtp-host=
fastplatform.core.services.web.pdf-generator.smtp-login=
fastplatform.core.services.web.pdf-generator.smtp-password.b64=
fastplatform.core.services.web.pdf-generator.smtp-port=
fastplatform.core.services.web.pdf-generator.smtp-sender=do-not-reply@fastplatform.eu
# Time Zone
fastplatform.core.services.web.pdf-generator.time-zone=UTC

## web/vector-tiles/external
# Knative
fastplatform.core.services.web.vector-tiles.external.knative.public-name=web-vector-tiles-external-proxied-not-public
fastplatform.core.services.web.vector-tiles.external.knative.minscale=1
fastplatform.core.services.web.vector-tiles.external.knative.target=50
# Resources
fastplatform.core.services.web.vector-tiles.external.resources.requests.cpu=100m
fastplatform.core.services.web.vector-tiles.external.resources.requests.memory=200Mi
# tileserv
fastplatform.core.services.web.vector-tiles.external.tileserv-db-pool-max-conn-lifetime=5m
fastplatform.core.services.web.vector-tiles.external.tileserv-db-pool-max-conns=2
fastplatform.core.services.web.vector-tiles.external.tileserv-db-timeout=10
fastplatform.core.services.web.vector-tiles.external.tileserv-default-resolution=4096
fastplatform.core.services.web.vector-tiles.external.tileserv-default-buffer=256
fastplatform.core.services.web.vector-tiles.external.tileserv-max-features-per-tile=10000
fastplatform.core.services.web.vector-tiles.external.tileserv-default-min-zoom=0
fastplatform.core.services.web.vector-tiles.external.tileserv-default-max-zoom=22

## web/vector-tiles/external-cache
# Knative
fastplatform.core.services.web.vector-tiles.external-cache.knative.public-name=web-vector-tiles-external
fastplatform.core.services.web.vector-tiles.external-cache.knative.minscale=1
fastplatform.core.services.web.vector-tiles.external-cache.knative.target=500
# Resources
fastplatform.core.services.web.vector-tiles.external-cache.resources.requests.cpu=100m
fastplatform.core.services.web.vector-tiles.external-cache.resources.requests.memory=200Mi

## web/vector-tiles/fastplatform
# Knative
fastplatform.core.services.web.vector-tiles.fastplatform.knative.public-name=web-vector-tiles-fastplatform
fastplatform.core.services.web.vector-tiles.fastplatform.knative.minscale=1
fastplatform.core.services.web.vector-tiles.fastplatform.knative.target=50
# Resources
fastplatform.core.services.web.vector-tiles.fastplatform.resources.requests.cpu=100m
fastplatform.core.services.web.vector-tiles.fastplatform.resources.requests.memory=200Mi
# tileserv
fastplatform.core.services.web.vector-tiles.fastplatform.tileserv-db-pool-max-conn-lifetime=5m
fastplatform.core.services.web.vector-tiles.fastplatform.tileserv-db-pool-max-conns=2
fastplatform.core.services.web.vector-tiles.fastplatform.tileserv-db-timeout=10
fastplatform.core.services.web.vector-tiles.fastplatform.tileserv-default-resolution=4096
fastplatform.core.services.web.vector-tiles.fastplatform.tileserv-default-buffer=256
fastplatform.core.services.web.vector-tiles.fastplatform.tileserv-max-features-per-tile=10000
fastplatform.core.services.web.vector-tiles.fastplatform.tileserv-default-min-zoom=0
fastplatform.core.services.web.vector-tiles.fastplatform.tileserv-default-max-zoom=22
