��          �               ,  #   -  I   Q  3   �  @   �  ,     4   =  A   r  5   �  &   �  A     4   S     �  	   �     �     �     �  �  �  (   [  K   �  0   �  :     ,   <  4   i  B   �  6   �       ?   7  4   w  
   �  	   �     �     �     �   %(user_display_name)s sent a photo. %(user_link)s created ticket %(ticket_link)s for holding %(holding_link)s %(user_link)s replied to the ticket %(ticket_link)s %(user_link)s replied to the ticket %(ticket_link)s with a photo %(user_name)s created ticket %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Request %(ticket_link)s status changed to %(status)s. The request status has been changed to This email has been generated automatically, please do not reply. Ticket %(ticket_link)s status changed to %(status)s. closed duplicate open reopened resolved Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-25 19:03+0100
PO-Revision-Date: 2021-12-14 09:36+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: sk
Language-Team: sk <LL@li.org>
Plural-Forms: nplurals=3; plural=((n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 %(user_display_name)s poslal fotografiu. %(user_link)s vytvoril lístok %(ticket_link)s pre holding %(holding_link)s %(user_link)s odpovedal na tiket %(ticket_link)s %(user_link)s odpovedal na tip %(ticket_link)s fotografiou %(user_name)s vytvoril tiket %(ticket_link)s %(user_name)s odpovedal na žiadosť %(ticket_link)s %(user_name)s odpovedal na žiadosť %(ticket_link)s s fotografiou Stav žiadosti %(ticket_link)s zmenený na %(status)s. Stav žiadosti bol zmenený na Tento e-mail bol vygenerovaný automaticky, neodpovedajte naň. Stav tiketu %(ticket_link)s sa zmenil na %(status)s. zatvorené duplikát otvoriť znovu otvorené vyriešené 