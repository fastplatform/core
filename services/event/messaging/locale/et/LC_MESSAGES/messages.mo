��          �               ,  #   -  I   Q  3   �  @   �  ,     4   =  A   r  5   �  &   �  A     4   S     �  	   �     �     �     �  �  �  "   >  D   a  -   �  4   �  )   	  /   3  6   c  .   �     �  A   �  ,   !     N  	   V     `  
   g  
   r   %(user_display_name)s sent a photo. %(user_link)s created ticket %(ticket_link)s for holding %(holding_link)s %(user_link)s replied to the ticket %(ticket_link)s %(user_link)s replied to the ticket %(ticket_link)s with a photo %(user_name)s created ticket %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Request %(ticket_link)s status changed to %(status)s. The request status has been changed to This email has been generated automatically, please do not reply. Ticket %(ticket_link)s status changed to %(status)s. closed duplicate open reopened resolved Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-25 19:03+0100
PO-Revision-Date: 2020-12-04 14:35+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: et
Language-Team: et <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 %(user_display_name)s saatis foto. %(user_link)s lõi pileti %(ticket_link)s üksusele %(holding_link)s %(user_link)s vastas piletile %(ticket_link)s %(user_link)s vastas piletile %(ticket_link)s fotoga %(user_name)s lõi pileti %(ticket_link)s %(user_name)s vastas taotlusele %(ticket_link)s %(user_name)s vastas taotlusele %(ticket_link)s fotoga Taotluse %(ticket_link)s uus olek: %(status)s. Taotluse uus olek on: See on automaatselt koostatud kiri. Palun ärge vastake sellele.  Pileti %(ticket_link)s uus olek: %(status)s. suletud duplikaat avatud taasavatud lahendatud 