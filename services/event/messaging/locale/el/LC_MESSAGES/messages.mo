��          �               ,  #   -  I   Q  3   �  @   �  ,     4   =  A   r  5   �  &   �  A     4   S     �  	   �     �     �     �  �  �  A   >  x   �  H   �  i   B  G   �  B   �  c   7  ]   �  L   �  t   F  _   �     	     *	     =	  %   L	     r	   %(user_display_name)s sent a photo. %(user_link)s created ticket %(ticket_link)s for holding %(holding_link)s %(user_link)s replied to the ticket %(ticket_link)s %(user_link)s replied to the ticket %(ticket_link)s with a photo %(user_name)s created ticket %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Request %(ticket_link)s status changed to %(status)s. The request status has been changed to This email has been generated automatically, please do not reply. Ticket %(ticket_link)s status changed to %(status)s. closed duplicate open reopened resolved Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-25 19:03+0100
PO-Revision-Date: 2021-12-14 09:36+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: el
Language-Team: el <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 %(user_display_name)s έστειλε μια φωτογραφία. %(user_link)s δημιούργησε εισιτήριο %(ticket_link)s για εκμετάλλευση %(holding_link)s %(user_link)s απάντησε στο εισιτήριο %(ticket_link)s %(user_link)s απάντησε στο εισιτήριο %(ticket_link)s με μια φωτογραφία %(user_name)s δημιούργησε εισιτήριο %(ticket_link)s %(user_name)s απάντησε στο αίτημα %(ticket_link)s %(user_name)s απάντησε στο αίτημα %(ticket_link)s με μια φωτογραφία Η κατάσταση του αιτήματος %(ticket_link)s άλλαξε σε %(status)s. Η κατάσταση του αιτήματος έχει αλλάξει σε Αυτό το email δημιουργήθηκε αυτόματα, παρακαλούμε μην απαντήσετε. Η κατάσταση του εισιτηρίου %(ticket_link)s άλλαξε σε %(status)s. κλειστό διπλότυπο ανοιχτό επαναλειτούργησε το επιλυμένο 