��          �               ,  #   -  I   Q  3   �  @   �  ,     4   =  A   r  5   �  &   �  A     4   S     �  	   �     �     �     �  �  �  *   =  W   h  6   �  7   �  2   /  6   b  A   �  F   �  *   "  >   M  F   �     �  	   �     �  	   �     �   %(user_display_name)s sent a photo. %(user_link)s created ticket %(ticket_link)s for holding %(holding_link)s %(user_link)s replied to the ticket %(ticket_link)s %(user_link)s replied to the ticket %(ticket_link)s with a photo %(user_name)s created ticket %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Request %(ticket_link)s status changed to %(status)s. The request status has been changed to This email has been generated automatically, please do not reply. Ticket %(ticket_link)s status changed to %(status)s. closed duplicate open reopened resolved Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-25 19:03+0100
PO-Revision-Date: 2021-11-30 17:32+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 %(user_display_name)s a envoyé une photo. %(user_link)s a envoyé la demande %(ticket_link)s pour l'exploitation %(holding_link)s %(user_link)s a répondu à la demande %(ticket_link)s a répondu à la demande %(ticket_link)s avec une photo %(user_name)s a envoyé la demande %(ticket_link)s %(user_name)s a répondu à la demande %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Le statut de la demande %(ticket_link)s a été changé en %(status)s. La statut de la demande a été changé en Cet email a été généré automatiquement, ne pas répondre. Le statut de la demande %(ticket_link)s a été changé en %(status)s. fermé dupliqué ouvert réouvert résolu 