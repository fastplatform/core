��          �               ,  #   -  I   Q  3   �  @   �  ,     4   =  A   r  5   �  &   �  A     4   S     �  	   �     �     �     �  �  �  ,   n  R   �  1   �  A      -   b  3   �  C   �  @     $   I  D   n  =   �  	   �     �       	           %(user_display_name)s sent a photo. %(user_link)s created ticket %(ticket_link)s for holding %(holding_link)s %(user_link)s replied to the ticket %(ticket_link)s %(user_link)s replied to the ticket %(ticket_link)s with a photo %(user_name)s created ticket %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s %(user_name)s replied to the request %(ticket_link)s with a photo Request %(ticket_link)s status changed to %(status)s. The request status has been changed to This email has been generated automatically, please do not reply. Ticket %(ticket_link)s status changed to %(status)s. closed duplicate open reopened resolved Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-02-25 19:03+0100
PO-Revision-Date: 2021-12-14 09:36+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ro
Language-Team: ro <LL@li.org>
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 %(user_display_name)s a trimis o fotografie. %(user_link)s a creat biletul %(ticket_link)s pentru exploatație %(holding_link)s %(user_link)s a răspuns la bilet %(ticket_link)s %(user_link)s a răspuns la bilet %(ticket_link)s cu o fotografie %(user_name)s a creat biletul %(ticket_link)s %(user_name)s a răspuns la cererea %(ticket_link)s %(user_name)s a răspuns la cererea %(ticket_link)s cu o fotografie Statutul cererii %(ticket_link)s a fost schimbat în %(status)s. Statutul cererii a fost schimbat în Acest e-mail a fost generat automat, vă rugăm să nu răspundeți. Starea biletului %(ticket_link)s s-a schimbat în %(status)s. închisă duplicat deschis redeschis rezolvat 