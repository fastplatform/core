# `core/event/messaging`: Events handler for messaging activities

The [core/event/messaging](./) service is a [FastAPI](https://fastapi.tiangolo.com/) service that exposes several webhooks that are called by the [core/api_gateway](../../api_gateway/) when selected events are triggered on messaging objects (broacasts and tickets). 

Its role is to send notifications to users (farmers and staff).

## Architecture

The service is a [FastAPI](https://fastapi.tiangolo.com/) application served behind a [uvicorn](https://www.uvicorn.org/) HTTP server.

For each event, the handler will:
1. Process the event and determine the users to notify
2. Notify the Farmer application users by push notifications (if they registered their device previously)
3. Notify the Farmer application users by email (if their email address is known) using the User email template
4. Notify the Admin portal users (staff users) by email  (if their email address is known) using the Staff email template

The email are formatted using the [Jinja 2](https://jinja.palletsprojects.com/en/3.1.x/) templating engine and are internationalized by the [gettext](https://www.gnu.org/software/gettext/) utility.


```plantuml
component "API Gateway" as api_gateway
component "Messaging event handler" as messaging
component "SMTP server" as smtp_server
component "Push notifications service (behind the API Gateway)" as push_notifications
actor "User" as user

api_gateway --> messaging: **[1]** Trigger messaging event
messaging --> api_gateway: **[2]** Request users to notify
messaging --> smtp_server: **[3]** Notify users by email
messaging --> push_notifications: **[4]** Notify users by push notifications
smtp_server --> user : Email
push_notifications --> user : Push notification
```

## API

### Endpoints

There is one endpoint per type of event:
- `/broadcast/push_notification_broadcast_sending_triggered`: when a new broadcast is sent
- `/ticket/push_notification_on_new_ticket`: when a new ticket is created
- `/ticket/push_notification_on_new_ticket`: when a new ticket message is created
- `/ticket/push_notification_on_ticket_status_change`: when a ticket status is changed
- `/ticket/push_notification_on_ticket_queue_change`: when a ticket is assigned to a new queue

## Environment variables

- `ADMIN_PORTAL_URL`: URL of the [Administration portal](../../web/backend)
- `API_GATEWAY_FASTPLATFORM_URL`: URL of the [fastplatform](../../api_gateway/fastplatform) API Gateway
- `API_GATEWAY_SERVICE_KEY`: secret servive key to access the [fastplatform](../../api_gateway/fastplatform) API Gateway
- `EVENT_MESSAGING_LOCALE`: ```locale``` used for internationalized messages
- `LOG_LEVEL`: logging level (trace, debug, info, warn, error, fatal)
- `SMTP_HOST`: host of the SMTP server used for email notification
- `SMTP_PORT`: port of the SMTP server used for email notification (SMTP SSL only)
- `SMTP_SENDER`: address of the sender of all notification emails sent.
- `WEB_APP_URL`: URL of the [FaST](https://gitlab.com/fastplatform/mobile/farmer-mobile-app/-/blob/master/services/apps/farmer/deploy/base/service.yaml) web application


## Development

### Prerequisites

- Python 3.7+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)

### Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```
make start-tracing
```
Jaeger UI will be available at [http://localhost:16686]()

Start the service:
```
make start
```

The API server is now started and available at http://localhost:7010.

**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run 
```

### Internationalization (i18n)

This project is internationalized using the standard ```gettext``` utility (aliased to ```_```). Messages files are located in the [locale]() directory.

[Babel](http://babel.pocoo.org/en/latest/index.html) is used to process message files.

#### Add a new message

Follow the above steps when adding a new message in any ```.py``` or ```.html``` files:
1. Extract the messages
2. if need initialize a new locale (language)
3. Update the messages
4. Translate messages in ```<locale>/messages.po``` files for each locale
5. Finally compile messages

#### Common operations on messages

To extract messages to [locale/messages.pot]()
```
make extract-messages
```

To init a new locale in ```<locale>```
```
make init-messages LOCALE=<LOCALE>
```

To update ```<locale>/messages.po``` from [locale/messages.pot]() template
```
make update-messages
```

To compile ```<locale>/messages.po``` to ```<locale>/messages.mo```
```
make compile-messages
```