import logging

from opentelemetry import trace
from gql import gql
from graphql import GraphQLError

from app.settings import config

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def handler_push_notification_broadcast_sending_triggered(
    broadcast_id: int, graphql_clients: dict
):
    try:
        return await _handler_push_notification_broadcast_sending_triggered(
            broadcast_id, graphql_clients
        )
    except GraphQLError:
        logger.exception(
            f"A GraphQL error occured while sending notification for broadcast #{broadcast_id}"
        )


async def _handler_push_notification_broadcast_sending_triggered(
    broadcast_id: int, graphql_clients: dict
):
    sent = True

    with tracer().start_as_current_span("query_broadcast"):
        # In case the broadcast has changed since the event was emitted
        # fetch it again
        query = (config.API_DIR / "graphql/query_broadcast_by_pk.graphql").read_text()
        variables = {"id": broadcast_id}
        client = graphql_clients["fastplatform"]
        response = await client.execute(gql(query), variables)
        broadcast = response["broadcast_by_pk"]
        if not broadcast:
            return f"Broadcast {broadcast_id} does not exist"

    # If the broadcast does not have the right status, abort
    if broadcast.get("status", None) not in ["SENDING", "FAILED"]:
        return f"Not sent, status={broadcast['status']}"

    with tracer().start_as_current_span("determine_user_ids"):
        query = (config.API_DIR / "graphql/query_user_active.graphql").read_text()
        client = graphql_clients["fastplatform"]
        response = await client.execute(gql(query))
        user_ids = [user["id"] for user in response["user"]]

    with tracer().start_as_current_span("send_push_notification") as span:
        mutation = (
            config.API_DIR / "graphql/mutation_send_notification.graphql"
        ).read_text()
        title = broadcast["title"]
        body = broadcast["body"]
        name = broadcast["sent_by"]["name"]
        username = broadcast["sent_by"]["username"]
        display_name = name if name else username
        body_display = display_name
        if body:
            body_display += f": {body}"
        variables = {
            "user_ids": user_ids,
            "message_payload": {"title": title, "body": body_display},
        }
        client = graphql_clients["fastplatform"]
        try:
            response = await client.execute(gql(mutation), variables)
        except Exception as err:
            logger.exception(
                f"And error occured on the push notification server for broadcast #{broadcast_id}"
            )
            span.add_event(str(err))
            sent = False

    with tracer().start_as_current_span("set_broadcast_status_as_sent"):
        mutation = (
            config.API_DIR / "graphql/mutation_update_broadcast_status.graphql"
        ).read_text()
        status = "SENT" if sent else "FAILED"
        variables = {"id": broadcast_id, "status": status}
        client = graphql_clients["fastplatform"]
        response = await client.execute(gql(mutation), variables)

    return response
