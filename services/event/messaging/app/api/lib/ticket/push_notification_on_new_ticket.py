import logging

from opentelemetry import trace
from gql import gql
from graphql import GraphQLError

from app.settings import config
from app.email.send_email import send_email
from app.email.template import (
    get_subject,
    get_body_html,
    get_user_admin_link_html,
    get_ticket_admin_link_html,
    get_holding_admin_link_html,
    get_ticket_webapp_link_html,
)

# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


async def handler_push_notification_on_new_ticket(
    ticket_id: int, graphql_clients: dict, notify_only_staff_users: bool = False
):
    try:
        return await _handler_push_notification_on_new_ticket(
            ticket_id, graphql_clients, notify_only_staff_users
        )
    except GraphQLError:
        logger.exception(
            f"A GraphQL error occured while sending notification for ticket #{ticket_id}"
        )


async def _handler_push_notification_on_new_ticket(
    ticket_id: int, graphql_clients: dict, notify_only_staff_users: bool
):
    with tracer().start_as_current_span("query_ticket"):
        # In case the ticket has changed since the event was emitted
        # fetch it again
        query = (config.API_DIR / "graphql/query_ticket_by_pk.graphql").read_text()
        variables = {"id": ticket_id}
        client = graphql_clients["fastplatform"]
        response = await client.execute(gql(query), variables)
        ticket = response["ticket_by_pk"]
        if not ticket:
            return f"ticket {ticket_id} does not exist"
        ticket_id = ticket["id"]
        title = ticket["title"]
        body = ticket["body"]
        user_id = ticket["created_by"]["id"]
        name = ticket["created_by"]["name"]
        username = ticket["created_by"]["username"]
        display_name = name if name else username
        holding_id = ticket["holding"]["id"]
        holding_name = ticket["holding"]["name"]

    with tracer().start_as_current_span("determine_user_to_notify"):
        if notify_only_staff_users:
            user_ids = []
            user_emails = set()
        else:
            query = (
                config.API_DIR / "graphql/query_holding_user_active.graphql"
            ).read_text()
            variables = {
                "user_id": user_id,
                "holding_id": holding_id,
            }
            client = graphql_clients["fastplatform"]
            response = await client.execute(gql(query), variables)
            user_ids = [user["id"] for user in response["user"]]
            user_emails = set(
                [user["email"] for user in response["user"] if user["email"]]
            )

    with tracer().start_as_current_span("determine_staff_users_to_email"):
        query = (config.API_DIR / "graphql/query_queue_user.graphql").read_text()
        variables = {
            "user_id": ticket["created_by_id"],
            "queue_id": ticket["queue"]["id"],
        }
        client = graphql_clients["fastplatform"]
        response = await client.execute(gql(query), variables)
        staff_emails = set(
            [user["email"] for user in response["user"] if user["email"]]
        )
        # remove staff emails from user emails to avoid notifying them twice
        user_emails = user_emails - staff_emails

    with tracer().start_as_current_span("send_email"):
        email_data = _get_email_data(
            ticket_id, title, body, user_id, display_name, holding_id, holding_name
        )
        _send_email(staff_emails, email_data, staff=True)
        _send_email(user_emails, email_data)

    with tracer().start_as_current_span("send_push_notification") as span:
        if len(user_ids) > 0:
            mutation = (
                config.API_DIR / "graphql/mutation_send_notification.graphql"
            ).read_text()
            body_display = display_name
            if body:
                body_display += f": {body}"
            variables = {
                "user_ids": user_ids,
                "message_payload": {
                    "title": title,
                    "body": body_display,
                },
            }
            client = graphql_clients["fastplatform"]
            try:
                response = await client.execute(gql(mutation), variables)
            except Exception as err:
                logger.exception(
                    f"And error occured on the push notification server for ticket #{ticket_id}"
                )
                span.add_event(str(err))

    return response


def _send_email(emails, data, staff=False):
    subject = get_subject(data["ticket_id"], data["ticket_title"])
    template_prefix = "staff" if staff else "user"
    template_name = f"{template_prefix}_new_ticket.html"
    content = get_body_html(template_name, data)
    send_email(emails, subject, content, bcc=not (staff))


def _get_email_data(
    ticket_id, ticket_title, body, user_id, user_name, holding_id, holding_name
):
    return {
        "ticket_id": ticket_id,
        "ticket_title": ticket_title,
        "ticket_admin_link": get_ticket_admin_link_html(ticket_id, ticket_title),
        "ticket_webapp_link": get_ticket_webapp_link_html(ticket_id, ticket_title),
        "user_id": user_id,
        "user_name": user_name,
        "user_admin_link": get_user_admin_link_html(user_id, user_name),
        "holding_id": holding_id,
        "holding_name": holding_name,
        "holding_admin_link": get_holding_admin_link_html(holding_id, holding_name),
        "body_text": body,
    }