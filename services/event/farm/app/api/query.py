import logging
import graphene

from app.api.resolvers import (
    ComputedConstraintByPKResolver,
    ComputedConstraintsWaterCourseResolver,
    ComputedConstraintsManagementRestrictionOrRegulationZoneResolver,
    ComputedConstraintsProtectedSiteResolver,
    ComputedConstraintsSurfaceWaterResolver,
    ComputedPlotConstraintsResolver,
    ComputedSoilDerivedObjectByPKResolver,
    ComputedSoilDerivedObjectResolver,
)

from app.api.types import ComputedConstraint, ComputedSoilDerivedObject


logger = logging.getLogger(__name__)


class Query(graphene.ObjectType):

    computed_constraints_management_restriction_or_regulation_zone = graphene.List(
        ComputedConstraint,
        plot_id=graphene.Argument(graphene.Int, required=True),
    )

    computed_constraints_protected_site = graphene.List(
        ComputedConstraint,
        plot_id=graphene.Argument(graphene.Int, required=True),
    )

    computed_constraints_surface_water = graphene.List(
        ComputedConstraint,
        plot_id=graphene.Argument(graphene.Int, required=True),
    )

    computed_constraints_water_course = graphene.List(
        ComputedConstraint,
        plot_id=graphene.Argument(graphene.Int, required=True),
    )

    computed_plot_constraints = graphene.List(
        ComputedConstraint,
        plot_id=graphene.Argument(graphene.Int, required=True),
    )

    computed_constraint_by_pk = graphene.Field(
        ComputedConstraint,
        id=graphene.Argument(graphene.Int, required=True),
    )

    computed_soil_derived_object = graphene.List(
        ComputedSoilDerivedObject,
        plot_id=graphene.Argument(graphene.Int, required=True),
    )

    computed_soil_derived_object_by_pk = graphene.Field(
        ComputedSoilDerivedObject,
        id=graphene.Argument(graphene.Int, required=True),
    )

    def resolve_computed_constraints_management_restriction_or_regulation_zone(
        self, info, plot_id
    ):

        return (
            ComputedConstraintsManagementRestrictionOrRegulationZoneResolver().resolve(
                plot_id
            )
        )

    def resolve_computed_constraints_protected_site(self, info, plot_id):
        return ComputedConstraintsProtectedSiteResolver().resolve(plot_id)

    def resolve_computed_constraints_surface_water(self, info, plot_id):
        return ComputedConstraintsSurfaceWaterResolver().resolve(plot_id)

    def resolve_computed_constraints_water_course(self, info, plot_id):
        return ComputedConstraintsWaterCourseResolver().resolve(plot_id)

    def resolve_computed_constraint_by_pk(self, info, id):
        return ComputedConstraintByPKResolver().resolve(id)

    def resolve_computed_plot_constraints(self, info, plot_id):
        return ComputedPlotConstraintsResolver().resolve(plot_id)

    def resolve_computed_soil_derived_object_by_pk(self, info, id):
        return ComputedSoilDerivedObjectByPKResolver().resolve(id)

    def resolve_computed_soil_derived_object(self, info, plot_id):
        return ComputedSoilDerivedObjectResolver().resolve(plot_id)
