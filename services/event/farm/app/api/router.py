import logging

from fastapi import APIRouter, Request
from opentelemetry import trace

from app.api.lib.event import HasuraEvent
from app.api.controllers import (
    delete_all_plot_constraints_and_soil_derived_object,
    delete_all_soil_derived_objects_whose_plot_intersect_geometry,
    delete_all_soil_derived_object_whose_plot_belong_to_campaign,
    delete_all_constraints_of_type,
    delete_all_soil_derived_objects,
    get_soil_site_geometry,
)

INSERT = "INSERT"
UPDATE = "UPDATE"
DELETE = "DELETE"

# Log
logger = logging.getLogger(__name__)

# FastAPI
router = APIRouter()

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


@router.post("/delete_sdo_on_campaign_end_at_updated")
async def delete_sdo_on_campaign_end_at_updated(request: Request, event: HasuraEvent):
    logger.info("delete_sdo_on_campaign_end_at_updated")
    campaign_id = event.event.data.new["id"]

    return await delete_all_soil_derived_object_whose_plot_belong_to_campaign(
        campaign_id
    )


@router.post("/delete_sdo_and_constraints_on_plot_geometry_updated")
async def delete_sdo_and_constraints_on_plot_geometry_updated(
    request: Request, event: HasuraEvent
):
    logger.info("delete_sdo_and_constraints_on_plot_geometry_updated")
    plot_id = event.event.data.new["id"]

    return await delete_all_plot_constraints_and_soil_derived_object(plot_id)


@router.post("/delete_sdo_on_soil_site_event")
async def delete_sdo_on_soil_site_event(request: Request, event: HasuraEvent):
    logger.info("delete_sdo_on_soil_site_event")
    operation = event.event.op
    payload = event.event.data
    new_soil_site_id = payload.new["id"] if payload.new else None
    new_soil_site_geometry = payload.new["geometry"] if payload.new else None
    old_soil_site_id = payload.old["id"] if payload.old else None
    old_soil_site_geometry = payload.old["geometry"] if payload.old else None

    if operation == INSERT:
        return await delete_all_soil_derived_objects_whose_plot_intersect_geometry(
            new_soil_site_id, new_soil_site_geometry
        )
    elif operation == UPDATE:
        if old_soil_site_geometry is not None:
            await delete_all_soil_derived_objects_whose_plot_intersect_geometry(
                new_soil_site_id, old_soil_site_geometry
            )

        await delete_all_soil_derived_objects_whose_plot_intersect_geometry(
            new_soil_site_id, new_soil_site_geometry
        )
    elif operation == DELETE:
        return await delete_all_soil_derived_objects_whose_plot_intersect_geometry(
            old_soil_site_id, old_soil_site_geometry
        )


@router.post("/delete_sdo_on_observation_event")
async def delete_sdo_on_observation_event(request: Request, event: HasuraEvent):
    logger.info("delete_sdo_on_observation_event")
    operation = event.event.op
    payload = event.event.data
    soil_site_id = payload.new["soil_site_id"] if payload.new else None
    if operation == DELETE:
        soil_site_id = payload.old["soil_site_id"] if payload.old else None

    soil_site_geometry = await get_soil_site_geometry(soil_site_id)

    if not soil_site_geometry:
        # soil_site already deleted
        return None

    return await delete_all_soil_derived_objects_whose_plot_intersect_geometry(
        soil_site_id, soil_site_geometry
    )


@router.post("/delete_sdo_on_external_soil_site_version_is_active_updated")
async def delete_sdo_on_external_soil_site_version_is_active_updated(
    request: Request, event: HasuraEvent
):
    logger.info("delete_sdo_on_external_soil_site_version_is_active_updated")

    payload = event.event.data
    is_activating = payload.new["is_active"] if payload.new else None

    if is_activating:
        # TODO: optimize
        return await delete_all_soil_derived_objects()


@router.post("/delete_constraint_on_water_course_version_is_active_updated")
async def delete_constraint_on_water_course_version_is_active_updated(
    request: Request, event: HasuraEvent
):
    logger.info("delete_constraint_on_water_course_version_is_active_updated")

    payload = event.event.data
    is_activating = payload.new["is_active"] if payload.new else None

    if is_activating:
        # TODO: optimize
        return await delete_all_constraints_of_type("water_course")


@router.post("/delete_constraint_on_surface_water_version_is_active_updated")
async def delete_constraint_on_surface_water_version_is_active_updated(
    request: Request, event: HasuraEvent
):
    logger.info("delete_constraint_on_surface_water_version_is_active_updated")

    payload = event.event.data
    is_activating = payload.new["is_active"] if payload.new else None

    if is_activating:
        # TODO: optimize
        return await delete_all_constraints_of_type("surface_water")


@router.post("/delete_constraint_on_protected_site_version_is_active_updated")
async def delete_constraint_on_protected_site_version_is_active_updated(
    request: Request, event: HasuraEvent
):
    logger.info("delete_constraint_on_protected_site_version_is_active_updated")

    payload = event.event.data
    is_activating = payload.new["is_active"] if payload.new else None

    if is_activating:
        # TODO: optimize
        return await delete_all_constraints_of_type("protected_site")


@router.post(
    "/delete_constraint_on_management_restriction_or_regulation_zone_version_is_active_updated"
)
async def delete_constraint_on_management_restriction_or_regulation_zone_version_is_active_updated(
    request: Request, event: HasuraEvent
):
    logger.info(
        "delete_constraint_on_management_restriction_or_regulation_zone_version_is_active_updated"
    )

    payload = event.event.data
    is_activating = payload.new["is_active"] if payload.new else None

    if is_activating:
        # TODO: optimize
        return await delete_all_constraints_of_type(
            "management_restriction_or_regulation_zone"
        )
