import graphene
from graphene.types.generic import GenericScalar


class UnitOfMeasure(graphene.ObjectType):
    id = graphene.String(required=True)
    name = graphene.String(required=True)
    symbol = graphene.String(required=True)
    i18n = GenericScalar(required=False)

    class Meta:
        name = "computed_unit_of_measure"


class ObservableProperty(graphene.ObjectType):
    id = graphene.String(required=True)
    label = graphene.String(required=True)
    unit_of_measure = graphene.Field(UnitOfMeasure, required=True)
    i18n = GenericScalar(required=False)

    class Meta:
        name = "computed_observable_property"


class ComputedDerivedObservation(graphene.ObjectType):
    id = graphene.Int(required=True)
    result = GenericScalar()
    observed_property_id = graphene.ID(required=True)
    observable_property = graphene.Field(ObservableProperty, required=True)

    class Meta:
        name = "computed_derived_observation"


class ComputedSoilDerivedObject(graphene.ObjectType):
    id = graphene.Int(required=True)
    is_derived_from = graphene.JSONString()
    soil_sample_origin = graphene.NonNull(
        graphene.String, description="Soil sample origin"
    )
    soil_sites_count = graphene.NonNull(
        graphene.Int,
        description="Number of soil samples used to estimate the soil properties",
    )
    plot_id = graphene.ID(required=True)
    derived_observations = graphene.List(ComputedDerivedObservation)

    class Meta:
        name = "computed_soil_derived_object"
