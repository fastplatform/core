import logging
from opentelemetry import trace
import asyncio
import itertools
from app.api.errors import GraphQLHoldingPlotNotFoundError, GraphQLUnexpectedDataError
from . import (
    ComputedConstraintsManagementRestrictionOrRegulationZoneResolver,
    ComputedConstraintsProtectedSiteResolver,
    ComputedConstraintsSurfaceWaterResolver,
    ComputedConstraintsWaterCourseResolver,
)

# Log
logger = logging.getLogger(__name__)


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


class ComputedPlotConstraintsResolver:
    async def resolve(self, plot_id: int):
        """
        For a given plot, compute all its constraints:
            - management_restriction_or_regulation_zone
            - protected_site
            - surface_water
            - water_course

        Args:
            plot_id (int): the id of the plot whose constraints are calculated here.

        Raises:
            GraphQLHoldingPlotNotFoundError: No plot associated with the id provided.
            GraphQLUnexpectedDataError: The graphql operation could not be performed successfully.

        Returns:
            [type]: [description]
        """
        with tracer().start_as_current_span("resolve_plot_constraints"):
            logger.info("ComputedPlotConstraints: resolve()")
            try:
                resolvers = [
                    ComputedConstraintsManagementRestrictionOrRegulationZoneResolver,
                    ComputedConstraintsProtectedSiteResolver,
                    ComputedConstraintsSurfaceWaterResolver,
                    ComputedConstraintsWaterCourseResolver,
                ]

                async def resolve(resolver):
                    return await resolver().resolve(plot_id)

                resolves = [resolve(resolver=resolver) for resolver in resolvers]
                result = await asyncio.gather(*resolves)

                # Filter out None values
                result = list(filter(None, result))

                if result == []:
                    return []

                chain = itertools.chain(*result)
                constraints = list(chain)

                return constraints

            except GraphQLHoldingPlotNotFoundError as ex:
                raise GraphQLHoldingPlotNotFoundError() from ex

            except Exception as ex:
                logger.exception("ComputedPlotConstraints error")
                raise GraphQLUnexpectedDataError() from ex
