import logging

import graphene
import uvicorn
from fastapi import FastAPI
from starlette_graphene3 import GraphQLApp

from app.db.graphql_clients import fastplatform

from app.api.query import Query
from app.settings import config
from app.tracing import Tracing
from app.api import router

# FastAPI
app = FastAPI()

## TODO: events
app.include_router(router.router, prefix="/plot")

# GraphQL root
app.add_route(
    "/graphql",
    GraphQLApp(schema=graphene.Schema(query=Query, auto_camelcase=False)),
)

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():
    """On app startup"""
    logger.debug(config)

    # Debuging instrumentation
    if config.REMOTE_DEBUG:
        import debugpy

        debugpy.listen(5678)

    await fastplatform.connect()


@app.on_event("shutdown")
async def shutdown():
    """On app shutdown"""
    await fastplatform.close()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7777)
