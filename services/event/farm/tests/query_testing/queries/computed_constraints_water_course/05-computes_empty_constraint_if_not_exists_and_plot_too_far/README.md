# 04-computes_accurate_constraint_if_not_exists
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 water_course too far from plot, with distance > `config.API_PLOT_COMPUTE_CONSTRAINTS_WATER_COURSE_SEARCH_DISTANCE`
- No precomputed water_course constraint.

## Computed
Null constraint (`"computed_constraints_water_course": null`)

## Returns
The empty constraint computed, with description = null.