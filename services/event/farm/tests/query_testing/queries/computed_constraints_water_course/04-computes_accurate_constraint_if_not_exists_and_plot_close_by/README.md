# 04-computes_accurate_constraint_if_not_exists
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 water_course within distance of `config.API_PLOT_COMPUTE_CONSTRAINTS_WATER_COURSE_SEARCH_DISTANCE`
- No precomputed water_course constraint.

## Computed
A new constraint with accurate `distance`.

## Returns
The constraint with the correct distance.