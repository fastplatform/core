# 03-computes_sdo_from_private_soil_sites_if_any

## Setup
- 1 holding containing 1 plot.
- 1 private soil_site
- 1 public soil_site
- 0 previously computed sdo

## Computed
Finds no existing sdo, and computes from private soil_site, not public soil_site

## Returns
Returns sdo computed from private soil_site