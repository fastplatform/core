# 04-computes_sdo_from_public_soil_sites_if_any

## Setup
- 1 holding containing 1 plot.
- 0 private soil_site
- 2 public soil_site
- 0 previously computed sdo

## Computed
Finds no existing sdo, no private soil_site, and computes sdo from the average of 2 public soil_site

## Returns
Returns sdo computed from 2 public soil_site