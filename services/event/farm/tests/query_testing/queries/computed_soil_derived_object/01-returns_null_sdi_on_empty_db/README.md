# 01-returns_null_sdo_on_empty_db

## Setup
- 1 holding containing 1 plot.
- 0 private soil_site
- 0 public soil_site
- 0 previously computed sdo

## Computed
Computes and finds nothing, creates an empty sdo, with 
- "soil_sample_origin": "none"
- "derived_observations": []

## Returns
Null sdo (`"computed_soil_derived_object": null`)