# 03-returns_full_constraint_if_exists
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 surface_water within `config.API_PLOT_COMPUTE_CONSTRAINTS_SURFACE_WATER_SEARCH_DISTANCE`, one that should return a distance != 333
- 1 surface_water constraint, with description containing **not the right distance**, distance = 333.

## Computed
Nothing.

## Returns
The constraint from setup, with distance = 333, proving *it has not been recalculated.*