# 04-computes_accurate_constraint_if_not_exists
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 surface_water too far from plot, with distance > `config.API_PLOT_COMPUTE_CONSTRAINTS_WATER_COURSE_SEARCH_DISTANCE`
- No precomputed surface_water constraint.

## Computed
An empty constraint, with description = null.

## Returns
Null constraint (`"computed_constraints_surface_water": null`)