# 01-returns_null_constraint_on_empty_db

## Setup
- 1 holding containing 1 plot.

## Computed
An empty constraint should be added for plot, with description = null.

## Returns
Null constraint (`"computed_constraints_surface_water": null`)