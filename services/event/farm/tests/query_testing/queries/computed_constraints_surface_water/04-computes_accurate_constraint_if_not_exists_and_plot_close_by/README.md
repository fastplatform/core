# 04-computes_accurate_constraint_if_not_exists
If exists already, should not be recalculated.

## Setup
- 1 holding containing 1 plot.
- 1 surface_water within distance of `config.API_PLOT_COMPUTE_CONSTRAINTS_SURFACE_WATER_SEARCH_DISTANCE`
- No precomputed surface_water constraint.

## Computed
A new constraint with accurate `distance`.


## Returns
The constraint with the correct `distance`.