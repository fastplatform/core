"""
This class executes a any number of graphql setup files from the query_testing folder.

It can be used to prepare db and set it up in a specific way for testing.
"""
from pathlib import Path
import json
import asyncio
from gql import gql
from pathlib import Path
from app.db.graphql_clients import fastplatform
from .constants import SHARED_VALUES


QUERIES_DIR = Path(__file__).parent / "query_testing" / "queries"
CLEAR_DBS_QUERY_URI = Path(__file__).parent / "query_clear_dbs.graphql"


CLEAR_DBS_QUERY = CLEAR_DBS_QUERY_URI.read_text()

# -> Prepares db so that real plot sdo and constraints
#    can be computed from db data.
# PREPARE_QUERIES = {
#     "computed_soil_derived_object": [
#         "query_setup.graphql",
#         "03-computes_sdo_from_private_soil_sites_if_any",
#     ],
#     "computed_constraints_management_restriction_or_regulation_zone": [
#         "04-computes_accurate_constraint_if_not_exists_and_plot_intersects",
#     ],
#     "computed_constraints_protected_site": [
#         "04-computes_accurate_constraint_if_not_exists_and_plot_intersects",
#     ],
#     "computed_constraints_surface_water": [
#         "04-computes_accurate_constraint_if_not_exists_and_plot_close_by",
#     ],
#     "computed_constraints_water_course": [
#         "04-computes_accurate_constraint_if_not_exists_and_plot_close_by",
#     ],
# }

# -> Prepares db so that real plot so that some constraints (not all)
#    can be computed from db data.
PREPARE_QUERIES = {
    "computed_constraints_management_restriction_or_regulation_zone": [
        "query_setup.graphql",
        "04-computes_accurate_constraint_if_not_exists_and_plot_intersects",
    ],
    "computed_constraints_water_course": [
        "04-computes_accurate_constraint_if_not_exists_and_plot_close_by",
    ],
}

# # -> Prepares everything so that real plot sdo and constraints
# #    can be computed from db data, and then compute them.
# PREPARE_QUERIES = {
#     "computed_soil_derived_object": [
#         "query_setup.graphql",
#         "03-computes_sdo_from_private_soil_sites_if_any",
#         "query.graphql",
#     ],
#     "computed_constraints_management_restriction_or_regulation_zone": [
#         "04-computes_accurate_constraint_if_not_exists_and_plot_intersects",
#         "query.graphql",
#     ],
#     "computed_constraints_protected_site": [
#         "04-computes_accurate_constraint_if_not_exists_and_plot_intersects",
#         "query.graphql",
#     ],
#     "computed_constraints_surface_water": [
#         "04-computes_accurate_constraint_if_not_exists_and_plot_close_by",
#         "query.graphql",
#     ],
#     "computed_constraints_water_course": [
#         "04-computes_accurate_constraint_if_not_exists_and_plot_close_by",
#         "query.graphql",
#     ],
# }

# # -> Create just one holding and one plot, and nothing else
# PREPARE_QUERIES = {
#     "computed_soil_derived_object": [
#         "query_setup.graphql",
#     ]
# }


def load_json(relative_path):
    json_uri = str(QUERIES_DIR / f"{relative_path}.json")
    content = Path(json_uri).read_text()

    # Format expected with SHARED_VALUES
    for place_holder, value in (
        (ph, v) for (ph, v) in SHARED_VALUES.items() if ph in content
    ):
        if isinstance(value, str):
            # Keep double-quotes surrounding value in dict
            content = content.replace(f"{{{{ { place_holder } }}}}", value)
        else:
            # Remove double-quotes for every other types
            content = content.replace(f'"{{{{ { place_holder } }}}}"', str(value))

    # return actual json
    return json.loads(content)


def load_query(relative_path):
    query_path = Path(str(QUERIES_DIR / f"{relative_path}.graphql"))
    if not query_path.exists():
        return None

    query = query_path.read_text()
    for place_holder, value in (
        (ph, v) for (ph, v) in SHARED_VALUES.items() if ph in query
    ):
        query = query.replace(f"{{{{ { place_holder } }}}}", str(value))

    return query


async def execute():
    queries_to_execute = []

    for query_name in PREPARE_QUERIES.keys():
        for script in PREPARE_QUERIES[query_name]:
            if script.endswith(".graphql"):
                script = script.replace(".graphql", "")
                queries_to_execute.append(load_query(f"{query_name}/{script}"))
            else:
                queries_to_execute.append(
                    load_query(f"{query_name}/{script}/scenario_setup")
                )

        await fastplatform.connect()
        await fastplatform.execute(gql(CLEAR_DBS_QUERY))

        for query in queries_to_execute:
            await fastplatform.execute(gql(query))

        await fastplatform.close()


if __name__ == "__main__":
    asyncio.run(execute())
