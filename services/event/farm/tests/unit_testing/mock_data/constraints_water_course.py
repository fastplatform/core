from app.api.types import ComputedConstraint
from .shared import *

EXISTING_CONSTRAINT = {
    "description": {
        "water_course_id": 1,
        "water_course_name": "name",
        "distance_to_water_course": 0.0,
    },
    "id": 1,
    "name": "water_course",
    "plot_id": PLOT_ID,
}


EXISTING_CONSTRAINTS = [EXISTING_CONSTRAINT]

EXPECTED_EXISTING_CONSTRAINT = ComputedConstraint(**EXISTING_CONSTRAINT)

CLOSE_BY_WATER_COURSE = [
    {
        "id": 1,
        "geographical_name": "name",
        "geometry": CLOSE_BY_GEOMETRY_NODE,
    }
]

COMPUTED_CONSTRAINT = {
    "name": "water_course",
    "plot_id": PLOT_ID,
    "description": {
        "water_course_id": 1,
        "water_course_name": "name",
        "distance_to_water_course": 213.82312507725607,
    },
}

EMPTY_CONSTRAINT = {"name": "water_course", "plot_id": PLOT_ID}
