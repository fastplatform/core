from app.api.types import ComputedConstraint
from .shared import *

EXISTING_CONSTRAINT = {
    "description": {
        "surface_water_id": 1,
        "surface_water_name": "name",
        "distance_to_surface_water": 0,
    },
    "id": 1,
    "name": "surface_water",
    "plot_id": PLOT_ID,
}


EXISTING_CONSTRAINTS = [EXISTING_CONSTRAINT]

EXPECTED_EXISTING_CONSTRAINT = ComputedConstraint(**EXISTING_CONSTRAINT)

CLOSE_BY_WATER_COURSE = [
    {
        "id": 1,
        "geographical_name": "name",
        "geometry": CLOSE_BY_GEOMETRY_NODE,
    }
]

COMPUTED_CONSTRAINT = {
    "name": "surface_water",
    "plot_id": PLOT_ID,
    "description": {
        "surface_water_id": 1,
        "surface_water_name": "name",
        "distance_to_surface_water": 213.82312507725607,
    },
}


EMPTY_CONSTRAINT = {"name": "surface_water", "plot_id": PLOT_ID}
