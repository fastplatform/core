from shapely.geometry.base import EMPTY


PLOT_ID = 1
USER_ID = "USER_ID"

PLOT_GEOMETRY_NODE = {
    "type": "MultiPolygon",
    "crs": {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::4258"}},
    "coordinates": [
        [
            [
                [4.70049402428607, 50.3327983692753],
                [4.70111704105072, 50.3331926478598],
                [4.6999701336121, 50.3339088900415],
                [4.70049402428607, 50.3327983692753],
            ]
        ]
    ],
}

INTERSECTING_GEOMETRY_NODE = {
    "type": "MultiPolygon",
    "crs": {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::4258"}},
    "coordinates": [
        [
            [
                [4.69949402428607, 50.3318983692753],
                [4.70041704105072, 50.3332926478598],
                [4.6999701336121, 50.3340088900415],
                [4.69949402428607, 50.3318983692753],
            ]
        ]
    ],
}

CLOSE_BY_GEOMETRY_NODE = {
    "type": "MultiPolygon",
    "crs": {"type": "name", "properties": {"name": "urn:ogc:def:crs:EPSG::4258"}},
    "coordinates": [
        [
            [
                [4.69649402428607, 50.3318983692753],
                [4.69641704105072, 50.3332926478598],
                [4.6969701336121, 50.3340088900415],
                [4.69649402428607, 50.3318983692753],
            ]
        ]
    ],
}

QUERY_PLOT_BY_PK = {
    "id": 1,
    "geometry": PLOT_GEOMETRY_NODE,
    "site": {
        "id": 1,
        "holding_campaign": {
            "campaign": {
                "start_at": "2019-10-01T00:00:00+00:00",
                "end_at": "2020-10-01T00:00:00+00:00",
            },
            "holding_id": f"DEMO-{USER_ID}",
        },
    },
}
