from app.api.types import ComputedConstraint
from .shared import *

EXISTING_CONSTRAINT = {
    "description": {
        "protected_site_id": 1,
        "protected_site_name": "name",
        "plot_pct_in_protected_site": 0.7782753961971729,
        "plot_area_in_protected_site": 6236223.470505392,
    },
    "id": 1,
    "name": "protected_site",
    "plot_id": PLOT_ID,
}


EXISTING_CONSTRAINTS = [EXISTING_CONSTRAINT]

EXPECTED_EXISTING_CONSTRAINT = ComputedConstraint(**EXISTING_CONSTRAINT)

INTERSECTING_PROTECTED_SITE = [
    {"id": 1, "site_name": "name", "geometry": INTERSECTING_GEOMETRY_NODE}
]

COMPUTED_CONSTRAINT = {
    "name": "protected_site",
    "plot_id": PLOT_ID,
    "description": {
        "protected_site_id": 1,
        "protected_site_name": "name",
        "plot_pct_in_protected_site": 0.16944617396772127,
        "plot_area_in_protected_site": 602.8421348452359,
    },
}

EMPTY_CONSTRAINT = {"name": "protected_site", "plot_id": PLOT_ID}
