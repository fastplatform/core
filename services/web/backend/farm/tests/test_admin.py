from django.test import TestCase, override_settings

from utils.tests.test_admin import AdminTestCaseMixin

from farm.models import (
    Plot,
    PlotPlantVariety,
    HoldingCampaign,
    Site,
    Holding,
    Campaign,
    UserRelatedParty,
)

from configuration.tests.recipes import configuration_recipe
from farm.tests.recipes import (
    holding_recipe,
    campaign_recipe,
    holding_campaign_recipe,
    site_recipe,
    plot_recipe,
    user_related_party_recipe,
)


@override_settings(AXES_ENABLED=False)
class PlotAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Plot
    recipe = plot_recipe

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class PlotPlantVarietyAdminTestCase(AdminTestCaseMixin, TestCase):
    model = PlotPlantVariety


@override_settings(AXES_ENABLED=False)
class HoldingCampaignAdminTestCase(AdminTestCaseMixin, TestCase):
    model = HoldingCampaign
    recipe = holding_campaign_recipe

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class SiteAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Site
    recipe = site_recipe

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class HoldingAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Holding
    recipe = holding_recipe

    def setUp(self):
        super().setUp()
        configuration_recipe.prepare().save()


@override_settings(AXES_ENABLED=False)
class CampaignAdminTestCase(AdminTestCaseMixin, TestCase):
    model = Campaign
    recipe = campaign_recipe


@override_settings(AXES_ENABLED=False)
class UserRelatedPartyAdminTestCase(AdminTestCaseMixin, TestCase):
    model = UserRelatedParty
    recipe = user_related_party_recipe
