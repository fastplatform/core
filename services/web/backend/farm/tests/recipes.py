from itertools import cycle
from datetime import timedelta, datetime

from django.contrib.gis.geos import GEOSGeometry
from django.utils.timezone import make_aware

from model_bakery.recipe import Recipe, foreign_key, related

from farm.models import (
    Plot,
    PlotPlantVariety,
    HoldingCampaign,
    Site,
    Holding,
    Campaign,
    UserRelatedParty,
)

from authentication.tests.recipes import user_recipe


holding_recipe = Recipe(Holding)

campaign_start_ats = cycle([
    make_aware(datetime(2000, 1, 1, 0, 0, 0) + timedelta(days=i))
    for i in range(0, 1000, 10)
])
campaign_end_ats = cycle([
    make_aware(datetime(2000, 1, 1, 0, 0, 0) + timedelta(days=i))
    for i in range(9, 1009, 10)
])

campaign_recipe = Recipe(
    Campaign,
    start_at=campaign_start_ats,
    end_at=campaign_end_ats,
)

holding_campaign_recipe = Recipe(
    HoldingCampaign,
    holding=foreign_key(holding_recipe),
    campaign=foreign_key(campaign_recipe),
)

site_recipe = Recipe(
    Site,
    holding_campaign=foreign_key(holding_campaign_recipe),
)

plot_recipe = Recipe(
    Plot,
    site=foreign_key(site_recipe),
    geometry=GEOSGeometry(
        "MULTIPOLYGON(((1 1,5 1,5 5,1 5,1 1),(2 2,2 3,3 3,3 2,2 2)),((6 3,9 2,9 4,6 3)))"
    ),
    _fill_optional=False
)

user_related_party_recipe = Recipe(
    UserRelatedParty,
    holding=foreign_key(holding_recipe),
    user=foreign_key(user_recipe),
)