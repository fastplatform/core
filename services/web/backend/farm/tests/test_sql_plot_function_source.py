from farm.models.holding import UserRelatedParty
from time import sleep
from datetime import datetime, timedelta

from django.test import TestCase, override_settings
from django.db import connection
from django.utils import timezone

from oidc_provider.tests.app.utils import create_fake_client
from oidc_provider.lib.utils.token import create_token
from user_sessions.models import Session

import mapbox_vector_tile

from authentication.models import User
from farm.models import Holding, Campaign, HoldingCampaign, Site, Plot


PLOT_WKT = "MULTIPOLYGON(((1.292 43.499,2.155 43.505,1.218 43.843,1.292 43.499)))"
TILE_Z = 14
TILE_X = 8254
TILE_Y = 5981


@override_settings(AXES_ENABLED=False)
class PlotFunctionSourceTestCase(TestCase):
    def createHolding(self, holding_id):
        holding, _ = Holding.objects.update_or_create(
            id=holding_id, defaults=dict(name="Test holding")
        )
        holding.user_related_parties.all().delete()
        holding.holding_campaigns.all().delete()

        user_related_party = UserRelatedParty.objects.create(
            holding=holding, user=self.user, role="farmer"
        )
        holding_campaign = HoldingCampaign.objects.create(
            holding=holding,
            campaign=self.campaign,
        )
        holding_campaign.sites.all().delete()

        site = Site.objects.create(holding_campaign=holding_campaign, name="Test site")
        site.plots.all().delete()

        plot = Plot.objects.create(
            site=site,
            geometry=PLOT_WKT,
            name="Test plot",
        )

        return holding, user_related_party, holding_campaign, site, plot

    def setUp(self):
        super().setUp()

        self.username = "test-user"
        self.password = "whatever"
        self.user, _ = User.objects.update_or_create(
            username=self.username, defaults=dict(is_active=True)
        )

        self.campaign_id = 2025
        self.campaign, _ = Campaign.objects.update_or_create(
            id=self.campaign_id,
            defaults=dict(
                name="2025",
                start_at=timezone.make_aware(datetime(2025, 1, 1)),
                end_at=timezone.make_aware(datetime(2026, 1, 1)),
                is_current=False,
            ),
        )

        # Generate a token for the user
        self.oidc_client = create_fake_client("code")
        self.access_token, refresh_token, at_hash, self.token = create_token(
            self.user, self.oidc_client, []
        )
        self.token.save()

        # Login the user to also generate a session
        self.user.set_password(self.password)
        self.user.save()
        self.user.session_set.all().delete()
        self.client.login(username=self.username, password=self.password)
        self.session_key = Session.objects.filter(user=self.user).first().session_key

        # Create a first holding
        (
            self.holding,
            self.user_related_party,
            self.holding_campaign,
            self.site,
            self.plot,
        ) = self.createHolding("test-holding")

        # Create a second holding with a plot at the same place,
        # but remove the user from the related parties
        (
            self.holding_2,
            self.user_related_party_2,
            self.holding_campaign_2,
            self.site_2,
            self.plot_2,
        ) = self.createHolding("test-holding-2")
        self.user_related_party_2.delete()

        # Open a cursor to the database
        self.cursor = connection.cursor()

    def tearDown(self):
        super().tearDown()
        self.cursor.close()

    def plot_function_source(
        self,
        access_token=None,
        session_key=None,
        holding_campaign_id=None,
        site_id=None,
        plot_id=None,
    ):

        self.cursor.execute(
            "SELECT public.plot_function_source(%s, %s, %s, %s, %s, %s, %s, %s, %s);",
            [
                TILE_Z,
                TILE_X,
                TILE_Y,
                session_key,
                access_token,
                holding_campaign_id,
                site_id,
                plot_id,
                None  # was plot_group_id
            ],
        )
        rows = self.cursor.fetchall()
        return mapbox_vector_tile.decode(rows[0][0]) if rows else None

    def assertTileContainsOnlyPlot(self, tile, plot):
        self.assertDictEqual(
            tile,
            {
                "plot": {
                    "extent": 4096,
                    "version": 2,
                    "features": [
                        {
                            "geometry": {
                                "type": "Polygon",
                                "coordinates": [
                                    [
                                        [-64, 4160],
                                        [4160, 4160],
                                        [4160, -64],
                                        [-64, -64],
                                        [-64, 4160],
                                    ]
                                ],
                            },
                            "properties": {
                                "id": plot.id,
                                "plot_name": plot.name,
                                "site_id": plot.site.id,
                                "site_name": plot.site.name,
                                "holding_campaign_id": plot.site.holding_campaign.id,
                                "campaign_id": plot.site.holding_campaign.campaign.id,
                                "campaign_name": plot.site.holding_campaign.campaign.name,
                                "holding_id": plot.site.holding_campaign.holding.id,
                                "holding_name": plot.site.holding_campaign.holding.name,
                            },
                            "id": 0,
                            "type": 3,
                        }
                    ],
                }
            },
        )

    def assertTileDoesNotContainPlot(self, tile, plot):
        self.assertTrue(
            "plot" not in tile
            or not any(
                [
                    feature["properties"]["id"] == plot.id
                    for feature in tile["plot"]["features"]
                ]
            )
        )

    def test_access_token_ok(self):
        tile = self.plot_function_source(
            access_token=self.access_token, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

        tile = self.plot_function_source(
            access_token=self.access_token, site_id=self.site.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

        tile = self.plot_function_source(
            access_token=self.access_token, plot_id=self.plot.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

    def test_no_access_token_no_session_key(self):
        tile = self.plot_function_source(holding_campaign_id=self.holding_campaign.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(site_id=self.site.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(plot_id=self.plot.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

    def test_access_token_not_related_party(self):
        self.user_related_party.delete()

        tile = self.plot_function_source(
            access_token=self.access_token, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            access_token=self.access_token, site_id=self.site.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            access_token=self.access_token, plot_id=self.plot.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

    def test_access_token_expired(self):
        access_token, refresh_token, at_hash, token = create_token(
            self.user, self.oidc_client, []
        )
        # Force the token to expire by overriding the expiry date
        token.access_expires_at -= timedelta(hours=24)
        token.save()

        tile = self.plot_function_source(
            access_token=access_token, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            access_token=access_token, site_id=self.site.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            access_token=access_token, plot_id=self.plot.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        token.delete()

    def test_access_token_wrong_token(self):
        tile = self.plot_function_source(
            access_token="ABCD", holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(access_token="ABCD", site_id=self.site.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(access_token="ABCD", plot_id=self.plot.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

    def test_access_token_user_inactive(self):
        self.user.is_active = False
        self.user.save()

        tile = self.plot_function_source(
            access_token=self.access_token, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            access_token=self.access_token, site_id=self.site.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            access_token=self.access_token, plot_id=self.plot.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

    # Session key

    def test_session_key_ok_staff(self):
        self.user.is_staff = True
        self.user.save()

        tile = self.plot_function_source(
            session_key=self.session_key, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, site_id=self.site.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, plot_id=self.plot.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, holding_campaign_id=self.holding_campaign_2.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot_2)

        tile = self.plot_function_source(
            session_key=self.session_key, site_id=self.site_2.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot_2)

        tile = self.plot_function_source(
            session_key=self.session_key, plot_id=self.plot_2.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot_2)

    def test_session_key_ok_superuser(self):
        self.user.is_superuser = True
        self.user.save()

        tile = self.plot_function_source(
            session_key=self.session_key, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, site_id=self.site.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, plot_id=self.plot.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, holding_campaign_id=self.holding_campaign_2.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot_2)

        tile = self.plot_function_source(
            session_key=self.session_key, site_id=self.site_2.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot_2)

        tile = self.plot_function_source(
            session_key=self.session_key, plot_id=self.plot_2.id
        )
        self.assertTileContainsOnlyPlot(tile, self.plot_2)

    def test_session_key_not_staff_not_superuser(self):
        tile = self.plot_function_source(
            session_key=self.session_key, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, site_id=self.site.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, plot_id=self.plot.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, holding_campaign_id=self.holding_campaign_2.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot_2)

        tile = self.plot_function_source(
            session_key=self.session_key, site_id=self.site_2.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot_2)

        tile = self.plot_function_source(
            session_key=self.session_key, plot_id=self.plot_2.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot_2)

    @override_settings(SESSION_COOKIE_AGE=1)
    def test_session_key_expired(self):
        self.user.session_set.all().delete()
        self.client.login(username=self.username, password=self.password)
        session_key = Session.objects.filter(user=self.user).first().session_key

        sleep(2)

        tile = self.plot_function_source(
            session_key=session_key, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(session_key=session_key, site_id=self.site.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(session_key=session_key, plot_id=self.plot.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

    def test_session_key_wrong_session_key(self):
        tile = self.plot_function_source(
            session_key="ABCD", holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(session_key="ABCD", site_id=self.site.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(session_key="ABCD", plot_id=self.plot.id)
        self.assertTileDoesNotContainPlot(tile, self.plot)

    def test_session_key_user_inactive(self):
        self.user.is_active = False
        self.user.save()

        tile = self.plot_function_source(
            session_key=self.session_key, holding_campaign_id=self.holding_campaign.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, site_id=self.site.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)

        tile = self.plot_function_source(
            session_key=self.session_key, plot_id=self.plot.id
        )
        self.assertTileDoesNotContainPlot(tile, self.plot)
