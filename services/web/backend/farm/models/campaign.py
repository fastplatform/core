from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.postgres.constraints import ExclusionConstraint
from django.core.exceptions import ValidationError
from django.contrib.postgres.fields import (
    DateTimeRangeField,
    RangeBoundary,
    RangeOperators,
)

from utils.models import GetAdminURLMixin
from utils.models import TranslatableModel


class TsTzRange(models.Func):
    function = "TSTZRANGE"
    output_field = DateTimeRangeField()


class Campaign(GetAdminURLMixin, TranslatableModel):
    """A growing season

    The campaign label/name should be correspond to the harvest year (eg for the 2018-2019 season,
    the season name should be 2019). It also corresponds to the year the CAP subsidies are applied for.
    """

    id = models.IntegerField(primary_key=True, verbose_name=_("identifier"))

    name = models.CharField(
        unique=True,
        max_length=20,
        null=False,
        blank=False,
        verbose_name=_("name"),
        help_text=_("The display name of the campaign"),
    )

    is_current = models.BooleanField(
        default=False,
        verbose_name=_("is current"),
        help_text=_(
            "Whether this campaign is the current one (only one campaign can be the current one at a time)"
        ),
    )

    start_at = models.DateTimeField(
        null=False,
        blank=False,
        verbose_name=_("start date"),
    )

    end_at = models.DateTimeField(
        null=False,
        blank=False,
        verbose_name=_("end date"),
    )

    def __str__(self):
        return self.name

    def clean(self):
        super().clean()

        # Ensure end date is strictly after start date
        if self.start_at > self.end_at:
            raise ValidationError(
                _("The start date of a campaign must be before its end date.")
            )

        # Ensure boundaries do not overlap an existing campaign
        condition = (
            models.Q(start_at__gt=models.F("end_at"), start_at__lt=self.end_at)
            | models.Q(start_at__gt=models.F("end_at"), end_at__gt=self.start_at)
            | models.Q(start_at__lt=self.end_at, end_at__gt=self.start_at)
        )
        if self.pk:
            condition &= ~models.Q(id=self.pk)

        conflicting_campaigns = Campaign.objects.filter(condition).values_list("name")

        if conflicting_campaigns:
            raise ValidationError(
                _(
                    "The start and end date of this campaign overlap one (or more) existing campaign(s)"
                )
                + ": "
                + ", ".join([c[0] for c in conflicting_campaigns])
            )

    def save(self, *args, **kwargs):

        # Ensure only one can be "current"
        if self.is_current:
            queryset = Campaign.objects.filter(is_current=True)
            if self.pk:
                queryset = queryset.exclude(pk=self.pk)
            queryset.update(is_current=False)

        super().save(*args, **kwargs)

    class Meta:
        db_table = "campaign"
        verbose_name = _("campaign")
        verbose_name_plural = _("campaigns")
        help_text = _(
            "An agricultural growing season, during which the geometric definition of a farm is fixed."
        )

        constraints = [
            models.UniqueConstraint(
                fields=["is_current"],
                condition=models.Q(is_current=True),
                name="campaign_is_current_only_one_can_be_true",
            ),
            ExclusionConstraint(
                name="campaign_start_at_end_at_cannot_overlap",
                expressions=(
                    (
                        # Campaigns cannot overlap but their boundaries can be equal
                        TsTzRange(
                            "start_at",
                            "end_at",
                            RangeBoundary(inclusive_lower=False, inclusive_upper=False),
                        ),
                        RangeOperators.OVERLAPS,
                    ),
                ),
            ),
            models.CheckConstraint(
                name="campaign_end_at_after_start_at",
                check=models.Q(start_at__lt=models.F("end_at")),
            ),
        ]
