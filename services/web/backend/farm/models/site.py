from django.contrib.gis.db import models
from django.db.models.expressions import RawSQL
from django.utils.translation import gettext_lazy as _

from utils.models import GetAdminURLMixin


class Site(GetAdminURLMixin, models.Model):
    """Site

    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    """

    id = models.AutoField(primary_key=True, editable=True, verbose_name=_("identifier"))

    authority_id = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        db_index=True,
        verbose_name=_("authority identifier"),
    )

    holding_campaign = models.ForeignKey(
        to="farm.HoldingCampaign",
        null=False,
        blank=False,
        related_name="sites",
        on_delete=models.CASCADE,
        verbose_name=_("farm campaign"),
    )

    name = models.CharField(
        max_length=256, null=True, blank=True, verbose_name=_("name")
    )

    def number_of_plots(self):
        return self.plots.count()

    number_of_plots.short_description = _("number of plots")

    def number_of_plots_with_fertilization_plan(self):
        """Number of plots covered by a fertilization plan in this campaign"""
        from farm.models import Plot

        return Plot.objects.filter(site=self, fertilization_plans__isnull=False).count()

    number_of_plots_with_fertilization_plan.short_description = _(
        "number of plots with a fertilization plan"
    )

    @property
    def centroid(self):
        return self.plots.annotate(
            centroid=RawSQL(
                "centroid", [], output_field=models.FloatField()
            )
        ).aggregate(
            centroid=models.functions.Centroid(models.aggregates.Union("geometry"))
        )[
            "centroid"
        ]

    @property
    def area(self):
        return self.plots.annotate(
            area=RawSQL("area", [], output_field=models.FloatField())
        ).aggregate(area=models.Sum("area"))["area"]

    def __str__(self):
        return self.name if self.name is not None else "-"

    class Meta:
        db_table = "site"
        verbose_name = _("site")
        verbose_name_plural = _("sites")

        constraints = (
            models.UniqueConstraint(
                name="site_authority_id_holding_campaign_id_unique",
                fields=("authority_id", "holding_campaign"),
            ),
        )
