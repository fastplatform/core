```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "farm.Holding <Farm>" as farm.Holding #dcd6f4 {
    farm
    ..
    A holding
    The whole area and all infrastructures included on it, covering the same or
    different "sites", under the control of an operator to perform
    agricultural or aquaculture activities. The holding includes one
    specialisation of ActivityComplex, ie. Activity. the values of
    ActivityType are expressed in conformity with the classification of the
    economic activity of the holding, according to the NACE rev. 2.0 coding
    Holding is a thematic extension of the generic Class “Activity Complex”
    shared with other thematic areas describing entities related with
    Economical Activities (Legal Entity Class – Business).
    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    --
    + id (CharField) - This identifier must be unique in FaST
    + name (CharField) - 
    + description (TextField) - 
    --
}


class "farm.HoldingCampaign <Farm>" as farm.HoldingCampaign #dcd6f4 {
    farm campaign
    ..
    HoldingCampaign(id, holding, campaign)
    --
    - id (AutoField) - 
    ~ holding (ForeignKey) - 
    ~ campaign (ForeignKey) - 
    --
}
farm.HoldingCampaign *-- farm.Holding
farm.HoldingCampaign *-- farm.Campaign


class "farm.UserRelatedParty <Farm>" as farm.UserRelatedParty #dcd6f4 {
    farm member
    ..
    A party (person) linked to an activity complex within a certain role
    --
    - id (AutoField) - 
    ~ holding (ForeignKey) - 
    + role (CharField) - 
    ~ user (ForeignKey) - 
    --
}
farm.UserRelatedParty *-- farm.Holding


class "farm.Campaign <Farm>" as farm.Campaign #dcd6f4 {
    campaign
    ..
    A growing season
    The campaign label/name should be correspond to the harvest year (eg for the
2018-2019 season,
    the season name should be 2019). It also corresponds to the year the CAP
subsidies are applied for.
    --
    + i18n (JSONField) - 
    + id (IntegerField) - 
    + name (CharField) - The display name of the campaign
    + is_current (BooleanField) - Whether this campaign is the current one (only
one campaign can be the current one at a time)
    + start_at (DateTimeField) - 
    + end_at (DateTimeField) - 
    --
}


class "farm.Site <Farm>" as farm.Site #dcd6f4 {
    site
    ..
    Site
    Origin: INSPIRE Annex III - Agricultural and Aquaculture Facilities
    --
    - id (AutoField) - 
    + authority_id (CharField) - 
    ~ holding_campaign (ForeignKey) - 
    + name (CharField) - 
    --
}
farm.Site *-- farm.HoldingCampaign


class "farm.Plot <Farm>" as farm.Plot #dcd6f4 {
    plot
    ..
    Plot(id, authority_id, site, name, geometry, valid_from, valid_to)
    --
    - id (AutoField) - 
    + authority_id (CharField) - 
    ~ site (ForeignKey) - 
    + name (CharField) - 
    + geometry (MultiPolygonField) - 
    + valid_from (DateField) - 
    + valid_to (DateField) - 
    --
}
farm.Plot *-- farm.Site


class "farm.PlotPlantVariety <Farm>" as farm.PlotPlantVariety #dcd6f4 {
    plot plant variety
    ..
    A plant variety cultivated on a plot
    Note:
    - as a Plot object is local to a Site, which is local to a HoldingCampaign,
    a PlotPlantVariety is also local to a HoldingCampaign
    - we allow multiple plant varieties on the same plot during the
    same campaign (ie a many-to-many relationship)
    --
    - id (AutoField) - 
    ~ plot (ForeignKey) - 
    ~ plant_variety (ForeignKey) - 
    + percentage (FloatField) - Percentage of the plot dedicated to this plant
variety
    ~ irrigation_method (ForeignKey) - 
    + crop_yield (FloatField) - 
    --
}
farm.PlotPlantVariety *-- farm.Plot


class "farm.Constraint <Farm>" as farm.Constraint #dcd6f4 {
    constraint
    ..
    The Model for any constraint: NVZ, Natura 2000, hydrology, etc.
    --
    - id (AutoField) - 
    + name (CharField) - 
    + description (JSONField) - 
    ~ plot (ForeignKey) - 
    --
}
farm.Constraint *-- farm.Plot


@enduml
```