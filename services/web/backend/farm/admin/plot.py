from django.conf import settings
from django.contrib import admin
from django.db import models
from django.utils.translation import gettext_lazy as _

from import_export.admin import ExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin

from farm.models import (
    Plot,
    PlotPlantVariety,
    Constraint,
)
from fieldbook.models import FertilizationPlan
from farm.resources import PlotPlantVarietyResource, PlotResource


class PlotPlantVarietyInline(admin.TabularInline):
    model = PlotPlantVariety
    fields = [
        "plot",
        "plant_variety",
        "_plant_species",
        "irrigation_method",
        "percentage",
    ]
    autocomplete_fields = [
        "plant_variety",
    ]
    readonly_fields = ["_plant_species"]
    extra = 0
    verbose_name = _("crop")
    verbose_name_plural = _("crops")

    def _plant_species(self, obj):
        return obj.plant_variety.plant_species

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields += ("_plant_species",)
        if obj:
            readonly_fields += ("plot",)
        return readonly_fields


class FertilizationPlanInline(admin.TabularInline):
    model = FertilizationPlan
    extra = 0
    fields = ["name", "algorithm", "parameters", "results", "updated_by", "updated_at"]
    readonly_fields = ["updated_at"]
    verbose_name = _("fertilization plan")
    verbose_name_plural = _("fertilization plans")


class ConstraintInline(admin.TabularInline):
    model = Constraint
    extra = 0
    verbose_name = _("constraint")
    verbose_name_plural = _("constraints")

    fields = ["id", "name", "description", "_summary"]
    readonly_fields = ["id", "_summary"]

    def _summary(self, obj):
        return obj.pretty()


class PlotAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = PlotResource
    list_display = (
        "name",
        "authority_id",
        "site",
        "_holding",
        "_campaign",
        "_plant_species",
    )
    ordering = ("name",)
    search_fields = [
        "name",
        "id",
        "authority_id",
        "site__name",
        "site__id",
        "site__holding_campaign__holding__id",
        "site__holding_campaign__holding__name",
    ]
    group = ("site__holding_campaign__holding", "site", "id")
    list_filter = ("site__holding_campaign__campaign",)

    inlines = [PlotPlantVarietyInline, FertilizationPlanInline, ConstraintInline]

    autocomplete_fields = ["site"]

    def get_readonly_fields(self, request, obj):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields += ("id", "_campaign", "_holding", "_area")
        return readonly_fields

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [
                None,
                {"fields": ["name", "authority_id", "site", "_holding", "_campaign"]},
            ],
            [_("Geographic information"), {"fields": ["geometry"]}],
        ]
        if obj:
            fieldsets += [[_("Statistics"), {"fields": ["_area"]}]]
        return fieldsets

    def _holding(self, obj):
        return obj.site.holding_campaign.holding

    _holding.admin_order_field = "holding"
    _holding.short_description = _("farm")

    def _campaign(self, obj):
        return obj.site.holding_campaign.campaign

    _campaign.admin_order_field = "campaign"
    _campaign.short_description = _("campaign")

    def _area(self, obj):
        if obj.area is not None and obj.area > 0:
            if obj.area < 10000:
                return f"{round(obj.area, 0)} m2"
            return f"{round(obj.area / 10000, 2)} ha"
        return "-"

    _area.short_description = _("area")

    def _plant_species(self, obj=None):
        if obj:
            return ", ".join(
                [
                    ppv.plant_variety.plant_species.name
                    for ppv in obj.plot_plant_varieties.all()
                ]
            )
        return ""

    _plant_species.short_description = _("plant species")

    # Select related objects to improve performance
    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .annotate(
                area=models.expressions.RawSQL(
                    "area", [], output_field=models.FloatField()
                )
            )
            .select_related(
                "site", "site__holding_campaign", "site__holding_campaign__holding"
            )
            .prefetch_related(
                "plot_plant_varieties",
                "plot_plant_varieties__plant_variety",
                "plot_plant_varieties__plant_variety__plant_species",
                "constraints",
                "soil_derived_objects",
                "soil_derived_objects__derived_observations",
                "soil_derived_objects__derived_observations__observed_property",
            )
        )

    # Map parameters
    # --------------
    geometry_fields = ["geometry"]

    def get_holding_campaign_id(self, obj):
        if obj:
            return obj.site.holding_campaign.id
        return None


admin_site.register(Plot, PlotAdmin)


class PlotPlantVarietyAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = PlotPlantVarietyResource
    raw_id_fields = ("plot",)
    fields = ["plot", "plant_variety", "irrigation_method", "percentage"]
    autocomplete_fields = ["plant_variety"]

    def _plant_species(self, obj):
        return obj.plant_variety.plant_species

    _plant_species.short_description = _("plant species")

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields += ("_plant_species",)
        if obj:
            readonly_fields += ("plot",)
        return readonly_fields


admin_site.register(PlotPlantVariety, PlotPlantVarietyAdmin)
