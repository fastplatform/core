from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farm', '0063_auto_20220126_1743')
    ]

    operations = [
        migrations.RunSQL((Path(__file__).parent / Path('create_plot_function_source.sql')).read_text())
    ]
