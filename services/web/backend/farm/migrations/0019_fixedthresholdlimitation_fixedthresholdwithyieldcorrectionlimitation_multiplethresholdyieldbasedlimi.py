# Generated by Django 3.0.7 on 2020-07-30 08:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fieldbook', '0006_auto_20200709_1659'),
        ('farm', '0018_auto_20200730_0851'),
    ]

    operations = [
        migrations.CreateModel(
            name='MultipleThresholdYieldBasedLimitation',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('crop_yield', models.FloatField(help_text='Yield in kilograms', verbose_name='yield')),
                ('yield_type', models.CharField(choices=[('PLANNED', 'Planned yield'), ('AVERAGE', 'Indicated average yield')], max_length=16, verbose_name='yield type')),
                ('unit', models.CharField(choices=[('T_HA', 't/ha'), ('MOISTURE', 't/ha of commercial moisture grain'), ('DRY_MATTER', 't/ha of dry matter')], max_length=16, verbose_name='unit')),
                ('crop_eng', models.CharField(blank=True, max_length=128, null=True, verbose_name='crop name in english')),
                ('crop_original_tongue', models.CharField(blank=True, max_length=128, null=True, verbose_name='crop name in original tongue')),
                ('limitation', models.FloatField(help_text='limitation of nitrogen to spread', verbose_name='nitrogen limitation')),
                ('precisions', models.CharField(blank=True, max_length=2048, null=True, verbose_name='precisions on mandatory limitations')),
                ('recommendations', models.CharField(blank=True, max_length=2048, null=True, verbose_name='other recommendations (not mandatory)')),
                ('plant_species', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fieldbook.PlantSpecies', verbose_name='plant species')),
            ],
            options={
                'verbose_name': 'nitrogen limitation in Estonia',
                'verbose_name_plural': 'nitrogen limitations in Estonia',
                'db_table': 'nitrogen_limitation_estonia',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FixedThresholdWithYieldCorrectionLimitation',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('crop_yield', models.FloatField(help_text='Yield in kilograms', verbose_name='yield')),
                ('yield_type', models.CharField(choices=[('PLANNED', 'Planned yield'), ('AVERAGE', 'Indicated average yield')], max_length=16, verbose_name='yield type')),
                ('unit', models.CharField(choices=[('T_HA', 't/ha'), ('MOISTURE', 't/ha of commercial moisture grain'), ('DRY_MATTER', 't/ha of dry matter')], max_length=16, verbose_name='unit')),
                ('crop_eng', models.CharField(blank=True, max_length=128, null=True, verbose_name='crop name in english')),
                ('crop_original_tongue', models.CharField(blank=True, max_length=128, null=True, verbose_name='crop name in original tongue')),
                ('limitation', models.FloatField(help_text='limitation of nitrogen to spread', verbose_name='nitrogen limitation')),
                ('precisions', models.CharField(blank=True, max_length=2048, null=True, verbose_name='precisions on mandatory limitations')),
                ('recommendations', models.CharField(blank=True, max_length=2048, null=True, verbose_name='other recommendations (not mandatory)')),
                ('correction_factor', models.FloatField(help_text='additional quantity of fertilizer to apply for each additional ton of yield', verbose_name='correction factor')),
                ('plant_species', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fieldbook.PlantSpecies', verbose_name='plant species')),
            ],
            options={
                'verbose_name': 'fixed threshold with yield correction for nitrogen limitation',
                'verbose_name_plural': 'fixed thresholds with yield corrections for nitrogen limitations',
                'db_table': 'fixed_threshold_with_yield_correction_limitation',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FixedThresholdLimitation',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('crop_yield', models.FloatField(help_text='Yield in kilograms', verbose_name='yield')),
                ('yield_type', models.CharField(choices=[('PLANNED', 'Planned yield'), ('AVERAGE', 'Indicated average yield')], max_length=16, verbose_name='yield type')),
                ('unit', models.CharField(choices=[('T_HA', 't/ha'), ('MOISTURE', 't/ha of commercial moisture grain'), ('DRY_MATTER', 't/ha of dry matter')], max_length=16, verbose_name='unit')),
                ('crop_eng', models.CharField(blank=True, max_length=128, null=True, verbose_name='crop name in english')),
                ('crop_original_tongue', models.CharField(blank=True, max_length=128, null=True, verbose_name='crop name in original tongue')),
                ('limitation', models.FloatField(help_text='limitation of nitrogen to spread', verbose_name='nitrogen limitation')),
                ('precisions', models.CharField(blank=True, max_length=2048, null=True, verbose_name='precisions on mandatory limitations')),
                ('recommendations', models.CharField(blank=True, max_length=2048, null=True, verbose_name='other recommendations (not mandatory)')),
                ('plant_species', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fieldbook.PlantSpecies', verbose_name='plant species')),
            ],
            options={
                'verbose_name': 'nitrogen limitation in Andalucia',
                'verbose_name_plural': 'nitrogen limitations in Andalucia',
                'db_table': 'nitrogen_limitation_andalucia',
                'abstract': False,
            },
        ),
    ]
