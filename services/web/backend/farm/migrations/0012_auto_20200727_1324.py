# Generated by Django 3.0.7 on 2020-07-27 13:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0005_address'),
        ('farm', '0011_auto_20200727_1303'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='holdingaddress',
            name='administrative_unit_level_1',
        ),
        migrations.RemoveField(
            model_name='holdingaddress',
            name='administrative_unit_level_2',
        ),
        migrations.RemoveField(
            model_name='holdingaddress',
            name='administrative_unit_level_3',
        ),
        migrations.RemoveField(
            model_name='holdingaddress',
            name='administrative_unit_level_4',
        ),
        migrations.RemoveField(
            model_name='holdingaddress',
            name='administrative_unit_level_5',
        ),
        migrations.RemoveField(
            model_name='holdingaddress',
            name='administrative_unit_level_6',
        ),
        migrations.RemoveField(
            model_name='holdingaddress',
            name='holding',
        ),
        migrations.RemoveField(
            model_name='plotaddress',
            name='administrative_unit_level_1',
        ),
        migrations.RemoveField(
            model_name='plotaddress',
            name='administrative_unit_level_2',
        ),
        migrations.RemoveField(
            model_name='plotaddress',
            name='administrative_unit_level_3',
        ),
        migrations.RemoveField(
            model_name='plotaddress',
            name='administrative_unit_level_4',
        ),
        migrations.RemoveField(
            model_name='plotaddress',
            name='administrative_unit_level_5',
        ),
        migrations.RemoveField(
            model_name='plotaddress',
            name='administrative_unit_level_6',
        ),
        migrations.RemoveField(
            model_name='siteaddress',
            name='administrative_unit_level_1',
        ),
        migrations.RemoveField(
            model_name='siteaddress',
            name='administrative_unit_level_2',
        ),
        migrations.RemoveField(
            model_name='siteaddress',
            name='administrative_unit_level_3',
        ),
        migrations.RemoveField(
            model_name='siteaddress',
            name='administrative_unit_level_4',
        ),
        migrations.RemoveField(
            model_name='siteaddress',
            name='administrative_unit_level_5',
        ),
        migrations.RemoveField(
            model_name='siteaddress',
            name='administrative_unit_level_6',
        ),
        migrations.RemoveField(
            model_name='siteaddress',
            name='site',
        ),
        migrations.AddField(
            model_name='agribuilding',
            name='address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='common.Address'),
        ),
        migrations.AddField(
            model_name='holding',
            name='address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='common.Address'),
        ),
        migrations.AddField(
            model_name='site',
            name='address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='common.Address'),
        ),
        migrations.AlterField(
            model_name='plot',
            name='address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='common.Address'),
        ),
        migrations.DeleteModel(
            name='AgriBuildingAddress',
        ),
        migrations.DeleteModel(
            name='HoldingAddress',
        ),
        migrations.DeleteModel(
            name='PlotAddress',
        ),
        migrations.DeleteModel(
            name='SiteAddress',
        ),
    ]
