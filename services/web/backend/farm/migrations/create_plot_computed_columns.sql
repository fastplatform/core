ALTER TABLE plot
ADD COLUMN area FLOAT GENERATED ALWAYS AS (
        ST_Area(
            ST_Transform(geometry, 3035)
        )
    ) STORED;

ALTER TABLE plot
ADD COLUMN perimeter FLOAT GENERATED ALWAYS AS (
        ST_Perimeter(
            ST_Transform(geometry, 3035)
        )
    ) STORED;

ALTER TABLE plot
ADD COLUMN centroid Geometry(Point, 4258) GENERATED ALWAYS AS (
        ST_Centroid(geometry)
    ) STORED;

ALTER TABLE plot
ADD COLUMN bbox Geometry(Polygon, 4258) GENERATED ALWAYS AS (
        ST_Envelope(geometry)
    ) STORED;

ALTER TABLE plot
ADD COLUMN bbox_leaflet Geometry(Polygon, 4326) GENERATED ALWAYS AS (
        ST_Envelope(
            ST_Transform(geometry, 4326)
        )
    ) STORED;

ALTER TABLE plot
ADD COLUMN thumbnail TEXT GENERATED ALWAYS AS (
        '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="300" width="300" preserveAspectRatio="xMidYMid meet" viewBox="'
        || ST_XMin(geometry)::TEXT
        || ' '
        || (-1 * ST_YMax(geometry))::TEXT
        || ' ' || (ST_XMax(geometry) - ST_XMin(geometry))::TEXT
        || ' ' || (ST_YMax(geometry) - ST_YMin(geometry))::TEXT
        || '"><path d="'
        || ST_AsSVG(ST_Simplify(geometry, 0.000001, TRUE), 1, 6)
        || '" class="svg-geometry" /></svg>'
    ) STORED;
