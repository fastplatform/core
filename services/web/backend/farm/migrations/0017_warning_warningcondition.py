# Generated by Django 3.0.7 on 2020-07-27 15:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fieldbook', '0006_auto_20200709_1659'),
        ('farm', '0016_auto_20200727_1355'),
    ]

    operations = [
        migrations.CreateModel(
            name='WarningCondition',
            fields=[
                ('id', models.CharField(max_length=128, primary_key=True, serialize=False, verbose_name='Identifier')),
                ('management_restriction_or_regulation_zone', models.CharField(choices=[('NVZ', 'In management restriction or regulation zone'), ('NO_NVZ', 'Not in management restriction or regulation zone'), ('BOTH', 'Everywhere')], help_text='Does the condition apply on management restriction or regulation zone', max_length=8, verbose_name='management restriction or regulation zone')),
                ('distance_water', models.IntegerField(blank=True, help_text='distance from a water body in meter', null=True, verbose_name='distance from water')),
                ('fertilizer_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='fieldbook.FertilizerType', verbose_name='fertilizer type')),
                ('plant_species', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='fieldbook.PlantSpecies', verbose_name='plant species')),
                ('plant_species_group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='fieldbook.PlantSpeciesGroup', verbose_name='plant species group')),
                ('plant_variety', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='fieldbook.PlantVariety', verbose_name='plant variety')),
            ],
            options={
                'verbose_name': 'warning condition',
                'verbose_name_plural': 'warning conditions',
                'db_table': 'warning_condition',
            },
        ),
        migrations.CreateModel(
            name='Warning',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='identifier')),
                ('text', models.TextField(help_text='Text to display in warning popup', verbose_name='warning text')),
                ('condition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='farm.WarningCondition', verbose_name='warning condition')),
            ],
            options={
                'verbose_name': 'warning',
                'verbose_name_plural': 'warnings',
                'db_table': 'warning',
            },
        ),
    ]
