from common.resources import ModelResourceWithColumnIds

from farm.models import (
    Holding,
    HoldingCampaign,
    Site,
    Plot,
    Campaign,
    UserRelatedParty,
    PlotPlantVariety,
)


class CampaignResource(ModelResourceWithColumnIds):
    class Meta:
        model = Campaign


class HoldingResource(ModelResourceWithColumnIds):
    class Meta:
        model = Holding


class SiteResource(ModelResourceWithColumnIds):
    class Meta:
        model = Site


class PlotResource(ModelResourceWithColumnIds):
    class Meta:
        model = Plot


class HoldingCampaignResource(ModelResourceWithColumnIds):
    class Meta:
        model = HoldingCampaign


class UserRelatedPartyResource(ModelResourceWithColumnIds):
    class Meta:
        model = UserRelatedParty


class PlotPlantVarietyResource(ModelResourceWithColumnIds):
    class Meta:
        model = PlotPlantVariety
