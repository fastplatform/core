# Generated by Django 3.0.5 on 2020-06-25 14:10

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('configuration', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configuration',
            name='management_restriction_or_regulation_zone_overlay_style',
            field=django.contrib.postgres.fields.jsonb.JSONField(verbose_name='style for management restriction or regulation zones overlay'),
        ),
        migrations.AlterField(
            model_name='configuration',
            name='protected_sites_overlay_style',
            field=django.contrib.postgres.fields.jsonb.JSONField(verbose_name='style for protected sites overlay'),
        ),
    ]
