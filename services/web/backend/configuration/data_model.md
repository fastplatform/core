```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "configuration.Configuration <Configuration>" as configuration.Configuration #f4d6d7 {
    configuration
    ..
    Configuration(id, backend_leaflet_default_center_latitude,
backend_leaflet_default_center_longitude, backend_leaflet_default_zoom,
backend_leaflet_min_zoom, backend_leaflet_max_zoom, backend_leaflet_map_width,
backend_leaflet_map_height, backend_leaflet_display_raw,
backend_leaflet_map_srid, protected_site_overlay_style,
hydrography_overlay_style, soil_overlay_style,
management_restriction_or_regulation_zone_overlay_style,
agri_plot_overlay_style, plots_overlay_style, rgb_ndvi_overlay_style,
display_surface_unit_name, display_surface_unit_short_name,
display_surface_unit_ratio_to_hectare)
    --
    + id (CharField) - 
    + backend_leaflet_default_center_latitude (FloatField) - Default center
latitude for maps in the admin backend
    + backend_leaflet_default_center_longitude (FloatField) - Default center
longitude for maps in the admin backend
    + backend_leaflet_default_zoom (IntegerField) - Default zoom level of maps in
the admin backend
    + backend_leaflet_min_zoom (IntegerField) - Minimum zoom level of maps in the
admin backend
    + backend_leaflet_max_zoom (IntegerField) - Maximum zoom level of maps in the
admin backend
    + backend_leaflet_map_width (CharField) - Default width of maps in the admin
backend, in CSS units
    + backend_leaflet_map_height (CharField) - Default height of maps in the admin
backend, in CSS units
    + backend_leaflet_display_raw (BooleanField) - Whether or not to display the
current geometry as WKT in the admin backend
    + backend_leaflet_map_srid (IntegerField) - SRID of the maps in the admin
backend
    + protected_site_overlay_style (JSONField) - 
    + hydrography_overlay_style (JSONField) - 
    + soil_overlay_style (JSONField) - 
    + management_restriction_or_regulation_zone_overlay_style (JSONField) - 
    + agri_plot_overlay_style (JSONField) - 
    + plots_overlay_style (JSONField) - 
    + rgb_ndvi_overlay_style (JSONField) - 
    + display_surface_unit_name (CharField) - Will default to 'hectare' if not set
    + display_surface_unit_short_name (CharField) - Will default to 'ha' if not set
    + display_surface_unit_ratio_to_hectare (FloatField) - Will default to 1.0 if
not set
    --
}


class "configuration.MapBaseLayer <Configuration>" as configuration.MapBaseLayer #f4d6d7 {
    map base layer
    ..
    MapBaseLayer(i18n, id, title, min_zoom, max_zoom, is_active_in_farmer_app,
is_active_in_admin_portal, layer_type, url, max_native_zoom, attribution,
layers, proj_format, proj_name, proj_wkt, wms_version, styles, is_transparent,
display)
    --
    + i18n (JSONField) - 
    + id (CharField) - Can contain only lowercase letters (a-z), digits (0-9), dash
(-) and underscore (_).
    + title (CharField) - 
    + min_zoom (IntegerField) - Below this zoom level, the layer will not be
displayed.
    + max_zoom (IntegerField) - Above this zoom level, the layer will not be
displayed.
    + is_active_in_farmer_app (BooleanField) - Whether or not to display this layer
in the layer selection menu in the Farmer (mobile) App
    + is_active_in_admin_portal (BooleanField) - Whether or not to display this
layer in the layer selection menu in the Administration Portal
    + layer_type (CharField) - 
    + url (CharField) - 
    + max_native_zoom (IntegerField) - Maximum zoom level provided by the source
    + attribution (CharField) - 
    + layers (CharField) - Comma-separated list of source WMS layer(s) to display
    + proj_format (CharField) - 
    + proj_name (CharField) - Authority name of the source projection, in EPSG:XXXX
format
    + proj_wkt (TextField) - 
    + wms_version (CharField) - 
    + styles (CharField) - Comma-separated list of styles, leave blank to use
default style
    + is_transparent (BooleanField) - Whether the selected layer(s) contains any
alpha transparency (alpha channel)
    + display (BooleanField) - Whether or not this is the default base layer. Only
one base layer can be default at a time.
    --
}


class "configuration.MapOverlay <Configuration>" as configuration.MapOverlay #f4d6d7 {
    map overlay
    ..
    MapOverlay(i18n, id, title, min_zoom, max_zoom, is_active_in_farmer_app,
is_active_in_admin_portal, layer_type, url, max_native_zoom, attribution,
layers, proj_format, proj_name, proj_wkt, wms_version, styles, is_transparent,
layer_order, display)
    --
    + i18n (JSONField) - 
    + id (CharField) - Can contain only lowercase letters (a-z), digits (0-9), dash
(-) and underscore (_).
    + title (CharField) - 
    + min_zoom (IntegerField) - Below this zoom level, the layer will not be
displayed.
    + max_zoom (IntegerField) - Above this zoom level, the layer will not be
displayed.
    + is_active_in_farmer_app (BooleanField) - Whether or not to display this layer
in the layer selection menu in the Farmer (mobile) App
    + is_active_in_admin_portal (BooleanField) - Whether or not to display this
layer in the layer selection menu in the Administration Portal
    + layer_type (CharField) - 
    + url (CharField) - 
    + max_native_zoom (IntegerField) - Maximum zoom level provided by the source
    + attribution (CharField) - 
    + layers (CharField) - Comma-separated list of source WMS layer(s) to display
    + proj_format (CharField) - 
    + proj_name (CharField) - Authority name of the source projection, in EPSG:XXXX
format
    + proj_wkt (TextField) - 
    + wms_version (CharField) - 
    + styles (CharField) - Comma-separated list of styles, leave blank to use
default style
    + is_transparent (BooleanField) - Whether the selected layer(s) contains any
alpha transparency (alpha channel)
    + layer_order (IntegerField) - Stacking order of the layer
    + display (BooleanField) - Whether this overlay is displayed by default.
    --
}


@enduml
```