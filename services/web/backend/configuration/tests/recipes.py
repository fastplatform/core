from model_bakery.recipe import Recipe

from configuration.models import Configuration, MapBaseLayer, MapOverlay

configuration_recipe = Recipe(
    Configuration,
    id="CONFIG",
    protected_site_overlay_style={"radius": 5},
    hydrography_overlay_style={"radius": 5},
    soil_overlay_style={"radius": 5},
    management_restriction_or_regulation_zone_overlay_style={"radius": 5},
    agri_plot_overlay_style={"radius": 5},
    plots_overlay_style={"radius": 5},
    rgb_ndvi_overlay_style={"radius": 5},
)

map_base_layer_recipe = Recipe(
    MapBaseLayer, url="https://test-url.net", display=True, min_zoom=1, max_zoom=2
)

map_overlay_recipe = Recipe(
    MapOverlay, url="https://test-url.net", display=True, min_zoom=1, max_zoom=2
)
