from django.test import TestCase, override_settings

from utils.tests.test_admin import AdminTestCaseMixin

from common.models import UnitOfMeasure

from common.tests.recipes import unit_of_measure_recipe


@override_settings(AXES_ENABLED=False)
class UnitOfMeasureAdminTestCase(AdminTestCaseMixin, TestCase):
    model = UnitOfMeasure
    recipe = unit_of_measure_recipe
