from model_bakery.recipe import Recipe

from common.models import UnitOfMeasure


unit_of_measure_recipe = Recipe(UnitOfMeasure)
