# Generated by Django 3.0.7 on 2020-10-15 15:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0006_auto_20201015_1502'),
    ]

    operations = [
        migrations.RunSQL(
            sql='ALTER TABLE "administrative_unit" DROP CONSTRAINT "administrative_unit_parent_id_9ab06bb3_fk_administr";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "administrative_unit" ADD CONSTRAINT "administrative_unit_parent_id_9ab06bb3_fk_administr" FOREIGN KEY ("parent_id") REFERENCES "administrative_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "administrative_unit" DROP CONSTRAINT "administrative_unit_country_id_8a7a171a_fk_country_iso_code";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "administrative_unit" ADD CONSTRAINT "administrative_unit_country_id_8a7a171a_fk_country_iso_code" FOREIGN KEY ("country_id") REFERENCES "country" ("iso_code") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" DROP CONSTRAINT "address_administrative_unit__92a6378c_fk_administr";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" ADD CONSTRAINT "address_administrative_unit__92a6378c_fk_administr" FOREIGN KEY ("administrative_unit_level_1_id") REFERENCES "administrative_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" DROP CONSTRAINT "address_administrative_unit__efa0fc76_fk_administr";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" ADD CONSTRAINT "address_administrative_unit__efa0fc76_fk_administr" FOREIGN KEY ("administrative_unit_level_2_id") REFERENCES "administrative_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" DROP CONSTRAINT "address_administrative_unit__5af02036_fk_administr";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" ADD CONSTRAINT "address_administrative_unit__5af02036_fk_administr" FOREIGN KEY ("administrative_unit_level_3_id") REFERENCES "administrative_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" DROP CONSTRAINT "address_administrative_unit__6a184bf4_fk_administr";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" ADD CONSTRAINT "address_administrative_unit__6a184bf4_fk_administr" FOREIGN KEY ("administrative_unit_level_4_id") REFERENCES "administrative_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" DROP CONSTRAINT "address_administrative_unit__ec2d24b1_fk_administr";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" ADD CONSTRAINT "address_administrative_unit__ec2d24b1_fk_administr" FOREIGN KEY ("administrative_unit_level_5_id") REFERENCES "administrative_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" DROP CONSTRAINT "address_administrative_unit__fb59df27_fk_administr";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "address" ADD CONSTRAINT "address_administrative_unit__fb59df27_fk_administr" FOREIGN KEY ("administrative_unit_level_6_id") REFERENCES "administrative_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "unit_of_measure_linear_conversion" DROP CONSTRAINT "unit_of_measure_line_unit_from_id_a9533caa_fk_unit_of_m";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "unit_of_measure_linear_conversion" ADD CONSTRAINT "unit_of_measure_line_unit_from_id_a9533caa_fk_unit_of_m" FOREIGN KEY ("unit_from_id") REFERENCES "unit_of_measure" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "unit_of_measure_linear_conversion" DROP CONSTRAINT "unit_of_measure_line_unit_to_id_af5d9c82_fk_unit_of_m";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "unit_of_measure_linear_conversion" ADD CONSTRAINT "unit_of_measure_line_unit_to_id_af5d9c82_fk_unit_of_m" FOREIGN KEY ("unit_to_id") REFERENCES "unit_of_measure" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
    ]
