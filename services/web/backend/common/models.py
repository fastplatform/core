from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from utils.models import GetAdminURLMixin, TranslatableModel, validate_latin1


# Abstract classes
# ----------------


class AbstractCodeList(GetAdminURLMixin, TranslatableModel, models.Model):
    """A list of codes as defined in the Inspire conceptual model

    Each item has an id (which is usually an URL for Inspire objects),
    a label, and reference to a parent
    """

    id = models.CharField(
        max_length=256,
        primary_key=True,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    label = models.CharField(
        max_length=256, null=False, blank=False, verbose_name=_("label")
    )

    definition = models.CharField(
        null=True, blank=True, max_length=8192, verbose_name=_("definition")
    )

    description = models.TextField(null=True, blank=True, verbose_name=_("description"))

    parent = models.ForeignKey(
        to="self",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_("parent"),
    )

    def __str__(self):
        return self.label

    class Meta:
        abstract = True


# Units of measure
# ----------------


class AbstractUnitOfMeasure(GetAdminURLMixin, TranslatableModel, models.Model):
    """A unit of measure of a phenomenon

    As defined in Inspire
    """

    id = models.CharField(
        max_length=256,
        primary_key=True,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    name = models.CharField(
        max_length=256, verbose_name=_("name"), null=False, blank=False
    )

    symbol = models.CharField(
        max_length=256, verbose_name=_("symbol"), null=False, blank=False
    )

    def __str__(self):
        if self.symbol != "-":
            return f"{self.name} ({self.symbol})"
        else:
            return self.name

    class Meta:
        abstract = True


class UnitOfMeasure(AbstractUnitOfMeasure):
    class Meta:
        db_table = "unit_of_measure"
        verbose_name = _("unit of measure")
        verbose_name_plural = _("units of measure")
