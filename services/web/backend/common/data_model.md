```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "common.UnitOfMeasure <Reference datasets>" as common.UnitOfMeasure #d6d7f4 {
    unit of measure
    ..
    UnitOfMeasure(i18n, id, name, symbol)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - 
    + symbol (CharField) - 
    --
}


@enduml
```