from django.apps import apps
from django.urls import NoReverseMatch, reverse
from django.contrib import admin
from django.utils.text import capfirst
from django.utils.translation import gettext_lazy as _
from django.template.response import TemplateResponse
from django.views.decorators.cache import never_cache

from fastplatform.routers import FaSTRouter


class FaSTAdminSite(admin.AdminSite):
    name = "admin_site"
    site_header = _("FaST")
    site_title = _("FaST Administration")
    site_url = None
    index_title = _("Administration Portal")

    index_template = "admin/index.html"
    app_index_template = "admin/app_index.html"

    enable_nav_sidebar = False

    def _build_app_dict(self, request, label=None):
        """
        Build the app dictionary. The optional `label` parameter filters models
        of a specific app.
        """
        app_dict = {}

        if label:
            models = {
                m: m_a
                for m, m_a in self._registry.items()
                if m._meta.app_label == label
            }
        else:
            models = self._registry

        for model, model_admin in models.items():
            app_label = model._meta.app_label

            has_module_perms = model_admin.has_module_permission(request)
            if not has_module_perms:
                continue

            perms = model_admin.get_model_perms(request)

            # Check whether user has any perm for this module.
            # If so, add the module to the model_list.
            if True not in perms.values():
                continue

            info = (app_label, model._meta.model_name)
            help_text = getattr(model._meta, "help_text", None)
            hide_from_dashboard = getattr(model_admin, "hide_from_dashboard", False)

            model_dict = {
                "name": capfirst(model._meta.verbose_name_plural),
                "object_name": model._meta.object_name,
                "perms": perms,
                "admin_url": None,
                "add_url": None,
                "model": model,
                "help_text": help_text,
                "hide_from_dashboard": hide_from_dashboard,
            }
            if perms.get("change") or perms.get("view"):
                model_dict["view_only"] = not perms.get("change")
                try:
                    model_dict["admin_url"] = reverse(
                        "admin:%s_%s_changelist" % info, current_app=self.name
                    )
                except NoReverseMatch:
                    pass
            if perms.get("add"):
                try:
                    model_dict["add_url"] = reverse(
                        "admin:%s_%s_add" % info, current_app=self.name
                    )
                except NoReverseMatch:
                    pass

            if app_label in app_dict:
                app_dict[app_label]["models"].append(model_dict)
            else:
                app_config = apps.get_app_config(app_label)
                help_text = (
                    app_config.help_text if hasattr(app_config, "help_text") else None
                )
                app_dict[app_label] = {
                    "name": app_config.verbose_name,
                    "help_text": help_text,
                    "app_label": app_label,
                    "app_url": reverse(
                        "admin:app_list",
                        kwargs={"app_label": app_label},
                        current_app=self.name,
                    ),
                    "has_module_perms": has_module_perms,
                    "models": [model_dict],
                }

        for app_label in app_dict.keys():
            app_dict[app_label]["hide_from_dashboard"] = all(
                [
                    model["hide_from_dashboard"]
                    for model in app_dict[app_label]["models"]
                ]
            )

        if label:
            return app_dict.get(label)
        return app_dict

    @never_cache
    def index_all_objects(self, request, extra_context=None):
        """
        Display the main admin index page, which lists all of the installed
        apps that have been registered in this site.
        """
        app_list = self.get_app_list(request)
        router = FaSTRouter()

        db_dict = {}
        for app in app_list:
            for model_dict in app["models"]:
                db = router.db_for_read(model_dict["model"])
                if db not in db_dict:
                    db_dict[db] = []
                db_dict[db] += [model_dict]

        for db in db_dict.keys():
            db_dict[db] = sorted(db_dict[db], key=lambda m: m["model"].__name__)

        db_list = [{"db": db, "models": db_dict[db]} for db in db_dict.keys()]

        context = {
            **self.each_context(request),
            "title": self.index_title,
            "db_list": db_list,
            **(extra_context or {}),
        }

        request.current_app = self.name

        return TemplateResponse(request, "admin/index_all_objects.html", context)


admin_site = FaSTAdminSite()
