from django.contrib.admin.apps import AdminConfig

class FaSTAdminConfig(AdminConfig):
    default_site = 'fastplatform.admin.FaSTAdminSite'