import os

AUTH_USER_MODEL = "authentication.User"

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

AUTHENTICATION_BACKENDS = (
    "axes.backends.AxesBackend",
    "django.contrib.auth.backends.ModelBackend",
)

LOCAL_CONNECTOR = {"ID": "django", "Name": "Local", "URL": "", "Type": "local"}

# Change the Django login url variable to use to authentication endpoint
LOGIN_URL = "/authentication"

LOGIN_REDIRECT_URL = "/authentication/callback"

# URL of the site on which we delegate authentication
# AUTHENTICATION_LOGIN_URL = os.environ.get('AUTHENTICATION_LOGIN_URL','/authentication')

LOGOUT_REDIRECT_URL = LOGIN_URL

SESSION_ENGINE = "user_sessions.backends.db"

AUTHENTICATION_API_GATEWAY_SERVICE_KEY = os.environ.get(
    "AUTHENTICATION_API_GATEWAY_SERVICE_KEY"
)
