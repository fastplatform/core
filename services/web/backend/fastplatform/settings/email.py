import os

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

EMAIL_USE_SSL = os.environ.get("EMAIL_USE_SSL", "TRUE").upper() == "TRUE"

EMAIL_HOST = os.environ.get("SMTP_HOST")

EMAIL_PORT = os.environ.get("SMTP_PORT")

EMAIL_HOST_USER = os.environ.get("SMTP_LOGIN")

EMAIL_HOST_PASSWORD = os.environ.get("SMTP_PASSWORD")

DEFAULT_FROM_EMAIL = os.environ.get("SMTP_SENDER")
