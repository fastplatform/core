import os

API_GATEWAY_FASTPLATFORM_URL = os.environ.get(
    "API_GATEWAY_FASTPLATFORM_URL", "https://localhost.fastplatform.eu:48087/v1/graphql"
)
API_GATEWAY_FASTPLATFORM_PUBLIC_URL = os.environ.get(
    "API_GATEWAY_FASTPLATFORM_PUBLIC_URL",
    "https://localhost.fastplatform.eu:48087/v1/graphql",
)
API_GATEWAY_SERVICE_KEY = os.environ.get("API_GATEWAY_SERVICE_KEY")
