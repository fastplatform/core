import os
from pathlib import Path

from fastplatform.settings import BASE_DIR

STATICFILES_DIRS = [Path(BASE_DIR) / "static", Path(BASE_DIR) / "utils" / "static"]
STATIC_URL = os.environ.get("STATIC_URL", "/static/")

# S3 storage
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html

STATIC_AWS_ACCESS_KEY_ID = os.environ.get("STATIC_AWS_ACCESS_KEY_ID")
STATIC_AWS_SECRET_ACCESS_KEY = os.environ.get("STATIC_AWS_SECRET_ACCESS_KEY")
STATIC_AWS_STORAGE_BUCKET_NAME = os.environ.get("STATIC_AWS_STORAGE_BUCKET_NAME")
STATIC_AWS_S3_ENDPOINT_URL = os.environ.get("STATIC_AWS_S3_ENDPOINT_URL")
STATIC_AWS_S3_OBJECT_PARAMETERS = {
    "CacheControl": "max-age={}".format(
        os.environ.get("STATIC_AWS_S3_OBJECT_PARAMETERS_MAX_AGE", "86400")
    ),
}
STATIC_AWS_LOCATION = os.environ.get("STATIC_AWS_LOCATION")
STATIC_AWS_QUERYSTRING_AUTH = (
    os.environ.get("STATIC_AWS_QUERYSTRING_AUTH", "FALSE").upper() == "TRUE"
)
STATIC_AWS_IS_GZIPPED = (
    os.environ.get("STATIC_AWS_IS_GZIPPED", "TRUE").upper() == "TRUE"
)
STATIC_AWS_DEFAULT_ACL = None

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATICFILES_STORAGE = os.environ.get(
    "STATICFILES_STORAGE", "django.contrib.staticfiles.storage.StaticFilesStorage"
)
