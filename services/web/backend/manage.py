#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

from opentelemetry import trace
from opentelemetry.exporter.zipkin.json import Protocol
from opentelemetry.exporter.zipkin.json import ZipkinExporter
from opentelemetry.instrumentation.django import DjangoInstrumentor
from opentelemetry.instrumentation.psycopg2 import Psycopg2Instrumentor
from opentelemetry.propagate import set_global_textmap
from opentelemetry.propagators.b3 import B3SingleFormat
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace.sampling import ParentBased, TraceIdRatioBased

def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fastplatform.settings')

    # Debuging instrumentation
    os.environ.setdefault("DJANGO_REMOTE_DEBUG", "False")
    if os.environ.get("DJANGO_REMOTE_DEBUG").upper() == "TRUE":
        import debugpy
        debugpy.listen(5678)

    # Tracing instrumentation
    os.environ.setdefault("OTEL_PYTHON_DJANGO_INSTRUMENT", "True")
    zipkin_exporter = ZipkinExporter(
        version= Protocol.V2,
        endpoint=os.environ.get(
            'OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT', 'http://127.0.0.1:9411/api/v2/spans'),
    )

    OPENTELEMETRY_SAMPLING_RATIO = float(os.environ.get('OPENTELEMETRY_SAMPLING_RATIO', 0.1))
    sampler = TraceIdRatioBased(OPENTELEMETRY_SAMPLING_RATIO)
    
    trace.set_tracer_provider(
        TracerProvider(
                resource=Resource.create(
                    {
                        SERVICE_NAME: os.environ.get(
                            'OPENTELEMETRY_SERVICE_NAME', 'web-backend')
                    }
                ),
                sampler=ParentBased(sampler)
            )
    )
    trace.get_tracer_provider().add_span_processor(
        BatchSpanProcessor(zipkin_exporter)
    )
    set_global_textmap(B3SingleFormat())
    DjangoInstrumentor().instrument()
    Psycopg2Instrumentor().instrument()

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?") from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
