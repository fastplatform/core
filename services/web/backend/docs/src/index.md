# Documentation

Welcome to the FaST Administration Portal documentation. We hope you will find the answer to your question(s) in the following pages, but if not, you can contact the [support team](mailto:support@fastplatform.eu) and we will do our best to help you.

## Introduction

The FaST is composed of many bricks, but from a high-level, it can be summarized as:

- a **mobile application** (with a web version), to be used by farmers and advisors
- an **Administration Portal**, that you are currently looking at, reserved to members of public institutions.

## Areas of this documentation
- [Managing users](/services/web/backend/docs/en/users.md)<br>
  Everything you need to know about how to create, remove and deal with FaST users authentication and authorization

- [User farms and fertilization plans](/services/web/backend/docs/en/farm.md)<br>
  Learn how to visualize information created by users on the mobile application, especially farms and fertilization plans

- [Geo-tagged photos](/services/web/backend/docs/en/photos.md)<br>
  View photos uploaded by farmers in the mobile application

- [Messaging](/services/web/backend/docs/en/messaging.md)<br>
  Send and receive messages to a single farmer or to all of them

- [Maps](/services/web/backend/docs/en/maps.md)<br>
  Configure the maps that are displayed in the mobile application and in the Administration Portal

- [Imported GIS data](/services/web/backend/docs/en/external.md)<br>
  View and manage the data that was imported from external GIS source (NVZ, Natura2000, soil, etc)

- [Observation](/services/web/backend/docs/en/observations.md)<br>
  Learn how to create and manage private and public observations, as well as how to constraint user input values using validation rules.

- [The API and add-ons](/services/web/backend/docs/en/add_ons.md)<br>
  Understand how add-ons can be used to connect to the FaST API to build upon FaST data
  
- [Technical details](/services/web/backend/docs/en/technical.md)<br>
  Some technical elements you might be interested in... or not :).


## General considerations on the use of the Administration Portal

The **Administration Portal** is a convenient and user-friendly interface to access the objects stored within FaST. The Administration Portal [landing page](/admin/) presents a summarized and categorized view of these objects. A more comprehensive [all objects view](/admin/all_objects/) is available and lists all the objects in a flat manner. Remember that the objects that you can see in the Portal are determined by your [permissions](/services/web/backend/docs/en/users/authorization).

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/landing_page.png" height="400"/>
<figcaption>Administration Portal landing page</figcaption>
</figure>

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/all_objects.png" height="400"/>
<figcaption>All objects page</figcaption>
</figure>

The Administration Portal interface allows the 4 major database operations (CRUD):

- Viewing objects
- Changing existing objects
- Adding new objects
- Deleting objects

### Viewing objects

On the landing page, the object types are grouped into _categories_. Not all object types are displayed on the landing page, only the most common ones. By clicking on an object type, you will be presented with the list of objects of this type. You can then click on the link corresponding to each object, to see the object description. Depending on the object type, filters might be available either on the right side (regular filters) or above the list (date filters). A search bar is also available on most object types to lookup specific objects.

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/changelist.png" height="400"/>
<figcaption>List of objects and available controls</figcaption>
</figure>

### Changing objects

If you have the permission to change certain object types, then opening an object will allow you to edit its properties and save the modified object to the database. Be careful that edits are irreversible: there is no "undo" button in the Administration Portal.

Once you have edited the object's fields, you can click the <kbd>Save</kbd>, <kbd>Save and continue editing</kbd> or <kbd>Save and add another</kbd> button. If you want to leave the page without saving your edits to the object, you can just leave the page; the interface will _not_ warn you that you are about to lose your edits.

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/save_object.png" height="60"/>
<figcaption>Saving your edits to an object</figcaption>
</figure>

### Adding new objects

You can add a new object of a certain type by clicking on the <kbd>Add _Object Type_</kbd> on the top-right of the list of objects for this type. Once you have configured the new object's fields, you can just save it using the same buttons as above.

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/save_object.png" height="60"/>
<figcaption>Saving a new object</figcaption>
</figure>

You can also add sub-objects within an object. To do so, click on <kbd>Add another _Sub-Object Type_</kbd> within a parent object, configure the the new sub-object's fields, and click <kbd>Save</kbd> to save the parent object (and the new sub-object).

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/add_inline.png" height="120"/>
<figcaption>Adding a sub-object</figcaption>
</figure>

### Deleting objects

To to delete an object, you can either:

- open the object page and click <kbd>Delete</kbd>

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/delete_object.png" height="60"/>
<figcaption>Delete an object from the object page</figcaption>
</figure>

- select the object on the object type list, select <kbd>Delete selected _Object Type_</kbd> from the menu, and click <kbd>Go</kbd>

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/delete_selected.png" height="120"/>
<figcaption>Delete an object using the action menu</figcaption>
</figure>

- if you want to delete a sub-object, you can tick the "Delete?" checkbox next to it and then save the parent object

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/delete_inline.png" height="300"/>
<figcaption>Delete a sub-object</figcaption>
</figure>