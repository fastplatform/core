# Photos

- [Photos](#photos)

The FaST mobile application allows the user to take photos and upload them with their farm data. These photos are also visible from the Administration Portal (as always, relevant permissions apply).

<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/photos/changelist.png" height="300"/>
<figcaption>List of geo-tagged photos</figcaption>
</figure>

Each geo-tagged photo is saved with a set of metadata:

- the actual photo, as a .png file
- the date and time when the photo was taken by the user. Note this may not correspond with the creation date of the file (for example if the user did not have network connectivity when the photo was taken)
- the width and height of the file, in pixels
- Description: a free-form text field for the user to describe the photo
- Thumbnail: a thumbnail of the photo, as a .png file
- Geographical information
    - the heading of the mobile phone when the photo was taken (in degrees, measured from the North)
    - the elevation, in meters
    - the latitude and longitude of the photo, in degrees
    - the geographical location of the picture, on a map (see screenshot below)
- Linkage data
    - the farm campaign this photo is linked to (this is automatically derived as the campaign that was active on the user's mobile when the picture was taken)
    - the farm plot this photo is linked to (if provided by the user)
- Uploading: date and time when the photo was uploaded to the server, and the user who uploaded it.

Notes:

- FaST does not apply any constraint between the actual location of a photo and the plot linked to this photo
- even though the heading is stored with decimal precision, note that the compass measurements are _not_ stabilized on the mobile phone. Also, the heading is not available when the photo was taken on a mobile phone with no compass, or directly from the web app itself (on a computer).
- Administration Portal users should ideally not be granted the _change_ or _add_ permission on the Geo-tagged Photo object type, as the photos are supposed to be uploaded by users of the mobile application. It has to be noted that _change_ and _add_ permissions also allows the user to replace the photo file itself, a right that should be used with caution.


<figure class="figure-docs">
<img src="/services/web/backend/docs/static/docs/img/photos/photos_map.png" height="400"/>
<figcaption>Geo-location of a photo</figcaption>
</figure>








