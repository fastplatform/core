# Technical

The technical documentation of the FaST platform is co-located with the codebase, and can be found at: [https://gitlab.com/fastplatform/docs](https://gitlab.com/fastplatform/docs).

The technical documentation is only available in English language.
