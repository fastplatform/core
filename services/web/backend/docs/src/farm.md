# Farms

- [Farms](#farms)
  - [Demo holdings versus real holdings from IACS data](#demo-holdings-versus-real-holdings-from-iacs-data)
    - [Demo holdings](#demo-holdings)
    - [Real holdings](#real-holdings)
  - [Holding membership](#holding-membership)
  - [Campaign-based holding data](#campaign-based-holding-data)
  - [Holding-based data](#holding-based-data)
  - [Important notes on holding identifiers](#important-notes-on-holding-identifiers)
  - [How do I...?](#how-do-i)
      - [How do I see the list of all the holdings?](#how-do-i-see-the-list-of-all-the-holdings)
      - [Can I create a new holding from the Administration Portal?](#can-i-create-a-new-holding-from-the-administration-portal)
      - [Can I delete a holding from the Administration Portal?](#can-i-delete-a-holding-from-the-administration-portal)
      - [Can I view/change/add holding sub-objects using the Administration Portal?](#can-i-viewchangeadd-holding-sub-objects-using-the-administration-portal)

The agricultural data of the farmer is stored within the <x>Holding</x> object and its dependencies. A [<x>Holding</x>](/admin/farm/holding/) corresponds to the legal entity under which the farmer operates his/her business. It also serves, in FaST, to determine the access rights of users to various holdings.

A [<x>Holding</x>](/admin/farm/holding/) contains certain elements that depend on the users and on the access rights, other elements that depend on the campaign (i.e. the growing season) and finally elements that are more "permanent" to the holding itself.

```plantuml
skinparam defaultFontSize 12

entity "Holding" as holding

package "Farm membership" as membership <<Rectangle>> #cyan {
}
package "Campaign-based elements" as holding_campaign <<Rectangle>> #yellow {
}
package "Farm-based elements" as other <<Rectangle>> #lightgreen {
}

holding -- membership
holding -- holding_campaign
holding -- other
```

The **farm membership** determines which users can see (and edit) the data of the holding in the FaST mobile application.

The **campaign-based elements** are objects that are relevant *within the context of a growing season* (a "campaign"). This mainly comprises the geometrical structure of the holding (like the plots polygons), associated crops, crop yields and fertilization plans, as well as the geo-tagged photos.

The **holding-based elements** are objects that make sense within the holding itself, and that are not necessarily constrained to the duration of a campaign. For example, soil samples are objects that are stored at the holding level, as their validity can span several consecutive campaigns.

## Demo holdings versus real holdings from IACS data

### Demo holdings

Some FaST users are not real farmers or do not wish to import their own private data into FaST: they can use FaST "demo holdings". These holdings are copies of a single prototype holding that has been predefined for each region, with a list of plots and crops that can be used to trial the mobile application. Any FaST user (including real farmers) can create a demo holding on their account. In the <x>Administration Portal</x>, the demo holdings have an identifier like `DEMO-username`.

<img src="/services/web/backend/docs/static/docs/img/farm/demo_farm.png" title="Demo holdings" class="img-docs" height="160" />

Each user can create one demo holding per campaign.

### Real holdings

Real holdings are based on data pulled from the IACS system in the region and mapped to holding objects in FaST. This process is one-way: the holding objects in FaST are never written back to the IACS system and live a life of their own in FaST. The farmer can modify his/her holding in FaST, and this will not have any impact on the data stored in the government system.

## Holding membership

Each holding has a certain number of members who can access its data. Those members can be the farmer himself/herself (i.e. the legal owner of the holding), authorized holding advisors, or any other user that the farmer has previously approved for access in the IACS system.

```plantuml
skinparam defaultFontSize 12

entity "Holding" as holding
entity "User" as user

package "Farm membership" as membership <<Rectangle>> #cyan {
    entity UserRelatedParty {
        role
    }
}

holding ||--o{ UserRelatedParty
user ||--|| UserRelatedParty

caption "Diagram of FaST objects\nfor holding membership"
```

<div style="width: 100%; text-align: center; border: 1px solid black; padding: 10px;">
In FaST, the holding membership is the criteria that determines access to a holding's data. If a user is member of holding, then the user can see and edit <b>all</b> the data of the holding. If a user is not member of a holding, then the user cannot see any of the data of the holding.
</div>

## Campaign-based holding data

 In FaST, the geometrical structure of a plot is assumed to be *constant* within a campaign: if an agricultural plot is added to a campaign, then it is added *for the whole duration of the campaign*. If a crop is specified for a campaign, then it is for the duration of the campaign, etc.

 Every time a user pulls their data for a campaign from the IACS system, the geometrical structure of the holding is updated.

```plantuml
skinparam defaultFontSize 12

entity Holding
entity PlantVariety
entity Campaign {
    start_at
    end_at
}

package "Campaign-based elements" as holding_campaign <<Rectangle>> #yellow {
    entity HoldingCampaign
    
    package "Geometry" as geometric <<Rectangle>> #yellow {
        entity Site
        entity Plot
    }
    entity FertilizationPlan
    entity GeoTaggedPhoto
    HoldingCampaign ||--|| Campaign
    HoldingCampaign ||--o{ Site
    HoldingCampaign ||--o{ GeoTaggedPhoto
    Site ||--o{ Plot
    Plot ||-o{ FertilizationPlan
    Plot ||--o| SoilDerivedObject

    Plot ||--o{ Constraint
    SoilDerivedObject ||--o{ DerivedObservation
    Plot }o--o{ PlantVariety
    GeoTaggedPhoto }o--o| Plot
}

Holding ||--o{ HoldingCampaign
PlantVariety }o--|| PlantSpecies
PlantSpecies }o--|| PlantSpeciesGroup
PlantSpeciesGroup }o--|| PlantSpeciesGroup
DerivedObservation ||--|| ObservableProperty

caption
Diagram of FaST objects linked to a holding campaign
endcaption
```

The structure of the description of a holding is derived from the [INSPIRE Annex III (Agriculture and Aquaculture)](https://inspire.ec.europa.eu/data-model/approved/r4618/html/) semantics.

The <x>Holding</x> contains one or many <x>HoldingCampaigns</x>, which in turn contain one or many <x>Sites</x>, which in turn contain one or many <x>Plots</x>. Note that in FaST, currently, all holding campaigns contain a single site (for all regions); this is due to how the data is returned by the various IACS APIs, and the fact they do not contain the notion of "site". An intermediate <x>Site</x> object is artificially created by FaST to respect the INSPIRE data model.

Each plot (agricultural field) contains several properties but the main ones are:

 - the actual *geometry* of the plot (for the duration of the campaign it is attached to) ; in FaST, this geometry is a MultiPolygon, i.e. a collection of polygons. 
 - an optional *name*, that the farmer can customize
 - an optional *authority ID*, which is the identifier optionally returned by the IACS/GSAA APIs for each plot

 Other objects are attached to a plot:

 - geo-tagged photos (if the farmer explicitly linked the photo to the plot)
 - plant varieties (including the target yield and surface percentage): these are the plant varieties (and therefore the plant species) cultivated on this plot during this campaign. If the IACS API provides this information, the plant variety of the plots will also be imported when the farmer pulls their farm from the IACS system. In any case, the farmer can add/modify plant varieties manually in FaST.
 - fertilization plans
 - soil properties estimates
 - constraints, such as NVZ intersections or proximity to water bodies

```plantuml
skinparam defaultFontSize 12

card "Holding\n//DEMO-matus//" as holding

card "Campaign\n//2022-2023//" as campaign_2022
card "Campaign\n//2023-2024//" as campaign_2023

package "2022-2023" as 2022 #silver {
    card "HoldingCampaign\n//DEMO-matus@2022-2023//" as holding_campaign_2022
    card "Site 123725" as site_2022
    card "Plot A" as plot_2022_1
    card "Plot B" as plot_2022_2
    card "Plot C" as plot_2022_3

    holding_campaign_2022--> site_2022
    site_2022 --> plot_2022_1
    site_2022 --> plot_2022_2
    site_2022 --> plot_2022_3
}

package "2023-2024" as 2023 #silver {
    card "HoldingCampaign\n//DEMO-matus@2023-2024//" as holding_campaign_2023
    card "Site 27628" as site_2023
    card "Plot W" as plot_2023_1
    card "Plot X" as plot_2023_2
    card "Plot Y" as plot_2023_3
    card "Plot Z" as plot_2023_4

    holding_campaign_2023 --> site_2023
    site_2023 --> plot_2023_1
    site_2023 --> plot_2023_2
    site_2023 --> plot_2023_3
    site_2023 --> plot_2023_4
}

holding --> holding_campaign_2022
campaign_2022 --> holding_campaign_2022
holding --> holding_campaign_2023
campaign_2023 --> holding_campaign_2023

caption
For each campaign, the holding geometry is redefined entirely. The holding plots A, B and C of
campaign 2022-2023 are different (and independent) from holding plots W, X, Y and Z
of campaign 2023-2024 (even if their geometries are the same or overlap).
endcaption
```

## Holding-based data

Some other objects are not linked to an actual campaign of a holding, but are linked to the holding itself. These are objects that exist outside of the temporal context of a single growing season.

- subscriptions to [add-ons](/services/web/backend/docs/en/add_ons)
- [tickets](/services/web/backend/docs/en/messaging.md) created within the context of this holding
- private soil samples input by the farmer (not to be confused with [external soil sites](/services/web/backend/docs/en/external.md#soil-sites), that are public regional soil data accessible to all farmers)
- private custom fertilizers defined by the farmer

```plantuml
skinparam defaultFontSize 12

entity Holding
entity AddOn
entity ChemicalElement

package "Farm-based elements" as other <<Rectangle>> #lightgreen {
    entity SoilSite
    entity Observation
    entity Ticket
    entity TicketMessage
    entity CustomFertilizer
    entity CustomFertilizerElement
    entity AddOnSubscription
    Holding ||--o{ SoilSite
    SoilSite ||--o{ Observation
    Holding ||--o{ Ticket
    Ticket ||--o{ TicketMessage
    Holding ||--o{ CustomFertilizer
    CustomFertilizer ||--o{ CustomFertilizerElement
    Holding ||--o{ AddOnSubscription
    AddOnSubscription ||--|| AddOn
}

CustomFertilizerElement ||--|{ ChemicalElement

caption
Diagram of FaST objects linked to a holding
endcaption
```

## Important notes on holding identifiers

The identifier of a holding in FaST is the same identifier as the holding in the regional IACS system. Specifically:

- For Wallonia, the holding identifier is the `numéro de registre national (NRN)` of the physical person ("personne physique") of the farmer ("producteur");
- for Bulgaria, the holding identifier is the id of the farm as returned by the `ДФЗ` IACS system when querying the user's farms. It is a numerical value, such as `50877588`;
- For Grece, the holding identifier is the numerical value such as `047775755`, that corresponds to the tax identification number of the farmer, as known to the OPEKEPE API;
- For Piemonte, the holding identifier is the *azienda identifier* (a numeric identifier such as `101888`);
- For Romania, the holding identifier is the `id` field returned the IACS APIA API, a text identifier such as `RO000107738`;
- For Slovakia, the holding identifier is the `USER_ID` field returned by the IACS APA API. If the user has a `farmer` status, this `USER_ID` will be returned while querying the APA_API_ZIADATEL_URL endpoint. For `advisor` status, it will be returned while querying the APA_API_INTEGRATOR_URL endpoint.
- For Estonia, the holding identifier is the *registrikood* of the holding (if the holding is registered under a business name) or the *isikukood* of the holding owner (if the holding is registered under a personal name)
- For Castilla y Leon, the holding identifier is the *DNI of the holding owner*, a text identifier such as `0000001R`
- For Andalucia, the holding identifier is the *DNI of the holding owner*, a text identifier such as `0000001R`


If the identifier for a holding starts with `DEMO` then the holding is not considered to be a "real" holding and therefore campaigns cannot be synchronized with the IACS system.

All holdings created by the IACS/GSAA connectors or by the "demo farm creator" respect the above identifier rules. If you manually create a holding in the Administration Portal, you need to make sure that you use an identifier that does not collide with the above identifier scheme.

## How do I...?

#### How do I see the list of all the holdings?

If you are a superuser or have the relevant permissions:

- Just open the [holdings list](/admin/farm/holding/)

This list is accessible from the landing page of the Administration Portal.

#### Can I create a new holding from the Administration Portal?

Yes, but this is not the preferred way it should be done. Holdings should be created by the farmer using the mobile application (either by synchronizing the holdings he/she can access, or by creating a new "demo farm").

Be cautious with the identifier you choose for your holding (see above).

- Open the [holdings list](/admin/farm/holding/)
- Click on <kbd>Add Holding</kbd> on the top right of the page
- Fill in the identifier and name of the holding
- Optionally add members to this holding
- Click <kbd>Save</kbd>

#### Can I delete a holding from the Administration Portal?

Yes, you can. However, if there are many objects attached to the holding, the system might ask you to delete those objects first (no automatic "cascade"), to avoid unintentional data deletion.

- Open the [holdings list](/admin/farm/holding/)
- Click on the holding you wish to delete
- Click on the <kbd>Delete</kbd> button and follow the confirmation prompts

#### Can I view/change/add holding sub-objects using the Administration Portal?

Yes, you can view/change/add the holding's sub-objects from the Administration Portal (object-level permissions apply, as usual).

- Open the [holdings list](/admin/farm/holding/)
- Click on the holding you wish to view/change
- Navigate in the holding sub-objects by following the links
