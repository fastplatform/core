{% load static %}<h1 id="toc-header">Dokumentation</h1>
<p>Willkommen bei der Dokumentation des FaST Verwaltungsportals. Wir hoffen, dass Sie auf den folgenden Seiten die Antwort auf Ihre Frage(n) finden. Sollte dies nicht der Fall sein, können Sie sich an das <a href="mailto:support@fastplatform.eu">Support-Team</a> wenden und wir werden unser Bestes tun, um Ihnen zu helfen.</p>
<h2 id="toc-header_1">Einführung</h2>
<p>FaST besteht aus vielen Bausteinen, aber auf einer hohen Ebene kann man es wie folgt zusammenfassen:</p>
<ul>
<li>eine <strong>mobile Anwendung</strong> (mit einer Webversion), die von Landwirten und Beratern genutzt werden kann</li>
<li>ein <strong>Verwaltungsportal</strong>, das Sie gerade sehen und das den Mitgliedern öffentlicher Einrichtungen vorbehalten ist.</li>
</ul>
<h2 id="toc-header_2">Bereiche dieser Dokumentation</h2>
<ul>
<li>
<p><a href="/docs/users/" rel="nofollow">Verwaltung von Benutzern</a><br/>Alles, was Sie über die Erstellung, das Entfernen und den Umgang mit der Authentifizierung und Autorisierung von FaST-Benutzern wissen müssen</p>
</li>
<li>
<p><a href="/docs/farm/" rel="nofollow">Benutzerbetriebe und Düngepläne</a><br/>Erfahren Sie, wie Sie die von Benutzern erstellten Informationen in der mobilen Anwendung visualisieren können, insbesondere Betriebe und Düngepläne</p>
</li>
<li>
<p><a href="/docs/photos/" rel="nofollow">Fotos mit Geo-Tags</a><br/>Von Landwirten hochgeladene Fotos in der mobilen Anwendung anzeigen</p>
</li>
<li>
<p><a href="/docs/messaging/" rel="nofollow">Nachrichten</a><br/>Senden und empfangen Sie Nachrichten an einen einzelnen Landwirt oder an alle Landwirte</p>
</li>
<li>
<p><a href="/docs/maps/" rel="nofollow">Karten</a><br/>Konfigurieren Sie die Karten, die in der mobilen Anwendung und im Verwaltungsportal angezeigt werden</p>
</li>
<li>
<p><a href="/docs/external/" rel="nofollow">Importierte GIS-Daten</a><br/>Anzeigen und Verwalten von Daten, die aus externen GIS-Quellen importiert wurden (NVZ, Natura2000, Böden usw.)</p>
</li>
<li>
<p><a href="/docs/observations/" rel="nofollow">Beobachtung</a><br/>Erfahren Sie, wie Sie private und öffentliche Beobachtungen erstellen und verwalten und wie Sie die Eingabewerte der Benutzer durch Validierungsregeln einschränken können.</p>
</li>
<li>
<p><a href="/docs/add_ons/" rel="nofollow">Die API und Add-ons</a><br/>Verstehen Sie, wie Add-ons verwendet werden können, um sich mit der FaST-API zu verbinden und auf FaST-Daten aufzubauen</p>
</li>
<li>
<p><a href="/docs/technical/" rel="nofollow">Technische Details</a><br/>Einige technische Elemente, die Sie interessieren könnten... oder auch nicht :).</p>
</li>
</ul>
<h2 id="toc-header_3">Allgemeine Überlegungen zur Nutzung des Administrationsportals</h2>
<p>Das <strong>Administrationsportal</strong> ist eine bequeme und benutzerfreundliche Schnittstelle für den Zugriff auf die in FaST gespeicherten Objekte. Die <a href="/admin/" rel="nofollow">Landing Page</a> des Administrationsportals bietet eine zusammengefasste und kategorisierte Ansicht dieser Objekte. Eine umfassendere <a href="/admin/all_objects/" rel="nofollow">Ansicht aller Objekte</a> ist verfügbar und listet alle Objekte in einer flachen Form auf. Denken Sie daran, dass die Objekte, die Sie im Portal sehen können, durch Ihre <a href="/docs/users/authorization" rel="nofollow">Berechtigungen</a> bestimmt werden.</p>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/landing_page.png' %}"/>
<figcaption>Landing Page des Administrationsportals</figcaption>
</figure>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/all_objects.png' %}"/>
<figcaption>Seite "Alle Objekte</figcaption>
</figure>
<p>Die Schnittstelle des Administrationsportals ermöglicht die 4 wichtigsten Datenbankoperationen (CRUD):</p>
<ul>
<li>Anzeigen von Objekten</li>
<li>Ändern von bestehenden Objekten</li>
<li>Hinzufügen neuer Objekte</li>
<li>Löschen von Objekten</li>
</ul>
<h3 id="toc-header_4">Anzeigen von Objekten</h3>
<p>Auf der Landing Page sind die Objekttypen in <em>Kategorien</em> gruppiert. Es werden nicht alle Objekttypen auf der Landing Page angezeigt, sondern nur die gängigsten. Wenn Sie auf einen Objekttyp klicken, wird Ihnen eine Liste der Objekte dieses Typs angezeigt. Sie können dann auf den Link klicken, der zu jedem Objekt gehört, um die Objektbeschreibung zu sehen. Je nach Objekttyp sind Filter entweder auf der rechten Seite (normale Filter) oder oberhalb der Liste (Datumsfilter) verfügbar. Bei den meisten Objekttypen steht auch eine Suchleiste zur Verfügung, um bestimmte Objekte zu suchen.</p>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/changelist.png' %}"/>
<figcaption>Liste der Objekte und verfügbaren Steuerelemente</figcaption>
</figure>
<h3 id="toc-header_5">Ändern von Objekten</h3>
<p>Wenn Sie die Berechtigung haben, bestimmte Objekttypen zu ändern, können Sie beim Öffnen eines Objekts dessen Eigenschaften bearbeiten und das geänderte Objekt in der Datenbank speichern. Achten Sie darauf, dass die Änderungen nicht rückgängig gemacht werden können: Im Verwaltungsportal gibt es keine Schaltfläche "Rückgängig".</p>
<p>Sobald Sie die Felder des Objekts bearbeitet haben, können Sie auf die Schaltfläche <kbd>Speichern</kbd>, <kbd>Speichern und weiter bearbeiten</kbd> oder <kbd>Speichern und ein weiteres Objekt hinzufügen</kbd> klicken. Wenn Sie die Seite verlassen möchten, ohne Ihre Änderungen am Objekt zu speichern, können Sie die Seite einfach verlassen; die Schnittstelle warnt Sie <em>nicht</em>, dass Ihre Änderungen verloren gehen.</p>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/save_object.png' %}"/>
<figcaption>Speichern Ihrer Änderungen an einem Objekt</figcaption>
</figure>
<h3 id="toc-header_6">Hinzufügen neuer Objekte</h3>
<p>Sie können ein neues Objekt eines bestimmten Typs hinzufügen, indem Sie auf die Schaltfläche <kbd> <em>Objekttyp</em></kbd> hinzufügen oben rechts in der Liste der Objekte dieses Typs klicken. Sobald Sie die Felder des neuen Objekts konfiguriert haben, können Sie es mit denselben Schaltflächen wie oben speichern. </p>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/save_object.png' %}"/>
<figcaption>Speichern eines neuen Objekts</figcaption>
</figure>
<p>Sie können auch Unterobjekte innerhalb eines Objekts hinzufügen. Klicken Sie dazu auf Einen <kbd>weiteren <em>Unterobjekttyp</em></kbd> innerhalb eines übergeordneten Objekts <kbd>hinzufügen</kbd>, konfigurieren Sie die Felder des neuen Unterobjekts und klicken Sie auf <kbd>Speichern</kbd>, um das übergeordnete Objekt (und das neue Unterobjekt) zu speichern.</p>
<figure class="figure-docs">
<img height="120" src="{% static 'docs/img/add_inline.png' %}"/>
<figcaption>Hinzufügen eines Unterobjekts</figcaption>
</figure>
<h3 id="toc-header_7">Löschen von Objekten</h3>
<p>Um ein Objekt zu löschen, können Sie entweder:</p>
<ul>
<li>die Objektseite öffnen und auf <kbd>Löschen</kbd>klicken</li>
</ul>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/delete_object.png' %}"/>
<figcaption>ein Objekt von der Objektseite löschen</figcaption>
</figure>
<ul>
<li>Wählen Sie das Objekt in der Objekttypliste aus, wählen Sie im Menü <kbd>Ausgewählten <em>Objekttyp</em></kbd> löschen und klicken Sie auf <kbd>Los</kbd></li>
</ul>
<figure class="figure-docs">
<img height="120" src="{% static 'docs/img/delete_selected.png' %}"/>
<figcaption>ein Objekt über das Aktionsmenü löschen</figcaption>
</figure>
<ul>
<li>Wenn Sie ein Unterobjekt löschen möchten, können Sie das Kontrollkästchen "Löschen?" daneben aktivieren und dann das übergeordnete Objekt speichern.</li>
</ul>
<figure class="figure-docs">
<img height="300" src="{% static 'docs/img/delete_inline.png' %}"/>
<figcaption>Löschen eines Unterobjekts</figcaption>
</figure>