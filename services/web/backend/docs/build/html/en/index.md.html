{% load static %}<h1 id="toc-header">Documentation</h1>
<p>Welcome to the FaST Administration Portal documentation. We hope you will find the answer to your question(s) in the following pages, but if not, you can contact the <a href="mailto:support@fastplatform.eu">support team</a> and we will do our best to help you.</p>
<h2 id="toc-header_1">Introduction</h2>
<p>The FaST is composed of many bricks, but from a high-level, it can be summarized as:</p>
<ul>
<li>a <strong>mobile application</strong> (with a web version), to be used by farmers and advisors</li>
<li>an <strong>Administration Portal</strong>, that you are currently looking at, reserved to members of public institutions.</li>
</ul>
<h2 id="toc-header_2">Areas of this documentation</h2>
<ul>
<li>
<p><a href="/docs/users/" rel="nofollow">Managing users</a><br/>
  Everything you need to know about how to create, remove and deal with FaST users authentication and authorization</p>
</li>
<li>
<p><a href="/docs/farm/" rel="nofollow">User farms and fertilization plans</a><br/>
  Learn how to visualize information created by users on the mobile application, especially farms and fertilization plans</p>
</li>
<li>
<p><a href="/docs/photos/" rel="nofollow">Geo-tagged photos</a><br/>
  View photos uploaded by farmers in the mobile application</p>
</li>
<li>
<p><a href="/docs/messaging/" rel="nofollow">Messaging</a><br/>
  Send and receive messages to a single farmer or to all of them</p>
</li>
<li>
<p><a href="/docs/maps/" rel="nofollow">Maps</a><br/>
  Configure the maps that are displayed in the mobile application and in the Administration Portal</p>
</li>
<li>
<p><a href="/docs/external/" rel="nofollow">Imported GIS data</a><br/>
  View and manage the data that was imported from external GIS source (NVZ, Natura2000, soil, etc)</p>
</li>
<li>
<p><a href="/docs/observations/" rel="nofollow">Observation</a><br/>
  Learn how to create and manage private and public observations, as well as how to constraint user input values using validation rules.</p>
</li>
<li>
<p><a href="/docs/add_ons/" rel="nofollow">The API and add-ons</a><br/>
  Understand how add-ons can be used to connect to the FaST API to build upon FaST data</p>
</li>
<li>
<p><a href="/docs/technical/" rel="nofollow">Technical details</a><br/>
  Some technical elements you might be interested in... or not :).</p>
</li>
</ul>
<h2 id="toc-header_3">General considerations on the use of the Administration Portal</h2>
<p>The <strong>Administration Portal</strong> is a convenient and user-friendly interface to access the objects stored within FaST. The Administration Portal <a href="/admin/" rel="nofollow">landing page</a> presents a summarized and categorized view of these objects. A more comprehensive <a href="/admin/all_objects/" rel="nofollow">all objects view</a> is available and lists all the objects in a flat manner. Remember that the objects that you can see in the Portal are determined by your <a href="/docs/users/authorization" rel="nofollow">permissions</a>.</p>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/landing_page.png' %}"/>
<figcaption>Administration Portal landing page</figcaption>
</figure>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/all_objects.png' %}"/>
<figcaption>All objects page</figcaption>
</figure>
<p>The Administration Portal interface allows the 4 major database operations (CRUD):</p>
<ul>
<li>Viewing objects</li>
<li>Changing existing objects</li>
<li>Adding new objects</li>
<li>Deleting objects</li>
</ul>
<h3 id="toc-header_4">Viewing objects</h3>
<p>On the landing page, the object types are grouped into <em>categories</em>. Not all object types are displayed on the landing page, only the most common ones. By clicking on an object type, you will be presented with the list of objects of this type. You can then click on the link corresponding to each object, to see the object description. Depending on the object type, filters might be available either on the right side (regular filters) or above the list (date filters). A search bar is also available on most object types to lookup specific objects.</p>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/changelist.png' %}"/>
<figcaption>List of objects and available controls</figcaption>
</figure>
<h3 id="toc-header_5">Changing objects</h3>
<p>If you have the permission to change certain object types, then opening an object will allow you to edit its properties and save the modified object to the database. Be careful that edits are irreversible: there is no "undo" button in the Administration Portal.</p>
<p>Once you have edited the object's fields, you can click the <kbd>Save</kbd>, <kbd>Save and continue editing</kbd> or <kbd>Save and add another</kbd> button. If you want to leave the page without saving your edits to the object, you can just leave the page; the interface will <em>not</em> warn you that you are about to lose your edits.</p>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/save_object.png' %}"/>
<figcaption>Saving your edits to an object</figcaption>
</figure>
<h3 id="toc-header_6">Adding new objects</h3>
<p>You can add a new object of a certain type by clicking on the <kbd>Add <em>Object Type</em></kbd> on the top-right of the list of objects for this type. Once you have configured the new object's fields, you can just save it using the same buttons as above.</p>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/save_object.png' %}"/>
<figcaption>Saving a new object</figcaption>
</figure>
<p>You can also add sub-objects within an object. To do so, click on <kbd>Add another <em>Sub-Object Type</em></kbd> within a parent object, configure the the new sub-object's fields, and click <kbd>Save</kbd> to save the parent object (and the new sub-object).</p>
<figure class="figure-docs">
<img height="120" src="{% static 'docs/img/add_inline.png' %}"/>
<figcaption>Adding a sub-object</figcaption>
</figure>
<h3 id="toc-header_7">Deleting objects</h3>
<p>To to delete an object, you can either:</p>
<ul>
<li>open the object page and click <kbd>Delete</kbd></li>
</ul>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/delete_object.png' %}"/>
<figcaption>Delete an object from the object page</figcaption>
</figure>
<ul>
<li>select the object on the object type list, select <kbd>Delete selected <em>Object Type</em></kbd> from the menu, and click <kbd>Go</kbd></li>
</ul>
<figure class="figure-docs">
<img height="120" src="{% static 'docs/img/delete_selected.png' %}"/>
<figcaption>Delete an object using the action menu</figcaption>
</figure>
<ul>
<li>if you want to delete a sub-object, you can tick the "Delete?" checkbox next to it and then save the parent object</li>
</ul>
<figure class="figure-docs">
<img height="300" src="{% static 'docs/img/delete_inline.png' %}"/>
<figcaption>Delete a sub-object</figcaption>
</figure>