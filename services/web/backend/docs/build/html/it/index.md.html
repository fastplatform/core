{% load static %}<h1 id="toc-header">Documentazione</h1>
<p>Benvenuti nella documentazione del portale di amministrazione FaST. Ci auguriamo che nelle pagine seguenti possiate trovare la risposta alle vostre domande; in caso contrario, potete contattare il <a href="mailto:support@fastplatform.eu">team di supporto</a> e faremo del nostro meglio per aiutarvi.</p>
<h2 id="toc-header_1">Introduzione</h2>
<p>Il FaST è composto da molti elementi, ma ad alto livello può essere riassunto come:</p>
<ul>
<li>un'<strong>applicazione mobile</strong> (con una versione web), che può essere utilizzata da agricoltori e consulenti</li>
<li>un <strong>portale di amministrazione</strong>, che state vedendo in questo momento, riservato ai membri delle istituzioni pubbliche.</li>
</ul>
<h2 id="toc-header_2">Aree di questa documentazione</h2>
<ul>
<li>
<p><a href="/docs/users/" rel="nofollow">Gestione degli utenti</a><br/>Tutto quello che c'è da sapere su come creare, rimuovere e gestire l'autenticazione e l'autorizzazione degli utenti FaST</p>
</li>
<li>
<p><a href="/docs/farm/" rel="nofollow">Fattorie e piani di fertilizzazione degli utenti</a><br/>Imparate a visualizzare le informazioni create dagli utenti sull'applicazione mobile, in particolare le fattorie e i piani di fertilizzazione.</p>
</li>
<li>
<p><a href="/docs/photos/" rel="nofollow">Foto geo-taggate</a><br/>Visualizzare le foto caricate dagli agricoltori nell'applicazione mobile</p>
</li>
<li>
<p><a href="/docs/messaging/" rel="nofollow">Messaggistica</a><br/>Inviare e ricevere messaggi a un singolo agricoltore o a tutti gli agricoltori</p>
</li>
<li>
<p><a href="/docs/maps/" rel="nofollow">Mappe</a><br/>Configurare le mappe visualizzate nell'applicazione mobile e nel portale di amministrazione.</p>
</li>
<li>
<p><a href="/docs/external/" rel="nofollow">Dati GIS importati</a><br/>Visualizzare e gestire i dati importati da fonti GIS esterne (ZVN, Natura2000, suolo, ecc.).</p>
</li>
<li>
<p><a href="/docs/observations/" rel="nofollow">Osservazioni</a><br/>Imparare a creare e gestire osservazioni private e pubbliche, nonché a limitare i valori immessi dall'utente mediante regole di convalida.</p>
</li>
<li>
<p><a href="/docs/add_ons/" rel="nofollow">L'API e i componenti aggiuntivi</a><br/>Capire come i componenti aggiuntivi possono essere utilizzati per connettersi all'API FaST e costruire sui dati FaST.</p>
</li>
<li>
<p><a href="/docs/technical/" rel="nofollow">Dettagli tecnici</a><br/>Alcuni elementi tecnici che potrebbero interessarvi... oppure no :).</p>
</li>
</ul>
<h2 id="toc-header_3">Considerazioni generali sull'uso del Portale di amministrazione</h2>
<p>Il <strong>portale di amministrazione</strong> è un'interfaccia comoda e facile da usare per accedere agli oggetti memorizzati all'interno del FaST. La <a href="/admin/" rel="nofollow">pagina di destinazione</a> del Portale di amministrazione presenta una vista sintetica e categorizzata di questi oggetti. È disponibile una <a href="/admin/all_objects/" rel="nofollow">visualizzazione</a> più completa di <a href="/admin/all_objects/" rel="nofollow">tutti gli oggetti</a>, che elenca tutti gli oggetti in modo piatto. Ricordate che gli oggetti che potete vedere nel Portale sono determinati dalle vostre <a href="/docs/users/authorization" rel="nofollow">autorizzazioni</a>.</p>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/landing_page.png' %}"/>
<figcaption>Pagina di destinazione del portale di amministrazione</figcaption>
</figure>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/all_objects.png' %}"/>
<figcaption>Pagina di tutti gli oggetti</figcaption>
</figure>
<p>L'interfaccia del Portale di amministrazione consente le 4 principali operazioni di database (CRUD):</p>
<ul>
<li>Visualizzazione degli oggetti</li>
<li>Modificare gli oggetti esistenti</li>
<li>Aggiunta di nuovi oggetti</li>
<li>Eliminazione di oggetti</li>
</ul>
<h3 id="toc-header_4">Visualizzazione degli oggetti</h3>
<p>Nella pagina di destinazione, i tipi di oggetti sono raggruppati in <em>categorie</em>. Non tutti i tipi di oggetti sono visualizzati nella pagina di destinazione, ma solo quelli più comuni. Facendo clic su un tipo di oggetto, viene presentato l'elenco degli oggetti di questo tipo. È possibile fare clic sul link corrispondente a ciascun oggetto per visualizzarne la descrizione. A seconda del tipo di oggetto, i filtri possono essere disponibili sul lato destro (filtri normali) o sopra l'elenco (filtri di data). Nella maggior parte dei tipi di oggetti è disponibile anche una barra di ricerca per cercare oggetti specifici.</p>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/changelist.png' %}"/>
<figcaption>Elenco degli oggetti e dei controlli disponibili</figcaption>
</figure>
<h3 id="toc-header_5">Modifica degli oggetti</h3>
<p>Se si dispone dell'autorizzazione a modificare determinati tipi di oggetti, l'apertura di un oggetto consente di modificarne le proprietà e di salvare l'oggetto modificato nel database. Attenzione, le modifiche sono irreversibili: non esiste un pulsante "annulla" nel Portale di amministrazione.</p>
<p>Una volta modificati i campi dell'oggetto, è possibile fare clic sul pulsante <kbd>Salva</kbd>, <kbd>Salva e continua a modificare</kbd> o <kbd>Salva e aggiungi</kbd>. Se si desidera abbandonare la pagina senza salvare le modifiche apportate all'oggetto, è sufficiente uscire dalla pagina; l'interfaccia <em>non</em> avviserà che si stanno per perdere le modifiche.</p>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/save_object.png' %}"/>
<figcaption>Salvare le modifiche apportate a un oggetto</figcaption>
</figure>
<h3 id="toc-header_6">Aggiunta di nuovi oggetti</h3>
<p>È possibile aggiungere un nuovo oggetto di un certo tipo facendo clic su <kbd>Aggiungi <em>tipo di oggetto</em></kbd> in alto a destra nell'elenco degli oggetti di questo tipo. Una volta configurati i campi del nuovo oggetto, è sufficiente salvarlo utilizzando gli stessi pulsanti di cui sopra.</p>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/save_object.png' %}"/>
<figcaption>Salvataggio di un nuovo oggetto</figcaption>
</figure>
<p>È anche possibile aggiungere sotto-oggetti all'interno di un oggetto. Per farlo, fare clic su <kbd>Aggiungi un altro <em>tipo di sottooggetto</em></kbd> all'interno di un oggetto padre, configurare i campi del nuovo sottooggetto e fare clic su <kbd>Salva</kbd> per salvare l'oggetto padre (e il nuovo sottooggetto).</p>
<figure class="figure-docs">
<img height="120" src="{% static 'docs/img/add_inline.png' %}"/>
<figcaption>Aggiunta di un sottooggetto</figcaption>
</figure>
<h3 id="toc-header_7">Eliminazione di oggetti</h3>
<p>Per eliminare un oggetto, è possibile</p>
<ul>
<li>aprire la pagina dell'oggetto e fare clic su <kbd>Elimina</kbd></li>
</ul>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/delete_object.png' %}"/>
<figcaption>Eliminare un oggetto dalla pagina degli oggetti</figcaption>
</figure>
<ul>
<li>selezionare l'oggetto nell'elenco dei tipi di oggetto, selezionare <kbd>Elimina il <em>tipo di oggetto</em></kbd> selezionato dal menu e fare clic su <kbd>Vai</kbd>.</li>
</ul>
<figure class="figure-docs">
<img height="120" src="{% static 'docs/img/delete_selected.png' %}"/>
<figcaption>Cancellare un oggetto utilizzando il menu delle azioni</figcaption>
</figure>
<ul>
<li>Se si desidera eliminare un sottooggetto, è possibile selezionare la casella di controllo "Elimina?" accanto ad esso e salvare l'oggetto padre.</li>
</ul>
<figure class="figure-docs">
<img height="300" src="{% static 'docs/img/delete_inline.png' %}"/>
<figcaption>Eliminare un sottooggetto</figcaption>
</figure>