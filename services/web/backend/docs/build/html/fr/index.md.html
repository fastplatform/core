{% load static %}<h1 id="toc-header">Documentation</h1>
<p>Bienvenue dans la documentation du portail d'administration FaST. Nous espérons que vous trouverez la réponse à votre/vos question(s) dans les pages suivantes, mais si ce n'est pas le cas, vous pouvez contacter l'<a href="mailto:support@fastplatform.eu">équipe de support</a> et nous ferons de notre mieux pour vous aider.</p>
<h2 id="toc-header_1">Introduction</h2>
<p>Le FaST est composé de nombreuses briques, mais d'un point de vue général, il peut être résumé comme suit :</p>
<ul>
<li>une <strong>application mobile</strong> (avec une version web), à utiliser par les agriculteurs et les conseillers</li>
<li>un <strong>portail d'administration</strong>, que vous êtes en train de consulter, réservé aux membres des institutions publiques.</li>
</ul>
<h2 id="toc-header_2">Domaines de cette documentation</h2>
<ul>
<li>
<p><a href="/docs/users/" rel="nofollow">Gestion des utilisateurs</a><br/>Tout ce que vous devez savoir sur la création, la suppression et la gestion de l'authentification et de l'autorisation des utilisateurs FaST.</p>
</li>
<li>
<p><a href="/docs/farm/" rel="nofollow">Exploitations et plans de fertilisation des utilisateurs</a><br/>Apprenez à visualiser les informations créées par les utilisateurs sur l'application mobile, notamment les exploitations et les plans de fertilisation.</p>
</li>
<li>
<p><a href="/docs/photos/" rel="nofollow">Photos géolocalisées</a><br/>Visualiser les photos téléchargées par les agriculteurs dans l'application mobile</p>
</li>
<li>
<p><a href="/docs/messaging/" rel="nofollow">Messagerie</a><br/>Envoyez et recevez des messages à un seul agriculteur ou à tous les agriculteurs.</p>
</li>
<li>
<p><a href="/docs/maps/" rel="nofollow">Cartes</a><br/>Configurer les cartes qui s'affichent dans l'application mobile et dans le portail d'administration.</p>
</li>
<li>
<p><a href="/docs/external/" rel="nofollow">Données SIG importées</a><br/>Visualisez et gérez les données qui ont été importées d'une source SIG externe (NVZ, Natura2000, sol, etc.).</p>
</li>
<li>
<p><a href="/docs/observations/" rel="nofollow">Observation</a><br/>Apprenez à créer et à gérer des observations privées et publiques, ainsi qu'à contraindre les valeurs saisies par l'utilisateur à l'aide de règles de validation.</p>
</li>
<li>
<p><a href="/docs/add_ons/" rel="nofollow">L'API et les modules complémentaires</a><br/>Comprendre comment les modules complémentaires peuvent être utilisés pour se connecter à l'API de FaST afin d'exploiter les données FaST.</p>
</li>
<li>
<p><a href="/docs/technical/" rel="nofollow">Détails techniques</a><br/>Quelques éléments techniques qui pourraient vous intéresser... ou pas :).</p>
</li>
</ul>
<h2 id="toc-header_3">Considérations générales sur l'utilisation du portail d'administration</h2>
<p>Le <strong>portail d'administration</strong> est une interface pratique et conviviale pour accéder aux objets stockés dans FaST. La <a href="/admin/" rel="nofollow">page d'accueil</a> du portail d'administration présente une vue résumée et catégorisée de ces objets. Une <a href="/admin/all_objects/" rel="nofollow">vue</a> plus complète <a href="/admin/all_objects/" rel="nofollow">de tous les objets</a> est disponible et répertorie tous les objets de manière plate. N'oubliez pas que les objets que vous pouvez voir dans le portail sont déterminés par vos <a href="/docs/users/authorization" rel="nofollow">autorisations</a>.</p>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/landing_page.png' %}"/>
<figcaption>Page d'accueil du portail d'administration</figcaption>
</figure>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/all_objects.png' %}"/>
<figcaption>Page de tous les objets</figcaption>
</figure>
<p>L'interface du portail d'administration permet les 4 principales opérations de base de données (CRUD) :</p>
<ul>
<li>Visualisation des objets</li>
<li>Modification des objets existants</li>
<li>Ajouter de nouveaux objets</li>
<li>Suppression d'objets</li>
</ul>
<h3 id="toc-header_4">Visualisation des objets</h3>
<p>Sur la page de renvoi, les types d'objets sont regroupés en <em>catégories</em>. Tous les types d'objets ne sont pas affichés sur la page de renvoi, mais seulement les plus courants. En cliquant sur un type d'objet, vous obtiendrez la liste des objets de ce type. Vous pouvez ensuite cliquer sur le lien correspondant à chaque objet, pour voir la description de l'objet. En fonction du type d'objet, des filtres sont disponibles soit sur le côté droit (filtres ordinaires), soit au-dessus de la liste (filtres de date). Une barre de recherche est également disponible sur la plupart des types d'objets pour rechercher des objets spécifiques.</p>
<figure class="figure-docs">
<img height="400" src="{% static 'docs/img/changelist.png' %}"/>
<figcaption>Liste des objets et des contrôles disponibles</figcaption>
</figure>
<h3 id="toc-header_5">Modification des objets</h3>
<p>Si vous avez l'autorisation de modifier certains types d'objets, l'ouverture d'un objet vous permettra de modifier ses propriétés et d'enregistrer l'objet modifié dans la base de données. Attention, les modifications sont irréversibles : il n'y a pas de bouton "Annuler" dans le portail d'administration.</p>
<p>Une fois que vous avez modifié les champs de l'objet, vous pouvez cliquer sur le bouton <kbd>Enregistrer</kbd>, <kbd>Enregistrer et poursuivre la modification</kbd> ou <kbd>Enregistrer et ajouter un autre objet</kbd>. Si vous voulez quitter la page sans sauvegarder les modifications apportées à l'objet, vous pouvez simplement quitter la page ; l'interface <em>ne</em> vous avertira <em>pas</em> que vous êtes sur le point de perdre vos modifications.</p>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/save_object.png' %}"/>
<figcaption>Enregistrement des modifications apportées à un objet</figcaption>
</figure>
<h3 id="toc-header_6">Ajout de nouveaux objets</h3>
<p>Vous pouvez ajouter un nouvel objet d'un certain type en cliquant sur le bouton <kbd>Ajouter un <em>type d'objet en</em></kbd> haut à droite de la liste des objets de ce type. Une fois que vous avez configuré les champs du nouvel objet, vous pouvez l'enregistrer en utilisant les mêmes boutons que ci-dessus.</p>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/save_object.png' %}"/>
<figcaption>Enregistrement d'un nouvel objet</figcaption>
</figure>
<p>Vous pouvez également ajouter des sous-objets à l'intérieur d'un objet. Pour ce faire, cliquez sur <kbd>Add another <em>Sub-Object Type</em></kbd> au sein d'un objet parent, configurez les champs du nouveau sous-objet, puis cliquez sur <kbd>Save</kbd> pour enregistrer l'objet parent (et le nouveau sous-objet).</p>
<figure class="figure-docs">
<img height="120" src="{% static 'docs/img/add_inline.png' %}"/>
<figcaption>Ajout d'un sous-objet</figcaption>
</figure>
<h3 id="toc-header_7">Suppression d'objets</h3>
<p>Pour supprimer un objet, vous pouvez soit :</p>
<ul>
<li>ouvrir la page de l'objet et cliquer sur <kbd>Supprimer</kbd></li>
</ul>
<figure class="figure-docs">
<img height="60" src="{% static 'docs/img/delete_object.png' %}"/>
<figcaption>supprimer un objet de la page des objets</figcaption>
</figure>
<ul>
<li>sélectionner l'objet dans la liste des types d'objets, sélectionner <kbd>Supprimer le <em>type d'objet</em></kbd> sélectionné dans le menu, puis cliquer sur <kbd>Aller.</kbd></li>
</ul>
<figure class="figure-docs">
<img height="120" src="{% static 'docs/img/delete_selected.png' %}"/>
<figcaption>Supprimer un objet à l'aide du menu d'action</figcaption>
</figure>
<ul>
<li>si vous souhaitez supprimer un sous-objet, vous pouvez cocher la case "Delete ?" à côté de celui-ci, puis enregistrer l'objet parent.</li>
</ul>
<figure class="figure-docs">
<img height="300" src="{% static 'docs/img/delete_inline.png' %}"/>
<figcaption>Supprimer un sous-objet</figcaption>
</figure>