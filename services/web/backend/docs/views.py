import logging
from pathlib import Path
import json
import os

from django.conf import settings
from django.http import Http404
from django.http.response import HttpResponseRedirect
from django.template import Template, Context
from django.utils.safestring import mark_safe
from django.views.generic import TemplateView
from django.views.decorators.cache import cache_control
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.http import require_GET
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse

from whoosh.qparser import QueryParser
from whoosh import index


# Logging
logger = logging.getLogger(__name__)

DOCS_BUILD_DIR: Path = Path(settings.BASE_DIR, "docs", "build")
TOCS: dict = None
INDEX: index = None


def load_tocs():
    global TOCS
    if TOCS is None and Path(DOCS_BUILD_DIR, "tocs.json").exists():
        TOCS = json.loads(Path(DOCS_BUILD_DIR, "tocs.json").read_text())


def load_index():
    global INDEX
    if INDEX is None and Path(DOCS_BUILD_DIR, "index").exists():
        INDEX = index.open_dir(Path(DOCS_BUILD_DIR, "index"))


class TableOfContentsMixin:
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        toc = TOCS.get(self.request.LANGUAGE_CODE, settings.DOCS_DEFAULT_LANGUAGE)
        context["toc"] = toc
        return context


class CurrentURLNoLanguageMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_url = "/" + "/".join(self.request.path.split("/")[2:])
        context["current_url"] = current_url
        return context


class BreadcrumbsMixin:
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        toc = TOCS.get(self.request.LANGUAGE_CODE, settings.DOCS_DEFAULT_LANGUAGE)
        toc_dict = {toc_item["url"]: toc_item for toc_item in toc}

        breadcrumbs = []
        tokens = self.request.path.split("/")[2:-1]
        for i, token in enumerate(tokens):
            partial_url = "/" + "/".join(tokens[: i + 1]) + "/"
            toc_item = toc_dict[partial_url]
            breadcrumbs.append(
                {
                    "url": partial_url if i < len(tokens) - 1 else None,
                    "title": toc_item["title"],
                }
            )

        context["breadcrumbs"] = breadcrumbs
        return context


@method_decorator(
    [
        login_required,
        staff_member_required,
        require_GET,
        cache_control(max_age=settings.DOCS_CACHE_MAX_AGE),
    ],
    name="dispatch",
)
class DocsView(
    BreadcrumbsMixin, CurrentURLNoLanguageMixin, TableOfContentsMixin, TemplateView
):
    """View for all the markdown documents

    Converts the markdown to HTML on the fly, supports static images and PlantUML
    charts
    """

    file_name = None
    template_name = "docs/docs.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        if not self.file_name:
            raise Exception("file_name attribute is required")

        # Try to read the file from the current language directory
        # and if not possible, from the English 'en" directory
        # and if not possible, fail with 404
        try:
            path = os.path.join(
                DOCS_BUILD_DIR, "html", self.request.LANGUAGE_CODE, self.file_name
            )
            html = Path(path + ".html").read_text()
            toc = Path(path + ".toc.html").read_text()
            title = Path(path + ".title.html").read_text()
        except FileNotFoundError:
            try:
                path = os.path.join(
                    DOCS_BUILD_DIR,
                    "html",
                    settings.DOCS_DEFAULT_LANGUAGE,
                    self.file_name,
                )
                html = Path(path + ".html").read_text()
                toc = Path(path + ".toc.html").read_text()
                title = Path(path + ".title.html").read_text()
            except FileNotFoundError:
                raise Http404()

        # Render the {% static "xxxx" %} blocks
        rendered_html = mark_safe(Template(html).render(Context({})))

        context.update(
            {
                "user": self.request.user,
                "has_permission": True,
                "markdown_content": mark_safe(rendered_html),
                "markdown_toc": mark_safe(toc),
                "page_title": mark_safe(title),
            }
        )

        return context


@method_decorator(
    [login_required, staff_member_required, require_GET, csrf_exempt], name="dispatch"
)
class DocsSearchView(CurrentURLNoLanguageMixin, TableOfContentsMixin, TemplateView):
    """View for the search results

    The search is based on the Whoosh index generated at startup time by the
    docs.index.fill_index_and_toc() function
    """

    file_name = None
    template_name = "docs/search.html"

    q = None

    def dispatch(self, *args, **kwargs):
        q = self.request.GET.get("q", None)
        if q is None:
            return HttpResponseRedirect(reverse("docs:index"))
        self.q = q
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context.update(
            {
                "user": self.request.user,
                "has_permission": True,
            }
        )

        context["q"] = self.q

        languages = dict(settings.LANGUAGES)

        with INDEX.searcher() as searcher:
            query = QueryParser("content", INDEX.schema).parse(self.q)
            results = searcher.search(query, limit=20)

            context["results"] = []
            context["language_name"] = languages.get(self.request.LANGUAGE_CODE, None)
            context["results_other_languages"] = []

            for hit in results:
                result = {**hit.fields(), "highlights": hit.highlights("content")}
                if result["language_code"] == self.request.LANGUAGE_CODE:
                    result["language_name"] = None
                    context["results"].append(result)
                else:
                    result["language_name"] = languages[result["language_code"]]
                    context["results_other_languages"].append(result)

        return context
