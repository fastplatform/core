from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DocsConfig(AppConfig):
    name = "docs"
    verbose_name = _("Documentation")

    def ready(self):
        from docs.views import load_tocs, load_index
        load_tocs()
        load_index()
