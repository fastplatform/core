
from model_bakery.recipe import Recipe, foreign_key

from soil.models import (
    SoilSite,
    Observation,
    PhenomenonType,
    SoilInvestigationPurpose,
    ObservableProperty,
)

from farm.tests.recipes import holding_recipe

soil_site_recipe = Recipe(SoilSite, holding=foreign_key(holding_recipe))

observation_recipe = Recipe(Observation, result={"value": 1.0})

phenomenon_type_recipe = Recipe(PhenomenonType)

soil_investigation_purpose_recipe = Recipe(SoilInvestigationPurpose)

observable_property_recipe = Recipe(ObservableProperty)