from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class SoilConfig(AppConfig):
    name = 'soil'
    verbose_name = _('Soil')
