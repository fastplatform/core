from functools import partial

from django.utils.translation import gettext_lazy as _
from django.contrib.admin.utils import unquote, flatten_fieldsets
from django.http import Http404
from django import forms
from django.core.exceptions import FieldError
from django.forms.models import modelform_defines_fields, modelform_factory

from utils.widgets.map_widget import MapWidget


class MapWidgetMixin:

    geometry_fields = []

    def get_holding_id(self, obj):
        # To be overridden
        return None

    def get_site_id(self, obj):
        # To be overridden
        return None

    def get_plot_id(self, obj):
        # To be overridden
        return None

    def get_holding_campaign_id(self, obj):
        # To be overridden
        return None

    map = False

    def change_view(self, request, *args, **kwargs):

        if getattr(self, "static_map", False):
            # Get the current object either from kwargs (regular ModelAdmin)
            # or from args (SubAdmin)
            try:
                if "object_id" in kwargs:
                    obj = self.model.objects.get(pk=unquote(kwargs["object_id"]))
                else:
                    obj = self.model.objects.get(pk=args[-1])
            except self.model.DoesNotExist:
                raise Http404("Not found")

            extra_context = kwargs.pop("extra_context", {})

            # pylint: disable=assignment-from-none
            extent = self.get_map_extent(obj)

            if extent is not None:
                widget = MapWidget(
                    geom_type="GEOMETRY",
                    static=True,
                    session_key=request.session.session_key,
                    holding_id=self.get_holding_id(obj),
                    holding_campaign_id=self.get_holding_campaign_id(obj),
                    site_id=self.get_site_id(obj),
                    plot_id=self.get_plot_id(obj),
                    extent=extent,
                )

                x = (extent[0] + extent[2]) / 2
                y = (extent[1] + extent[3]) / 2
                extra_context["static_map"] = {
                    "html": widget.render("map", f"POINT ({x} {y})")
                }

                return super().change_view(
                    request, *args, extra_context=extra_context, **kwargs
                )

        return super().change_view(
            request, *args, **kwargs
        )

    def get_form(self, request, obj=None, change=False, **kwargs):

        if "fields" in kwargs:
            fields = kwargs.pop("fields")
        else:
            fields = flatten_fieldsets(self.get_fieldsets(request, obj))
        excluded = self.get_exclude(request, obj)
        exclude = [] if excluded is None else list(excluded)
        readonly_fields = self.get_readonly_fields(request, obj)
        exclude.extend(readonly_fields)
        # Exclude all fields if it's a change form and the user doesn't have
        # the change permission.
        if (
            change
            and hasattr(request, "user")
            and not self.has_change_permission(request, obj)
        ):
            # FaST: here we override the behavior of the exclusion when on a 'change' page
            # to allow the geometry fields to go through (with a readonly widget) and be
            # displayed with the Leaflet widget
            if self.geometry_fields:
                exclude.extend([f for f in fields if f not in self.geometry_fields])
            else:
                exclude.extend(fields)
        if excluded is None and hasattr(self.form, "_meta") and self.form._meta.exclude:
            # Take the custom ModelForm's Meta.exclude into account only if the
            # ModelAdmin doesn't define its own.
            exclude.extend(self.form._meta.exclude)
        # if exclude is an empty list we pass None to be consistent with the
        # default on modelform_factory
        exclude = exclude or None

        # Remove declared form fields which are in readonly_fields.
        new_attrs = dict.fromkeys(
            f for f in readonly_fields if f in self.form.declared_fields
        )
        form = type(self.form.__name__, (self.form,), new_attrs)

        defaults = {
            "form": form,
            "fields": fields,
            "exclude": exclude,
            "formfield_callback": partial(self.formfield_for_dbfield, request=request),
            **kwargs,
        }

        if defaults["fields"] is None and not modelform_defines_fields(
            defaults["form"]
        ):
            defaults["fields"] = forms.ALL_FIELDS

        try:
            base_form = modelform_factory(self.model, **defaults)
        except FieldError as e:
            raise FieldError(
                "%s. Check fields/fieldsets/exclude attributes of class %s."
                % (e, self.__class__.__name__)
            )

        # FaST: now add custom widgets for geometry fields
        if self.geometry_fields:

            modifiable = (change and self.has_change_permission(request, obj)) or (
                not change and self.has_add_permission(request)
            )
            new_widgets = getattr(base_form.Meta, "widgets", {}).copy()

            for geometry_field in self.geometry_fields:
                field = self.model._meta.get_field(geometry_field)

                new_widgets[geometry_field] = MapWidget(
                    geom_type=field.geom_type,
                    modifiable=modifiable,
                    session_key=request.session.session_key,
                    holding_id=self.get_holding_id(obj),
                    holding_campaign_id=self.get_holding_campaign_id(obj),
                    site_id=self.get_site_id(obj),
                    plot_id=self.get_plot_id(obj),
                )

            def make_form_class(form):
                class FormWithMapWidget(form):
                    class Meta:
                        model = form.Meta.model
                        fields = getattr(form.Meta, "fields", [])
                        exclude = getattr(form.Meta, "exclude", [])
                        widgets = new_widgets

                return FormWithMapWidget

            return make_form_class(base_form)

        return base_form
