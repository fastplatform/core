from django.contrib import admin

from utils.admin.truncate_deleted_objects import TruncateDeleteObjectsMixin
from utils.admin.map import MapWidgetMixin
from utils.admin.json_editor import JSONEditorMixin


class CommonModelAdmin(
    TruncateDeleteObjectsMixin,
    JSONEditorMixin,
    MapWidgetMixin,
    admin.ModelAdmin
):
    pass
