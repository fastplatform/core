import warnings

from django.apps import apps
from django.conf import settings
from django.db import models, connections, router, OperationalError, DEFAULT_DB_ALIAS
from django.db.backends.ddl_references import (
    Columns,
    Table,
)
from django.db.backends.base.schema import BaseDatabaseSchemaEditor

from django.db.migrations import Migration
from django.db.migrations.operations.special import RunSQL
from django.db.migrations.loader import MigrationLoader
from django.db.migrations.autodetector import MigrationAutodetector
from django.db.migrations.questioner import NonInteractiveMigrationQuestioner
from django.db.migrations.state import ProjectState

from django.core.management.commands.makemigrations import (
    Command as MakeMigrationCommand,
)
from django.core.management.base import BaseCommand, CommandError


class Command(MakeMigrationCommand):
    help = "Set comments on the Postgres columns using the verbose_name and help_text from the Djanog models"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.verbosity = 1
        self.include_header = True
        self.dry_run = False

    def add_arguments(self, parser):
        parser.add_argument(
            "app", type=str, help="App to set comments for in the database"
        )

    def handle(self, *args, **kwargs):
        app_label = kwargs["app"]
        statements = self.generate_sql_statements(app_label)
        self.write_migration(app_label, statements)

    def generate_sql_statements(self, app_label):

        connection = connections["default"]
        editor = BaseDatabaseSchemaEditor(connection)

        sql_set_comment_on_column = "COMMENT ON COLUMN %(table)s.%(column)s IS %%s;"
        sql_set_comment_on_table = "COMMENT ON TABLE %(table)s IS %%s;"

        sql_statements = []

        for model_name, model in apps.all_models[app_label].items():
            table = Table(model._meta.db_table, editor.quote_name)
            fields = model._meta.get_fields(include_parents=False, include_hidden=True)

            for field in fields:

                # Filter out these types of fields as they are not DB columns
                if isinstance(
                    field,
                    (models.ManyToOneRel, models.ManyToManyField, models.ManyToManyRel),
                ):
                    continue

                if field.help_text:
                    comment = str(field.help_text)
                elif (
                    field.verbose_name
                    and field.verbose_name.lower()
                    != field.name.lower().replace("_", " ")
                ):
                    comment = str(field.verbose_name)
                else:
                    comment = None

                column = Columns(
                    model._meta.db_table, [field.column], editor.quote_name
                )

                statement = sql_set_comment_on_column % {
                    "table": table,
                    "column": column,
                }
                sql_statements += [(statement, (comment,))]

            comment = (
                model._meta.help_text if hasattr(model._meta, "help_text") else None
            )
            statement = sql_set_comment_on_table % {"table": table}
            sql_statements += [(statement, (comment,))]

        return sql_statements

    def write_migration(self, app_label, sql_statements):
        # Load the current graph state. Pass in None for the connection so
        # the loader doesn't try to resolve replaced migrations from DB.
        loader = MigrationLoader(None, ignore_no_migrations=True)

        # Raise an error if any migrations are applied before their dependencies.
        consistency_check_labels = {config.label for config in apps.get_app_configs()}
        # Non-default databases are only checked if database routers used.
        aliases_to_check = (
            connections if settings.DATABASE_ROUTERS else [DEFAULT_DB_ALIAS]
        )
        for alias in sorted(aliases_to_check):
            connection = connections[alias]
            if connection.settings_dict["ENGINE"] != "django.db.backends.dummy" and any(
                # At least one model must be migrated to the database.
                router.allow_migrate(
                    connection.alias, app_label, model_name=model._meta.object_name
                )
                for app_label in consistency_check_labels
                for model in apps.get_app_config(app_label).get_models()
            ):
                try:
                    loader.check_consistent_history(connection)
                except OperationalError as error:
                    warnings.warn(
                        "Got an error checking a consistent migration history "
                        "performed for database connection '%s': %s" % (alias, error),
                        RuntimeWarning,
                    )

        # Before anything else, see if there's conflicting apps and drop out
        # hard if there are any and they don't want to merge
        conflicts = loader.detect_conflicts()

        if app_label in conflicts:
            name_str = "; ".join(
                "%s in %s" % (", ".join(names), app)
                for app, names in conflicts[app_label].items()
            )
            raise CommandError(
                "Conflicting migrations detected; multiple leaf nodes in the "
                "migration graph: (%s).\nTo fix them run "
                "'python manage.py makemigrations --merge'" % name_str
            )

        # Set up autodetector
        questioner = NonInteractiveMigrationQuestioner(
            specified_apps=[app_label], dry_run=self.dry_run
        )
        autodetector = MigrationAutodetector(
            loader.project_state(),
            ProjectState.from_apps(apps),
            questioner,
        )

        migration = Migration("set_db_comments", app_label)
        migration.operations = [RunSQL(sql_statements)]

        changes = {app_label: [migration]}

        changes = autodetector.arrange_for_graph(
            changes=changes,
            graph=loader.graph,
            migration_name=f"set_db_comments_{app_label}",
        )

        self.write_migration_files(changes)
