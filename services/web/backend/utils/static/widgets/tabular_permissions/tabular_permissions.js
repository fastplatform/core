window.addEventListener("load", function () {
        (function ($) {
            function resetCounts(perm) {
                count = 0
                $('#tabular_permissions').find(`tr td.${perm}`).find('input').each(function (i, e) {
                    count += $(e).prop('checked')
                })
                $(`#user_${perm}_permissions_count`).text(count)
            }
            $(".related-widget-wrapper:has(table)").addClass('related-widget-wrapper-user-permissions');
            $('#perm_view_select_all').on('change', function () {
                var state = $(this).prop('checked');
                $('#tabular_permissions').find('tr td.view').find('input').each(function (i, e) {
                    $(e).prop('checked', state)
                })
                resetCounts('view')
            });
            $('#perm_add_select_all').on('change', function () {
                var state = $(this).prop('checked');
                $('#tabular_permissions').find('tr td.add').find('input').each(function (i, e) {
                    $(e).prop('checked', state)
                })
                resetCounts('add')
            });
            $('#perm_change_select_all').on('change', function () {
                var state = $(this).prop('checked');
                $('#tabular_permissions').find('tr td.change').find('input').each(function (i, e) {
                    $(e).prop('checked', state)
                })
                resetCounts('change')
            });
            $('#perm_delete_select_all').on('change', function () {
                var state = $(this).prop('checked');
                $('#tabular_permissions').find('tr td.delete').find('input').each(function (i, e) {
                    $(e).prop('checked', state)
                })
                resetCounts('delete')
            });
            $('#tabular_permissions').find('tr td.view').find('input').on('change', function () {
                resetCounts('view')
            })
            $('#tabular_permissions').find('tr td.add').find('input').on('change', function () {
                resetCounts('add')
            })
            $('#tabular_permissions').find('tr td.change').find('input').on('change', function () {
                resetCounts('change')
            })
            $('#tabular_permissions').find('tr td.delete').find('input').on('change', function () {
                resetCounts('delete')
            })
            $('#tabular_permissions').find('tr td.custom').find('input').on('change', function () {
                resetCounts('custom')
            })
            $('.select-all.select-row').on('change', function () {
                var $this = $(this);
                $this.parents('tr').find('.checkbox').not('.select-all').each(function (i, elem) {
                    $(elem).prop('checked', $this.prop('checked'));
                })
                resetCounts('view')
                resetCounts('add')
                resetCounts('change')
                resetCounts('delete')
                resetCounts('custom')
            });
            $('form').on('submit', function () {
                var user_perms = [];
                var table_permissions = $('#tabular_permissions');
                var input_name = table_permissions.attr('data-input-name');
                table_permissions.find("input[type=checkbox]").not('.select-all').each(function (i, elem) {
                    var $elem = $(elem);
                    if ($(elem).prop('checked')) {
                        user_perms.push($elem.attr('data-perm-id'))
                    }
                });
                var user_group_permissions = $('[name=' + input_name + ']');
                var output = [];
                $.each(user_perms, function (key, value) {
                    output.push('<option value="' + value + '" selected="selected" style="display:none"></option>');
                });
                user_group_permissions.append(output);
            })
        })(django.jQuery);
});