# Generated by Django 3.0.7 on 2021-01-04 15:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('add_ons', '0014_remove_stale_contenttypes'),
    ]

    operations = [
        migrations.RunSQL(
            sql='ALTER TABLE "provider_member" DROP CONSTRAINT "provider_member_provider_id_79b4bf55_fk_provider_id";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "provider_member" ADD CONSTRAINT "provider_member_provider_id_79b4bf55_fk_provider_id" FOREIGN KEY ("provider_id") REFERENCES "provider" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "provider_member" DROP CONSTRAINT "provider_member_user_id_e04694f8_fk_user_username";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "provider_member" ADD CONSTRAINT "provider_member_user_id_e04694f8_fk_user_username" FOREIGN KEY ("user_id") REFERENCES "user" ("username") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on" DROP CONSTRAINT "add_on_provider_id_67a11081_fk_provider_id";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on" ADD CONSTRAINT "add_on_provider_id_67a11081_fk_provider_id" FOREIGN KEY ("provider_id") REFERENCES "provider" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_api_key" DROP CONSTRAINT "add_on_api_key_add_on_id_7d636ee5_fk_add_on_id";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_api_key" ADD CONSTRAINT "add_on_api_key_add_on_id_7d636ee5_fk_add_on_id" FOREIGN KEY ("add_on_id") REFERENCES "add_on" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_api_key" DROP CONSTRAINT "add_on_api_key_generated_by_id_92714792_fk_user_username";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_api_key" ADD CONSTRAINT "add_on_api_key_generated_by_id_92714792_fk_user_username" FOREIGN KEY ("generated_by_id") REFERENCES "user" ("username") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_subscription" DROP CONSTRAINT "add_on_subscription_add_on_id_e332ef2b_fk_add_on_id";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_subscription" ADD CONSTRAINT "add_on_subscription_add_on_id_e332ef2b_fk_add_on_id" FOREIGN KEY ("add_on_id") REFERENCES "add_on" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_subscription" DROP CONSTRAINT "add_on_subscription_holding_id_096cbf29_fk_holding_id";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_subscription" ADD CONSTRAINT "add_on_subscription_holding_id_096cbf29_fk_holding_id" FOREIGN KEY ("holding_id") REFERENCES "holding" ("id") ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED;',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_subscription" DROP CONSTRAINT "add_on_subscription_subscribed_by_id_90e795a8_fk_user_username";',
        ),
        migrations.RunSQL(
            sql='ALTER TABLE "add_on_subscription" ADD CONSTRAINT "add_on_subscription_subscribed_by_id_90e795a8_fk_user_username" FOREIGN KEY ("subscribed_by_id") REFERENCES "user" ("username") ON DELETE RESTRICT ON UPDATE RESTRICT DEFERRABLE INITIALLY DEFERRED;',
        ),
    ]
