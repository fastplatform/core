from django.db import migrations


def remove_stale_contenttypes(apps, schema_editor):
    ContentType = apps.get_model("contenttypes.ContentType")
    ContentType.objects.filter(
        model="subscribedaddon",
        app_label="add_ons",
    ).delete()


class Migration(migrations.Migration):

    dependencies = [
        ("add_ons", "0013_remove_old_permissions"),
    ]

    operations = [
        migrations.RunPython(remove_stale_contenttypes),
    ]
