from django.db import migrations


def remove_old_permissions(apps, schema_editor):
    ContentType = apps.get_model("contenttypes.ContentType")
    Permission = apps.get_model("auth.Permission")
    content_type = ContentType.objects.filter(
        model="subscribedaddon",
        app_label="add_ons",
    ).first()
    if content_type is not None:
        Permission.objects.filter(content_type=content_type).delete()


class Migration(migrations.Migration):

    dependencies = [
        ("add_ons", "0012_auto_20210104_1152"),
    ]

    operations = [
        migrations.RunPython(remove_old_permissions),
    ]
