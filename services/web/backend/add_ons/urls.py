from django.urls import path

from add_ons.views import AddOnCallbackView, GraphiQLView

urlpatterns = [
    path(
        "callback/<add_on_id>/",
        AddOnCallbackView.as_view(),
        name="callback",
    ),
    path(
        "graphiql/",
        GraphiQLView.as_view(),
        name="graphiql",
    )
]