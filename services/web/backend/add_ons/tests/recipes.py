from model_bakery.recipe import Recipe, foreign_key, related

from add_ons.models import (
    AddOn,
    Provider,
    AddOnAPIKey,
    AddOnSubscription,
    APIAccess
)

from authentication.tests.recipes import group_recipe
from farm.tests.recipes import holding_recipe

provider_recipe = Recipe(Provider)

add_on_recipe = Recipe(AddOn, is_visible=True, provider=foreign_key(provider_recipe))

add_on_api_key_recipe = Recipe(AddOnAPIKey, add_on=foreign_key(add_on_recipe))

add_on_subscription_recipe = Recipe(
    AddOnSubscription,
    add_on=foreign_key(add_on_recipe),
    holding=foreign_key(holding_recipe),
)

api_access_recipe = Recipe(APIAccess, is_visible=False)


