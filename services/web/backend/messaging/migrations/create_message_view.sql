CREATE
OR REPLACE VIEW "message" AS
SELECT
  CONCAT('b-', id) AS id,
  'broadcast' AS message_type,
  id AS message_id,
  id AS broadcast_id,
  NULL AS ticket_id,
  NULL AS conversation_id,  
  sent_at AS last_updated_at
FROM
  broadcast
UNION ALL
SELECT
  CONCAT('t-', t.id) AS id,
  'ticket' AS message_type,
  t.id AS message_id,
  NULL AS broadcast_id,
  t.id AS ticket_id,
  NULL AS conversation_id,
  COALESCE(tm.created_at, t.created_at) AS last_updated_at
FROM (
  ticket t
  LEFT JOIN (
    SELECT
      DISTINCT ON (ticket_id) ticket_id, created_at
    FROM
      ticket_message
    ORDER BY
      ticket_id, id DESC
  ) AS tm 
  ON tm.ticket_id = t.id
);