from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [("messaging", "0012_auto_20201119_1455")]

    operations = [
        migrations.RunSQL(
            (Path(__file__).parent / Path("create_message_view.sql")).read_text()
        )
    ]
