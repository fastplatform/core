from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [("messaging", "0025_auto_20201214_0949")]

    operations = [
        migrations.RunSQL(
            sql="ALTER TABLE broadcast_user_read_status ALTER COLUMN read_at SET DEFAULT now();",
        ),
        migrations.RunSQL(
            sql="ALTER TABLE ticket_user_read_status ALTER COLUMN read_at SET DEFAULT now();",
        ),
    ]
