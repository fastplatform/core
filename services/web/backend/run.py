#!/usr/bin/env python
import bjoern
import django
import glob
import os
import subprocess
import sys

from django.conf import settings
from django.core.management import (
    call_command,
    execute_from_command_line,
)
from django.core.management.base import CommandParser
from django.db import connections

from logging import StreamHandler

from opentelemetry import trace
from opentelemetry.exporter.zipkin.json import Protocol
from opentelemetry.exporter.zipkin.json import ZipkinExporter
from opentelemetry.instrumentation.django import DjangoInstrumentor
from opentelemetry.instrumentation.psycopg2 import Psycopg2Instrumentor
from opentelemetry.propagate import set_global_textmap
from opentelemetry.propagators.b3 import B3SingleFormat
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace.sampling import ParentBased, TraceIdRatioBased

from pathlib import Path, PurePath

from psycopg2 import sql

from requestlogger import ApacheFormatter, WSGILogger

from tabnanny import check

HOST = os.environ.get("HOST", "0.0.0.0")
PORT = int(os.environ.get("PORT", 8000))


def collectstatic(args):
    call_command("collectstatic", "--clear", "--no-input")


def creatersakey(args):
    manage_py = PurePath(__file__).parent / "manage.py"
    subprocess.run(
        [
            "python",
            manage_py,
            "creatersakey",
            "--settings=fastplatform.settings.migration",
        ],
        check=True,
    )


def init(args):
    import_directory = args.import_directory
    files = glob.glob(
        os.path.join(
            import_directory,
            "*.csv",
        )
    )
    files.sort()
    tables = [os.path.basename(f).split(".")[1] for f in files]

    if not files:
        print("No CSV file found !")
        return

    if args.clear:
        with connections[args.db_alias].cursor() as cursor:
            r = sql.SQL(
                "TRUNCATE {} CASCADE".format(",".join([f"public.{t}" for t in tables]))
            )
            cursor.execute(r)
            print("TRUNCATE {} OK".format(tables))

    for file, table in list(zip(files, tables)):
        call_command("import-copy", table, file, db_alias=args.db_alias)
        print("COPY {} OK".format(os.path.basename(file)))


def runserver(args):
    # Run Django migrations in a subprocess to use settings configured
    # with a Postgres superuser
    manage_py = PurePath(__file__).parent / "manage.py"
    subprocess.run(
        [
            "python",
            manage_py,
            "migrate",
            "--database=default",
            "--settings=fastplatform.settings.migration",
        ],
        check=True,
    )
    subprocess.run(
        [
            "python",
            manage_py,
            "migrate",
            "--database=external",
            "--settings=fastplatform.settings.migration",
        ],
        check=True,
    )

    # Reset Postgres sequences, in case they are off sync due to a PGCOPY data import
    sql_sequence_reset = subprocess.check_output(
        [
            "python",
            manage_py,
            "sqlsequencereset",
            "add_ons",
            "authentication",
            "common",
            "configuration",
            "farm",
            "fieldbook",
            "messaging",
            "photos",
            "soil",
            "--database=default",
            "--settings=fastplatform.settings.migration",
        ]
    )

    with connections["default"].cursor() as cursor:
        r = sql.SQL(sql_sequence_reset.decode("utf-8"))
        cursor.execute(r)

    sql_sequence_reset = subprocess.check_output(
        [
            "python",
            manage_py,
            "sqlsequencereset",
            "external",
            "--database=external",
            "--settings=fastplatform.settings.migration",
        ]
    )
    with connections["external"].cursor() as cursor:
        r = sql.SQL(sql_sequence_reset.decode("utf-8"))
        cursor.execute(r)

    zipkin_exporter = ZipkinExporter(
        version=Protocol.V2,
        endpoint=os.environ.get(
            "OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT",
            "http://127.0.0.1:9411/api/v2/spans",
        ),
    )

    sampler = TraceIdRatioBased(
        float(os.environ.get("OPENTELEMETRY_SAMPLING_RATIO", 0.1))
    )

    trace.set_tracer_provider(
        TracerProvider(
            resource=Resource.create(
                {
                    SERVICE_NAME: os.environ.get(
                        "OPENTELEMETRY_SERVICE_NAME", "web-backend"
                    )
                }
            ),
            sampler=ParentBased(sampler),
        )
    )

    trace.get_tracer_provider().add_span_processor(BatchSpanProcessor(zipkin_exporter))

    set_global_textmap(B3SingleFormat())

    DjangoInstrumentor().instrument()
    Psycopg2Instrumentor().instrument()

    # Debuging instrumentation
    os.environ.setdefault("DJANGO_REMOTE_DEBUG", "False")
    if os.environ.get("DJANGO_REMOTE_DEBUG").upper() == "TRUE":
        import debugpy

        debugpy.listen(5678)

    log_level = settings.LOGGING["root"]["level"]
    from fastplatform.wsgi import application

    app = WSGILogger(application, [StreamHandler(sys.stdout)], ApacheFormatter())
    app.logger.setLevel(log_level)
    bjoern.run(app, HOST, PORT)


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fastplatform.settings")
    os.environ.setdefault("OTEL_PYTHON_DJANGO_INSTRUMENT", "False")

    django.setup()

    parser = CommandParser()
    subcommand_parser = parser.add_subparsers(
        dest="subcommand",
        required=True,
        title="subcommands",
    )

    collectstatic_parser = subcommand_parser.add_parser(
        "collectstatic", help="collect Django static files"
    )
    collectstatic_parser.set_defaults(func=collectstatic)

    creatersakey_parser = subcommand_parser.add_parser(
        "creatersakey", help="Randomly generate a new RSA key for the OpenID server"
    )
    creatersakey_parser.set_defaults(func=creatersakey)

    init_parser = subcommand_parser.add_parser(
        "init", help="initialize Django with data"
    )
    init_parser.add_argument(
        "import_directory",
        metavar="import-directory",
        nargs="?",
        help="directory with the files to be imported (default: 'current working directory')",
        type=str,
        default=os.getcwd(),
    )
    init_parser.add_argument(
        "--db-alias",
        help="Django database alias to use (default='default')",
        type=str,
        default="default",
    )
    init_parser.add_argument(
        "--clear",
        help="Truncate tables before importing data",
        action="store_true",
    )
    init_parser.set_defaults(func=init)

    runserver_parser = subcommand_parser.add_parser(
        "runserver", help="run Django server (default)"
    )
    runserver_parser.set_defaults(func=runserver)

    args = parser.parse_args(sys.argv[1:] if len(sys.argv[1:]) > 0 else ["runserver"])
    args.func(args)


if __name__ == "__main__":
    main()
