import json

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from photos.models import GeoTaggedPhoto


def on_delete_geo_tagged_photo(request):
    return JsonResponse({"success": True, "status_code": 200})


@csrf_exempt
def update_thumbnail(request):

    payload = json.loads(request.body.decode("UTF-8"))
    new = payload["event"]["data"]["new"]

    photo = GeoTaggedPhoto.objects.get(pk=new["id"])
    photo.create_thumbnail()

    return JsonResponse({"success": True, "status_code": 200})
