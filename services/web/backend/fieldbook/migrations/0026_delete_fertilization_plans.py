from django.db import migrations

from fieldbook.models import FertilizationPlan


def delete_fertilization_plans(apps, schema_editor):
    FertilizationPlan.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ("fieldbook", "0025_auto_20210115_1729"),
    ]

    operations = [
        migrations.RunPython(delete_fertilization_plans),
    ]
