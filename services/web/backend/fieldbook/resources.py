from common.resources import ModelResourceWithColumnIds

from fieldbook.models import (
    PlantSpeciesGroup,
    PlantSpecies,
    PlantVariety,
    ChemicalElement,
    DeterminationMethod,
    CustomFertilizer,
    FertilizerType,
    CustomFertilizerElement,
    RegisteredFertilizer,
    RegisteredFertilizerElement,
    FertilizationPlan,
)


class PlantSpeciesGroupResource(ModelResourceWithColumnIds):
    class Meta:
        model = PlantSpeciesGroup


class PlantSpeciesResource(ModelResourceWithColumnIds):
    class Meta:
        model = PlantSpecies


class PlantVarietyResource(ModelResourceWithColumnIds):
    class Meta:
        model = PlantVariety


class ChemicalElementResource(ModelResourceWithColumnIds):
    class Meta:
        model = ChemicalElement


class DeterminationMethodResource(ModelResourceWithColumnIds):
    class Meta:
        model = DeterminationMethod


class FertilizerTypeResource(ModelResourceWithColumnIds):
    class Meta:
        model = FertilizerType


class CustomFertilizerResource(ModelResourceWithColumnIds):
    class Meta:
        model = CustomFertilizer


class CustomFertilizerElementResource(ModelResourceWithColumnIds):
    class Meta:
        model = CustomFertilizerElement


class RegisteredFertilizerResource(ModelResourceWithColumnIds):
    class Meta:
        model = RegisteredFertilizer


class RegisteredFertilizerElementResource(ModelResourceWithColumnIds):
    class Meta:
        model = RegisteredFertilizerElement


class FertilizationPlanResource(ModelResourceWithColumnIds):
    class Meta:
        model = FertilizationPlan
