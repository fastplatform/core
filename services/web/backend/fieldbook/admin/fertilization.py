from django.contrib import admin
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.contrib.postgres.aggregates import StringAgg

from import_export.admin import ImportMixin, ExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin

from fieldbook.models import (
    CustomFertilizer,
    RegisteredFertilizer,
    CustomFertilizerElement,
    RegisteredFertilizerElement,
    ChemicalElement,
    DeterminationMethod,
    FertilizerType,
    FertilizationPlan,
)
from fieldbook.resources import (
    FertilizationPlanResource,
    ChemicalElementResource,
    DeterminationMethodResource,
    FertilizerTypeResource,
    RegisteredFertilizerResource,
    CustomFertilizerResource,
    RegisteredFertilizerElementResource,
    CustomFertilizerElementResource,
)


class DeterminationMethodAdmin(ImportMixin, ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = DeterminationMethodResource
    search_fields = ("id", "label", "description")
    list_display = ("id", "label", "description")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


if settings.ENABLE_REGISTERED_FERTILIZER or settings.ENABLE_CUSTOM_FERTILIZER:
    admin_site.register(DeterminationMethod, DeterminationMethodAdmin)


class ChemicalElementAdmin(ImportMixin, ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = ChemicalElementResource
    search_fields = ("id", "label", "definition")
    list_display = ("id", "label", "definition")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


if settings.ENABLE_REGISTERED_FERTILIZER or settings.ENABLE_CUSTOM_FERTILIZER:
    admin_site.register(ChemicalElement, ChemicalElementAdmin)


class FertilizerTypeAdmin(ImportMixin, ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = FertilizerTypeResource
    ordering = ["parent", "label"]
    search_fields = ("id", "label", "definition")
    list_display = ("id", "label", "definition")

    def get_fieldsets(self, request, obj=None):
        fieldsets = [
            [None, {"fields": ("id", "label", "description", "definition", "parent")}]
        ]
        if settings.ENABLE_MODEL_TRANSLATIONS:
            fieldsets.append(
                [_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}]
            )

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["id"]
        return []


if settings.ENABLE_REGISTERED_FERTILIZER or settings.ENABLE_CUSTOM_FERTILIZER:
    admin_site.register(FertilizerType, FertilizerTypeAdmin)


class RegisteredFertilizerElementAdmin(ImportMixin, ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = RegisteredFertilizerElementResource
    autocomplete_fields = ("chemical_element",)
    fields = [
        "fertilizer",
        "chemical_element",
        "percentage",
        "efficiency",
        "determination_method",
    ]

    list_display = [
        "chemical_element",
        "percentage",
        "fertilizer",
    ]
    list_display_links = ["chemical_element", "percentage"]


if settings.ENABLE_REGISTERED_FERTILIZER:
    admin_site.register(RegisteredFertilizerElement, RegisteredFertilizerElementAdmin)


class RegisteredFertilizerElementInline(admin.TabularInline):
    model = RegisteredFertilizerElement
    autocomplete_fields = ("chemical_element",)
    verbose_name = _("element")
    verbose_name_plural = _("elements")
    extra = 0
    fields = ["chemical_element", "percentage", "efficiency", "determination_method"]


class RegisteredFertilizerAdmin(ImportMixin, ExportMixin, CommonModelAdmin):
    hide_from_dashboard = False
    resource_class = RegisteredFertilizerResource
    list_display = (
        "name",
        "reference",
        "fertilizer_type",
        "_chemical_elements",
        "organic",
    )
    list_select_related = ("fertilizer_type",)
    readonly_fields = ("_chemical_elements",)
    fieldsets = [
        [
            None,
            {
                "fields": [
                    "name",
                    "fertilizer_type",
                    "reference",
                    ("recommended_quantity", "recommended_quantity_unit_of_measure"),
                ]
            },
        ],
        [
            _("Additional data"),
            {"fields": ["additional_data"], "classes": ["collapse"]},
        ],
    ]

    inlines = [RegisteredFertilizerElementInline]

    def _chemical_elements(self, obj):
        return obj.chemical_elems

    _chemical_elements.short_description = _("chemical elements")

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related("fertilizer_type")
            .annotate(
                chemical_elems=StringAgg(
                    "fertilizer_elements__chemical_element__label", ", "
                )
            )
        )


if settings.ENABLE_REGISTERED_FERTILIZER:
    admin_site.register(RegisteredFertilizer, RegisteredFertilizerAdmin)


class CustomFertilizerElementInline(admin.TabularInline):
    model = CustomFertilizerElement
    autocomplete_fields = ("chemical_element",)
    extra = 0
    verbose_name = _("element")
    verbose_name_plural = _("elements")
    fields = ["chemical_element", "percentage", "efficiency", "determination_method"]


class CustomFertilizerElementAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = CustomFertilizerElementResource
    autocomplete_fields = ("chemical_element",)
    fields = [
        "fertilizer",
        "chemical_element",
        "percentage",
        "efficiency",
        "determination_method",
    ]

    list_display = [
        "chemical_element",
        "percentage",
        "fertilizer",
    ]
    list_display_links = ["chemical_element", "percentage"]


if settings.ENABLE_CUSTOM_FERTILIZER:
    admin_site.register(CustomFertilizerElement, CustomFertilizerElementAdmin)


class CustomFertilizerAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = CustomFertilizerResource
    raw_id_fields = ("holding",)
    list_display = (
        "name",
        "holding",
        "fertilizer_type",
        "organic",
        "created_by",
        "created_at",
    )
    list_select_related = ("fertilizer_type", "holding", "created_by")
    fields = ("name", "organic", "holding", "created_at", "created_by")
    readonly_fields = ("created_at", "created_by")
    inlines = [CustomFertilizerElementInline]

    def save_model(self, request, obj, form, change):
        if not hasattr(obj, "created_by") or not obj.created_by:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)


if settings.ENABLE_CUSTOM_FERTILIZER:
    admin_site.register(CustomFertilizer, CustomFertilizerAdmin)


class FertilizationPlanAdmin(ExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    resource_class = FertilizationPlanResource
    search_fields = ("name", "algorithm", "plot")
    list_display = ("id", "name", "algorithm", "plot")
    raw_id_fields = (
        "plot",
        "updated_by",
    )

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "id",
                    "name",
                    "algorithm",
                    "updated_by",
                    "updated_at",
                    "plot",
                    "parameters",
                    "results",
                )
            },
        ),
    )

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        readonly_fields += ("id", "updated_at")
        if obj:
            readonly_fields += ("updated_by",)
        return readonly_fields


admin_site.register(FertilizationPlan, FertilizationPlanAdmin)
