from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django import forms
from django.db import models

from import_export.admin import ImportExportMixin

from fastplatform.site import admin_site
from utils.admin import CommonModelAdmin
from fieldbook.models import PlantSpeciesGroup, PlantSpecies, PlantVariety
from fieldbook.resources import (
    PlantSpeciesGroupResource,
    PlantSpeciesResource,
    PlantVarietyResource,
)


class PlantVarietyAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    list_display = ("id", "name", "plant_species")
    list_display_links = ("id", "name")
    autocomplete_fields = ("plant_species",)
    search_fields = ("id", "name", "plant_species__name", "plant_species__group__name")
    ordering = ("name",)
    resource_class = PlantVarietyResource
    fieldsets = (
        (None, {"fields": ("id", "name", "plant_species", "notes")}),
        (_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}),
    )


admin_site.register(PlantVariety, PlantVarietyAdmin)


class PlantSpeciesGroupAdmin(ImportExportMixin, CommonModelAdmin):
    hide_from_dashboard = True
    list_display = ("id", "name", "parent")
    list_display_links = ("id", "name")
    autocomplete_fields = ("parent",)
    search_fields = ("id", "name", "parent__name")
    ordering = ("name",)
    resource_class = PlantSpeciesGroupResource
    fieldsets = (
        (None, {"fields": ("id", "name", "parent")}),
        (_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}),
    )


admin_site.register(PlantSpeciesGroup, PlantSpeciesGroupAdmin)


class PlantVarietyInlineForm(forms.ModelForm):
    class Meta:
        model = PlantVariety
        fields = ("id", "name", "notes")
        widgets = {
            "notes": forms.Textarea(attrs={"rows": 1, "cols": 60}),
        }


class PlantVarietyInline(admin.TabularInline):
    model = PlantVariety
    form = PlantVarietyInlineForm
    fields = ("id", "name", "notes", "i18n")
    search_fields = ("id", "name")
    verbose_name = _("variety")
    verbose_name_plural = _("varieties")
    ordering = ("name",)
    extra = 0


class PlantSpeciesAdmin(ImportExportMixin, CommonModelAdmin):
    resource_class = PlantSpeciesResource
    list_display = ("id", "name", "group", "_number_of_varieties")
    list_display_links = ("id", "name")
    search_fields = ("id", "name", "group__name")
    autocomplete_fields = ("group",)
    list_filter = ("group",)
    ordering = ("name",)
    readonly_fields = ("_number_of_varieties",)
    hide_from_dashboard = False
    fieldsets = (
        (None, {"fields": ("id", "name", "genus", "group", "notes")}),
        (_("Translations"), {"classes": ("collapse",), "fields": ("i18n",)}),
    )

    def _number_of_varieties(self, obj):
        return obj.number_of_varieties

    _number_of_varieties.short_description = _("number of varieties")

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related("group")
            .prefetch_related("plant_varieties")
            .annotate(number_of_varieties=models.Count("plant_varieties"))
        )

    inlines = [PlantVarietyInline]


admin_site.register(PlantSpecies, PlantSpeciesAdmin)
