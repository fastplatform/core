```plantuml
@startuml

    class "Explanation of the symbols used" as DESCRIPTION #FFF {
    - AutoField (identifiers)
    ..
    + Regular field (anything)
    ..
    # ForeignKey (ManyToMany)
    ..
    ~ ForeignKey (OneToOne, OneToMany)
    --
}


class "fieldbook.IrrigationMethod <Fieldbook & Agriculture>" as fieldbook.IrrigationMethod #e7f4d6 {
    irrigation method
    ..
    A status in the conversion to organic
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
fieldbook.IrrigationMethod *-- fieldbook.IrrigationMethod


class "fieldbook.DeterminationMethod <Fieldbook & Agriculture>" as fieldbook.DeterminationMethod #e7f4d6 {
    determination method
    ..
    A method for determining the composition of a product
    Origin: Estonian eFieldbook 2019 K-4
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
fieldbook.DeterminationMethod *-- fieldbook.DeterminationMethod


class "fieldbook.ChemicalElement <Fieldbook & Agriculture>" as fieldbook.ChemicalElement #e7f4d6 {
    chemical element
    ..
    An agronomically relevant chemical element
    Estonian eFieldbook 2019 K-6
    For example N, P, K, Ca, etc
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
fieldbook.ChemicalElement *-- fieldbook.ChemicalElement


class "fieldbook.FertilizerType <Fieldbook & Agriculture>" as fieldbook.FertilizerType #e7f4d6 {
    fertilizer type
    ..
    FertilizerType(i18n, id, label, definition, description, parent)
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + label (CharField) - 
    + definition (CharField) - 
    + description (TextField) - 
    ~ parent (ForeignKey) - 
    --
}
fieldbook.FertilizerType *-- fieldbook.FertilizerType


class "fieldbook.RegisteredFertilizer <Fieldbook & Agriculture>" as fieldbook.RegisteredFertilizer #e7f4d6 {
    registered fertilizer
    ..
    A fertilization product from a national database
    Derived from Estonian eFieldbook 2019 G-8.1 and K-14
    --
    + i18n (JSONField) - 
    - id (AutoField) - 
    + name (CharField) - 
    + additional_data (JSONField) - 
    + organic (BooleanField) - 
    ~ fertilizer_type (ForeignKey) - 
    + recommended_quantity (FloatField) - 
    ~ recommended_quantity_unit_of_measure (ForeignKey) - 
    + reference (CharField) - 
    --
}
fieldbook.RegisteredFertilizer *-- fieldbook.FertilizerType


class "fieldbook.RegisteredFertilizerElement <Fieldbook & Agriculture>" as fieldbook.RegisteredFertilizerElement #e7f4d6 {
    fertilizer element
    ..
    RegisteredFertilizerElement(id, chemical_element, percentage, efficiency,
determination_method, fertilizer)
    --
    - id (AutoField) - 
    ~ chemical_element (ForeignKey) - 
    + percentage (FloatField) - Percentage of mass, between 0 and 100.
    + efficiency (FloatField) - Percentage of efficiency, between 0 and 100.
Defaults to 100.
    ~ determination_method (ForeignKey) - 
    ~ fertilizer (ForeignKey) - 
    --
}
fieldbook.RegisteredFertilizerElement *-- fieldbook.ChemicalElement
fieldbook.RegisteredFertilizerElement *-- fieldbook.DeterminationMethod
fieldbook.RegisteredFertilizerElement *-- fieldbook.RegisteredFertilizer


class "fieldbook.CustomFertilizer <Fieldbook & Agriculture>" as fieldbook.CustomFertilizer #e7f4d6 {
    custom fertilizer
    ..
    A fertilization product that is custom-defined by the farmer for his farm
    Origin: derived from class G-8.1 from the Estonian eFieldbook, to take into
account the
    fact that not all fertilizers might be defined in the national database and
the farmer
    should be allowed to customize his own products for his farm
    --
    + i18n (JSONField) - 
    - id (AutoField) - 
    + name (CharField) - 
    + additional_data (JSONField) - 
    + organic (BooleanField) - 
    ~ fertilizer_type (ForeignKey) - 
    + recommended_quantity (FloatField) - 
    ~ recommended_quantity_unit_of_measure (ForeignKey) - 
    ~ holding (ForeignKey) - 
    ~ created_by (ForeignKey) - 
    + created_at (DateTimeField) - 
    --
}
fieldbook.CustomFertilizer *-- fieldbook.FertilizerType


class "fieldbook.CustomFertilizerElement <Fieldbook & Agriculture>" as fieldbook.CustomFertilizerElement #e7f4d6 {
    fertilizer element
    ..
    CustomFertilizerElement(id, chemical_element, percentage, efficiency,
determination_method, fertilizer)
    --
    - id (AutoField) - 
    ~ chemical_element (ForeignKey) - 
    + percentage (FloatField) - Percentage of mass, between 0 and 100.
    + efficiency (FloatField) - Percentage of efficiency, between 0 and 100.
Defaults to 100.
    ~ determination_method (ForeignKey) - 
    ~ fertilizer (ForeignKey) - 
    --
}
fieldbook.CustomFertilizerElement *-- fieldbook.ChemicalElement
fieldbook.CustomFertilizerElement *-- fieldbook.DeterminationMethod
fieldbook.CustomFertilizerElement *-- fieldbook.CustomFertilizer


class "fieldbook.FertilizationPlan <Fieldbook & Agriculture>" as fieldbook.FertilizationPlan #e7f4d6 {
    fertilization plan
    ..
    FertilizationPlan(id, plot, name, algorithm, parameters, results, updated_at,
updated_by)
    --
    - id (AutoField) - 
    ~ plot (ForeignKey) - 
    + name (CharField) - 
    ~ algorithm (ForeignKey) - 
    + parameters (JSONField) - 
    + results (JSONField) - 
    + updated_at (DateTimeField) - 
    ~ updated_by (ForeignKey) - 
    --
}


class "fieldbook.PlantSpecies <Fieldbook & Agriculture>" as fieldbook.PlantSpecies #e7f4d6 {
    plant species
    ..
    A plant species
    The content of the list is derived from the European plant variety
    database.
    Origin: semantics from code list K-12 of the Estonian eFieldbook
    See also:
    - https://www.unece.org/fileadmin/DAM/cefact/brs/BRS_eCROP_v1.pdf
    - http://ec.europa.eu/food/plant/plant_propagation_material/plant_variety_ca
talogues_databases/search//public/index.cfm?event=SearchForm&ctl_type=A
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - 
    + genus (CharField) - Standard latin name of the plant species
    ~ group (ForeignKey) - 
    + notes (TextField) - 
    --
}
fieldbook.PlantSpecies *-- fieldbook.PlantSpeciesGroup


class "fieldbook.PlantSpeciesGroup <Fieldbook & Agriculture>" as fieldbook.PlantSpeciesGroup #e7f4d6 {
    plant species group
    ..
    A group of plant species
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    + name (CharField) - 
    ~ parent (ForeignKey) - The parent group of this group, in case of a
hierarchical structure
    --
}
fieldbook.PlantSpeciesGroup *-- fieldbook.PlantSpeciesGroup


class "fieldbook.PlantVariety <Fieldbook & Agriculture>" as fieldbook.PlantVariety #e7f4d6 {
    plant variety
    ..
    A plant variety from a given species
    The content of the list is derived from the European plant variety
    database.
    Origin: code list K-13 from the Estonian eFieldbook
    See also:
    - http://ec.europa.eu/food/plant/plant_propagation_material/plant_variety_ca
talogues_databases/search//public/index.cfm?event=SearchForm&ctl_type=A
    --
    + i18n (JSONField) - 
    + id (CharField) - 
    ~ plant_species (ForeignKey) - 
    + name (CharField) - The name of the variety, can be left empty to designate an
undetermined variety within a species
    + notes (TextField) - 
    --
}
fieldbook.PlantVariety *-- fieldbook.PlantSpecies


@enduml
```