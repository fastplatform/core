from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from utils.models import GetAdminURLMixin, TranslatableModel, validate_latin1


class PlantSpecies(GetAdminURLMixin, TranslatableModel):
    """A plant species

    The content of the list is derived from the European plant variety
    database.

    Origin: semantics from code list K-12 of the Estonian eFieldbook

    See also:
    - https://www.unece.org/fileadmin/DAM/cefact/brs/BRS_eCROP_v1.pdf
    - http://ec.europa.eu/food/plant/plant_propagation_material/plant_variety_catalogues_databases/search//public/index.cfm?event=SearchForm&ctl_type=A
    """

    id = models.CharField(
        primary_key=True,
        max_length=50,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    name = models.CharField(
        max_length=256, null=False, blank=False, verbose_name=_("name")
    )

    genus = models.CharField(
        null=True,
        blank=True,
        max_length=256,
        verbose_name=_("genus"),
        help_text=_("Standard latin name of the plant species"),
    )

    group = models.ForeignKey(
        "fieldbook.PlantSpeciesGroup",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name=_("species group"),
    )

    notes = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("notes"),
    )

    def __str__(self):
        if self.group:
            return f"{self.group.name}/{self.name}"
        return self.name

    class Meta:
        db_table = "plant_species"
        verbose_name = _("plant species")
        verbose_name_plural = _("plant species")
        help_text = _("Plant species as they are described in the IACS nomenclature")


class PlantSpeciesGroup(GetAdminURLMixin, TranslatableModel):
    """A group of plant species"""

    id = models.CharField(
        max_length=50,
        primary_key=True,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    name = models.CharField(
        max_length=256, null=False, blank=False, verbose_name=_("name")
    )

    parent = models.ForeignKey(
        "self",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name=_("parent group"),
        help_text=_(
            "The parent group of this group, in case of a hierarchical structure"
        ),
    )

    def __str__(self):
        if self.parent:
            return f"{str(self.parent)}/{self.name}"
        return self.name

    class Meta:
        db_table = "plant_species_group"
        verbose_name = _("plant species group")
        verbose_name_plural = _("plant species groups")
        help_text = _(
            "Plant species groups as they are described in the IACS nomenclature"
        )


class PlantVariety(GetAdminURLMixin, TranslatableModel):
    """A plant variety from a given species

    The content of the list is derived from the European plant variety
    database.

    Origin: code list K-13 from the Estonian eFieldbook

    See also:
    - http://ec.europa.eu/food/plant/plant_propagation_material/plant_variety_catalogues_databases/search//public/index.cfm?event=SearchForm&ctl_type=A
    """

    id = models.CharField(
        primary_key=True,
        max_length=50,
        verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    plant_species = models.ForeignKey(
        "fieldbook.PlantSpecies",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        verbose_name=_("species"),
        related_name="plant_varieties",
    )

    name = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_("name"),
        help_text=_(
            "The name of the variety, can be left empty to designate an undetermined variety within a species"
        ),
    )

    notes = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("notes"),
    )

    def __str__(self):
        if self.name:
            return f"{str(self.plant_species)}/{self.name}"
        return f"{str(self.plant_species)}/-"

    class Meta:
        verbose_name = _("plant variety")
        verbose_name_plural = _("plant varieties")
        db_table = "plant_variety"
        help_text = _(
            "Plant varieties of plant species, as they are described in the IACS nomenclature"
        )
