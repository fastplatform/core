from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import MinValueValidator, MaxValueValidator

from common.models import AbstractCodeList
from utils.models import GetAdminURLMixin, TranslatableModel


class AbstractAgriculturalMaterial(GetAdminURLMixin, TranslatableModel, models.Model):
    """A material/product that can be added to a plot during the course of an
    agricultural activity
    """

    id = models.AutoField(
        primary_key=True, editable=False, verbose_name=_("identifier")
    )

    name = models.CharField(
        max_length=128, null=False, blank=False, verbose_name=_("name")
    )

    additional_data = models.JSONField(
        null=True, blank=True, verbose_name=_("additional data")
    )

    def __str(self):
        return self.name

    class Meta:
        abstract = True
        ordering = (("name",),)


class IrrigationMethod(AbstractCodeList):
    """A status in the conversion to organic"""

    class Meta:
        verbose_name = _("irrigation method")
        verbose_name_plural = _("irrigation methods")
        db_table = "irrigation_method"
        help_text = _("A method of irrigation")


class DeterminationMethod(AbstractCodeList):
    """A method for determining the composition of a product

    Origin: Estonian eFieldbook 2019 K-4
    """

    class Meta:
        verbose_name = _("determination method")
        verbose_name_plural = _("determination methods")
        db_table = "determination_method"


class ChemicalElement(AbstractCodeList):
    """An agronomically relevant chemical element

    Estonian eFieldbook 2019 K-6
    For example N, P, K, Ca, etc
    """

    class Meta:
        verbose_name = _("chemical element")
        verbose_name_plural = _("chemical elements")
        db_table = "chemical_element"


class AbstractFertilizer(AbstractAgriculturalMaterial):
    """A fertilization material/product

    Estonian eFieldbook 2019
    """

    organic = models.BooleanField(
        default=False, null=False, blank=False, verbose_name=_("organic")
    )

    fertilizer_type = models.ForeignKey(
        to="fieldbook.FertilizerType",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_("fertilizer type"),
    )

    recommended_quantity = models.FloatField(
        null=True,
        blank=True,
        validators=[MinValueValidator(0)],
        verbose_name=_("recommended quantity"),
    )

    recommended_quantity_unit_of_measure = models.ForeignKey(
        to="common.UnitOfMeasure",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measure of recommended quantity"),
    )

    def __str__(self):
        return self.name

    class Meta:
        abstract = True
        constraints = (
            models.CheckConstraint(
                name="fertilizer_recommended_quantity_recommended_quantity_unit_of_measure_must_be_both_null_or_none_null",
                check=(
                    models.Q(recommended_quantity__isnull=True)
                    & models.Q(recommended_quantity_unit_of_measure__isnull=True)
                )
                | (
                    models.Q(recommended_quantity__isnull=False)
                    & models.Q(recommended_quantity_unit_of_measure__isnull=False)
                ),
            ),
            models.CheckConstraint(
                name="fertilizer_recommended_quantity_must_be_positive_or_null",
                check=(
                    models.Q(recommended_quantity__gte=0)
                    | models.Q(recommended_quantity__isnull=True)
                ),
            ),
        )


class FertilizerType(AbstractCodeList):
    class Meta:
        db_table = "fertilizer_type"
        verbose_name = _("fertilizer type")
        verbose_name_plural = _("fertilizer types")
        help_text = _("Types of fertilizers")


class AbstractFertilizerElement(models.Model):

    id = models.AutoField(
        primary_key=True, editable=False, verbose_name=_("identifier")
    )

    chemical_element = models.ForeignKey(
        to="fieldbook.ChemicalElement",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        verbose_name=_("chemical element"),
    )

    percentage = models.FloatField(
        verbose_name=_("percentage"),
        null=False,
        blank=False,
        validators=[MaxValueValidator(100), MinValueValidator(0)],
        help_text=_("""Percentage of mass, between 0 and 100."""),
    )

    efficiency = models.FloatField(
        verbose_name=_("efficiency"),
        null=False,
        blank=False,
        default=100,
        validators=[MaxValueValidator(100), MinValueValidator(0)],
        help_text=_(
            """Percentage of efficiency, between 0 and 100. Defaults to 100."""
        ),
    )

    determination_method = models.ForeignKey(
        to="fieldbook.DeterminationMethod",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        verbose_name=_("percentage determination method"),
    )

    class Meta:
        abstract = True

        constraints = (
            models.CheckConstraint(
                name="fertilizer_element_percentage_must_be_between_0_and_100",
                check=models.Q(percentage__gte=0) & models.Q(percentage__lte=100),
            ),
            models.CheckConstraint(
                name="fertilizer_element_efficiency_must_be_between_0_and_100",
                check=models.Q(efficiency__gte=0) & models.Q(efficiency__lte=100),
            ),
        )


class RegisteredFertilizer(AbstractFertilizer):
    """A fertilization product from a national database

    Derived from Estonian eFieldbook 2019 G-8.1 and K-14
    """

    reference = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=_("reference from the national register"),
    )

    class Meta:
        verbose_name = _("registered fertilizer")
        verbose_name_plural = _("registered fertilizers")
        db_table = "registered_fertilizer"
        help_text = _(
            "Fertilizers provided by the Paying Agency / FaST and common to all users"
        )


class RegisteredFertilizerElement(AbstractFertilizerElement):

    fertilizer = models.ForeignKey(
        to="fieldbook.RegisteredFertilizer",
        related_name="fertilizer_elements",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        verbose_name=_("fertilizer"),
    )

    class Meta:
        verbose_name = _("fertilizer element")
        verbose_name_plural = _("fertilizer elements")
        db_table = "registered_fertilizer_element"
        help_text = _(
            "A chemical element and its proportion within a registered fertilizer"
        )

        constraints = (
            models.UniqueConstraint(
                name="registered_fertilizer_element_fertilizer_id_chemical_element_id__unique",
                fields=("fertilizer", "chemical_element"),
            ),
        )


class CustomFertilizer(AbstractFertilizer):
    """A fertilization product that is custom-defined by the farmer for his farm

    Origin: derived from class G-8.1 from the Estonian eFieldbook, to take into account the
    fact that not all fertilizers might be defined in the national database and the farmer
    should be allowed to customize his own products for his farm
    """

    holding = models.ForeignKey(
        "farm.Holding",
        related_name="custom_fertilizers",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        verbose_name=_("farm"),
    )

    created_by = models.ForeignKey(
        "authentication.User",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        verbose_name=_("created by"),
    )

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name=_("creation date")
    )

    class Meta:
        verbose_name = _("custom fertilizer")
        verbose_name_plural = _("custom fertilizers")
        db_table = "custom_fertilizer"
        help_text = _(
            "Fertilizers that are private to a holding and that have been defined by a farmer"
        )


class CustomFertilizerElement(AbstractFertilizerElement):

    fertilizer = models.ForeignKey(
        to="fieldbook.CustomFertilizer",
        related_name="fertilizer_elements",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        verbose_name=_("fertilizer"),
    )

    class Meta:
        verbose_name = _("fertilizer element")
        verbose_name_plural = _("fertilizer elements")
        db_table = "custom_fertilizer_element"
        help_text = _(
            "A chemical element and its proportion within a custom/private fertilizer"
        )

        constraints = (
            models.UniqueConstraint(
                name="custom_fertilizer_element_fertilizer_id_chemical_element_id_unique",
                fields=("fertilizer", "chemical_element"),
            ),
        )


def fertilization_plan_default_parameters():
    return {"placeholder": True}


def fertilization_plan_default_results():
    return {"placeholder": True}


class FertilizationPlan(models.Model):
    id = models.AutoField(
        primary_key=True, editable=False, verbose_name=_("identifier")
    )

    plot = models.ForeignKey(
        to="farm.Plot",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="fertilization_plans",
        verbose_name=_("plot"),
    )

    name = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_("name")
    )

    algorithm = models.ForeignKey(
        to="add_ons.AddOn",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="fertilization_plans",
        verbose_name=_("algorithm"),
    )

    parameters = models.JSONField(
        default=fertilization_plan_default_parameters,
        null=False,
        blank=False,
        verbose_name=_("parameters"),
    )

    results = models.JSONField(
        default=fertilization_plan_default_parameters,
        null=False,
        blank=False,
        verbose_name=_("results"),
    )

    updated_at = models.DateTimeField(
        null=False, blank=False, auto_now=True, verbose_name=_("update date")
    )

    updated_by = models.ForeignKey(
        to="authentication.User",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name="fertilization_plans",
        verbose_name=_("updated by"),
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "fertilization_plan"
        verbose_name = _("fertilization plan")
        verbose_name_plural = _("fertilization plans")
        help_text = _(
            "A fertilization plan, as computed by an algorithm for a plot, and based on a set of input parameters"
        )
