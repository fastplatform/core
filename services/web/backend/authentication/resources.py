from django.contrib.auth.hashers import make_password

from import_export.resources import Diff
from diff_match_patch import diff_match_patch
from django.utils.encoding import force_str
from django.utils.safestring import mark_safe

from common.resources import ModelResourceWithColumnIds
from authentication.models import (
    Country,
    Language,
    Region,
    User,
    Group,
    FederatedProvider,
    FederatedProviderLogEntry,
    TermsAndConditionsConsentRequest,
    UserTermsAndConditionsConsent,
    UserIACSConsent,
)


class CountryResource(ModelResourceWithColumnIds):
    class Meta:
        model = Country
        skip_unchanged = True
        import_id_fields = ("iso_code",)


class LanguageResource(ModelResourceWithColumnIds):
    class Meta:
        model = Language
        import_id_fields = ("iso_code",)


class RegionResource(ModelResourceWithColumnIds):
    class Meta:
        model = Region


class UserResourceDiff(Diff):
    def _export_resource_fields(self, resource, instance):
        return [
            "******"
            if f.column_name == "password"
            else resource.export_field(f, instance)
            if instance
            else ""
            for f in resource.get_user_visible_fields()
        ]


class UserResource(ModelResourceWithColumnIds):
    def get_export_fields(self):
        """
        Remove the password from the exportable fields.
        """
        fields = super().get_export_fields()
        fields = [field for field in fields if field.column_name != "password"]
        return fields

    def get_diff_class(self):
        return UserResourceDiff

    def before_import_row(self, row, row_number=None, **kwargs):
        """Pre-process the user data before importing it.

        Args:
            row (OrderedDict): The current row as read from the file
            row_number (int, optional): The current row number. Defaults to None.

        Returns:
            Nothing. The row is modified in place.
        """
        row["id"] = row["username"]

        # Hash the password (it is provided in clear text in the base file)
        row["password"] = make_password(row["password"])

        # Convert boolean values to actual booleans
        row["is_active"] = row["is_active"].upper() == "TRUE"
        row["is_staff"] = row["is_staff"].upper() == "TRUE"
        row["is_superuser"] = row["is_superuser"].upper() == "TRUE"

    class Meta:
        model = User
        import_id_fields = ("username",)
        fields = (
            "username",
            "password",
            "first_name",
            "last_name",
            "name",
            "email",
            "is_superuser",
            "is_staff",
            "is_active",
            "region_id",
            "preferred_language_id",
        )
        skip_unchanged = True
        report_skipped = True


class GroupResource(ModelResourceWithColumnIds):
    class Meta:
        model = Group


class UserTermsAndConditionsConsentResource(ModelResourceWithColumnIds):
    class Meta:
        model = UserTermsAndConditionsConsent


class UserIACSConsentResource(ModelResourceWithColumnIds):
    class Meta:
        model = UserIACSConsent


class TermsAndConditionsConsentRequestResource(ModelResourceWithColumnIds):
    class Meta:
        model = TermsAndConditionsConsentRequest


class FederatedProviderResource(ModelResourceWithColumnIds):
    class Meta:
        model = FederatedProvider


class FederatedProviderLogEntryResource(ModelResourceWithColumnIds):
    class Meta:
        model = FederatedProviderLogEntry
