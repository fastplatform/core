from pathlib import Path

from gql import gql

from django.conf import settings
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.http import HttpResponseForbidden

ROOT_PATH = Path(__file__).parent


def remove_user_password(request, obj):
    """Remove a user's password, to prevent him/heer from logging in using the FaST
    authentication system and force him to use a federated provider.

    Args:
        request (HttpRequest): Current request
        obj (User): User
    """
    if not (
        request.user.has_perm("authentication.change_user") or request.user.is_superuser
    ):
        return HttpResponseForbidden()

    try:
        obj.set_unusable_password()
        obj.save()
        messages.add_message(
            request,
            messages.INFO,
            _("The user's password has been removed"),
        )
    except Exception as e:
        messages.add_message(
            request,
            messages.ERROR,
            _("There was a error when removing the user's password") + ": " + str(e),
        )


def insert_demo_holding(request, obj):
    """Upserts a demo holding for a user

    Args:
        request (HttpRequest): Current request
        obj (User): User
    """
    if not settings.ENABLE_INSERT_DEMO_HOLDING_FROM_ADMIN:
        messages.add_message(
            request,
            messages.ERROR,
            _("This feature is not activated."),
        )
        return

    if not (
        request.user.has_perm("farm.insert_demo_holding") or request.user.is_superuser
    ):
        return HttpResponseForbidden()

    try:
        mutation = (ROOT_PATH / "graphql/mutation_insert_demo_holding.gql").read_text()
        client = request.graphql_clients["fastplatform"]
        extra_headers = {"X-Hasura-User-Id": obj.username, "X-Hasura-Role": "farmer"}
        response = client.execute(gql(mutation), extra_headers=extra_headers)

        messages.add_message(
            request,
            messages.INFO,
            f"{_('The following demo farm has been created / updated')}: {response['insert_demo_holding']['holding_id']}.",
        )
    except Exception as e:
        messages.add_message(
            request,
            messages.ERROR,
            f"{_('There was a error when creating / updating a demo farm')}: {str(e)}",
        )


def sync_holdings(request, obj):
    """Retrieve the holdings of a user from the IACS system

    Note that this will not work if the IACS system needs a recent authentication
    token from the user (like is the case with CyL).

    Args:
        request (HttpRequest): Current request
        obj (User): User
    """

    if not settings.ENABLE_SYNC_HOLDINGS_FROM_ADMIN:
        messages.add_message(
            request,
            messages.ERROR,
            _("This feature is not activated."),
        )
        return

    if not (request.user.has_perm("farm.sync_holdings") or request.user.is_superuser):
        return HttpResponseForbidden()

    try:
        mutation = (ROOT_PATH / "graphql/mutation_sync_holdings.gql").read_text()
        client = request.graphql_clients["fastplatform"]
        extra_headers = {"X-Hasura-User-Id": obj.username, "X-Hasura-Role": "farmer"}
        response = client.execute(gql(mutation), extra_headers=extra_headers)

        messages.add_message(
            request,
            messages.INFO,
            f"{_('The following farms have been created / updated')}: {', '.join(response['sync_holdings']['holding_ids'])}. {_('You now need to synchronize the parcels of each farm (on the page of each farm)')}",
        )
    except Exception as e:
        messages.add_message(
            request,
            messages.ERROR,
            f"{_('There was a error when synchronizing the user farms from IACS')}: {str(e)}",
        )
