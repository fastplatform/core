from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [("authentication", "0027_auto_20210111_1250")]

    operations = [
        migrations.RunSQL(
            sql="ALTER TABLE user_terms_and_conditions_consent ALTER COLUMN consent_received_at SET DEFAULT now();",
        ),
    ]
