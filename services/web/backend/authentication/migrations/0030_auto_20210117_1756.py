from pathlib import Path

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("authentication", "0029_auto_20210116_1854"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="user",
            name="iacs_consent",
        ),
        migrations.RemoveField(
            model_name="user",
            name="is_first_connection",
        ),
        # We need to recreate this view as it gets somehow deleted by the 2 RemoveField
        # migrations above
        migrations.RunSQL(
            (
                Path(__file__).parent / Path("create_private_user_info_view.sql")
            ).read_text()
        ),
    ]
