from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0005_auto_20200629_1716')
    ]

    operations = [
        migrations.RunSQL((Path(__file__).parent / Path('create_private_user_info_view.sql')).read_text())
    ]
