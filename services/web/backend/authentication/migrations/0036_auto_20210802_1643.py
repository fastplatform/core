# Generated by Django 3.0.7 on 2021-08-02 14:43

import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0035_region_iso_code_to_3_chars'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='username',
            field=models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 255 characters or fewer. Letters, digits and @/./+/-/_ only. For users authenticated through a third party service, this username is determined by the the third party service. The username MUST be unique accross the FaST environment.', max_length=255, primary_key=True, serialize=False, validators=[django.core.validators.RegexValidator(message='Username can contain only letters ("a" to "z" and "A" to "Z"), digits ("0" to "9"), dashes ("-") and dots (".").', regex=re.compile('^[a-zA-Z0-9\\.\\-]{1,255}$'))], verbose_name='username'),
        ),
    ]
