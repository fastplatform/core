import django.core.validators
from django.db import migrations, models
from pathlib import Path


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0034_sql_sequence_reset_auth'),
    ]

    operations = [
        # Need to drop this view first, otherwidse it blocks the type change for region(id)
        migrations.RunSQL("DROP VIEW IF EXISTS public.private_user_info CASCADE;"),
        # Remove the formatting constraint, will be recreated later on
        migrations.RemoveConstraint(
            model_name='region',
            name='region_id_must_be_country_iso2_dash_region_iso2',
        ),
        migrations.RemoveConstraint(
            model_name='region',
            name='region_iso_code_must_follow_iso_3166_2',
        ),
        # Alter the column type of the foreign keys pointing to region(id)
        migrations.RunSQL(
            "ALTER TABLE region_language ALTER COLUMN region_id TYPE varchar(6);"
        ),
        migrations.RunSQL(
            "ALTER TABLE public.user ALTER COLUMN region_id TYPE varchar(6);"
        ),
        # Alter region(id)
        migrations.AlterField(
            model_name='region',
            name='id',
            field=models.CharField(help_text='The region identifier, as XX-YYY, where XX is the ISO 3166-1 alpha 2 code of the country and YY is the ISO 3166-2 alpha 2 code of the region.', max_length=6, primary_key=True, serialize=False, validators=[django.core.validators.RegexValidator(message='ISO code must be country ISO code dash region ISO code', regex='[a-z0-9]{2}-[a-z0-9]{2,3}')], verbose_name='identifier'),
        ),
        # Alter region(iso_code) <- this one has no FK pointing to it
        migrations.AlterField(
            model_name='region',
            name='iso_code',
            field=models.CharField(db_index=True, help_text='The ISO 3166-2 alpha 2 code of the region', max_length=3, validators=[django.core.validators.RegexValidator(message='ISO code must be a valid ISO 3166-2 alpha 2 code', regex='[a-z0-9]{2,3}')], verbose_name='ISO code'),
        ),
        # Reinstate the formatting constraints
        migrations.AddConstraint(
            model_name='region',
            constraint=models.CheckConstraint(check=models.Q(id__regex='[a-z0-9]{2}-[a-z0-9]{2,3}'), name='region_id_must_be_country_iso2_dash_region_iso2'),
        ),
        migrations.AddConstraint(
            model_name='region',
            constraint=models.CheckConstraint(check=models.Q(iso_code__regex='[a-z0-9]{2,3}'), name='region_iso_code_must_follow_iso_3166_2'),
        ),
        # Reinstate the foreign key of region_language, that was dropped by Postgres because of the type change of region(id)
        migrations.RunSQL(
            "ALTER TABLE region_language ADD CONSTRAINT region_language_region_id_5b78a7a8_fk_region_id FOREIGN KEY (region_id) REFERENCES region(id);"
        ),
        # Reinstate the view we had dropped at the first step
        migrations.RunSQL((Path(__file__).parent / Path('create_private_user_info_view.sql')).read_text())
    ]
