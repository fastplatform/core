from itertools import cycle

from model_bakery.recipe import Recipe, foreign_key, related, seq

from authentication.models import (
    User,
    Group,
    Country,
    Region,
    Language,
    FederatedProvider,
    TermsAndConditionsConsentRequest,
    UserTermsAndConditionsConsent,
    UserIACSConsent,
    FederatedProviderLogEntry,
)

user_recipe = Recipe(User, username=seq("toto"))

group_recipe = Recipe(Group)

language_recipe = Recipe(Language, iso_code=cycle(["ab", "aa", "af", "ak"]))

country_recipe = Recipe(
    Country,
    iso_code=cycle(["fr", "es", "it", "ee"]),
    default_language=foreign_key(language_recipe),
    languages=related(language_recipe, language_recipe),
)

region_recipe = Recipe(
    Region,
    id=cycle(["fr-01", "es-cl", "it-21", "be-wal"]),
    iso_code=cycle(["01", "cl", "21", "wal"]),
    country=foreign_key(country_recipe),
    default_language=foreign_key(language_recipe),
    languages=related(language_recipe, language_recipe),
)

terms_and_conditions_consent_request_recipe = Recipe(TermsAndConditionsConsentRequest)

user_terms_and_conditions_consent_recipe = Recipe(UserTermsAndConditionsConsent)

user_iacs_consent_recipe = Recipe(UserIACSConsent)

federated_provider_recipe = Recipe(
    FederatedProvider, id=cycle(["apia", "csam", "apa", "spid"])
)

federated_provider_log_entry_recipe = Recipe(
    FederatedProviderLogEntry,
    federated_provider=foreign_key(federated_provider_recipe),
)
