from django.contrib.contenttypes.models import ContentType
from farm.models.holding import UserRelatedParty
from time import sleep
from datetime import datetime

from django.test import TestCase, override_settings
from django.conf import settings
from django.contrib.auth.models import Permission

from oidc_provider.tests.app.utils import create_fake_client
from oidc_provider.lib.utils.token import create_token
from user_sessions.models import Session

from authentication.models import User
from add_ons.models import AddOn, AddOnSubscription, Provider, AddOnAPIKey
from farm.models import Holding


HASURA_WEBHOOK_URL = "/authentication/hasura/"
MEDIA_WEBHOOK_URL = "/authentication/media/"


class HasuraWebhookBearerTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.user, _ = User.objects.get_or_create(username="test-user")
        self.oidc_client = create_fake_client("code")

    def test_bearer_token_valid(self):
        """Test that we go through if the token is valid and check the response
        format
        """
        access_token, refresh_token, at_hash, token = create_token(
            self.user, self.oidc_client, []
        )
        token.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL, HTTP_AUTHORIZATION=f"Bearer {access_token}"
        )
        self.assertEqual(response.status_code, 200)
        response = response.json()
        self.assertTrue("X-Hasura-Date" in response)
        self.assertEqual(
            datetime, type(datetime.fromisoformat(response["X-Hasura-Date"]))
        )
        response.pop("X-Hasura-Date")
        self.assertDictEqual(
            response, {"X-Hasura-User-Id": "test-user", "X-Hasura-Role": "farmer"}
        )

    def test_bearer_token_valid_with_language(self):
        """Test that we go through if the token is valid and check the response
        format includes the language pass-through
        """
        access_token, refresh_token, at_hash, token = create_token(
            self.user, self.oidc_client, []
        )
        token.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Bearer {access_token}",
            HTTP_USER_LANGUAGE="gr",
        )
        self.assertEqual(response.status_code, 200)
        response = response.json()
        self.assertTrue("X-Hasura-Date" in response)
        self.assertEqual(
            datetime, type(datetime.fromisoformat(response["X-Hasura-Date"]))
        )
        response.pop("X-Hasura-Date")
        self.assertDictEqual(
            response,
            {
                "X-Hasura-User-Id": "test-user",
                "X-Hasura-Role": "farmer",
                "X-Hasura-Language": "gr",
            },
        )

    @override_settings(OIDC_TOKEN_EXPIRE=1)  # Expire after 1 second
    def test_bearer_token_expired(self):
        access_token, refresh_token, at_hash, token = create_token(
            self.user, self.oidc_client, []
        )
        token.save()
        sleep(2)  # Sleep longer than the expiry duration
        response = self.client.get(
            HASURA_WEBHOOK_URL, HTTP_AUTHORIZATION=f"Bearer {access_token}"
        )
        self.assertEqual(response.status_code, 401)

    def test_bearer_token_invalid(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL, HTTP_AUTHORIZATION=f"Bearer ABCD"
        )
        self.assertEqual(response.status_code, 401)

    def test_bearer_token_valid_but_user_inactive(self):
        access_token, refresh_token, at_hash, token = create_token(
            self.user, self.oidc_client, []
        )
        token.save()

        self.user.is_active = False
        self.user.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL, HTTP_AUTHORIZATION=f"Bearer {access_token}"
        )
        self.assertEqual(response.status_code, 401)

        self.user.is_active = True
        self.user.save()


class HasuraWebhookAPIKeyTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.user, _ = User.objects.get_or_create(username="test-user")
        provider, _ = Provider.objects.update_or_create(
            id="test_provider", defaults=dict(name="Test provider", is_active=True)
        )
        self.add_on, _ = AddOn.objects.update_or_create(
            id="test_add_on",
            defaults=dict(
                name="Test add-on",
                provider=provider,
                auto_subscribe=False,
                is_active=True,
            ),
        )
        self.fake_permission, _ = Permission.objects.get_or_create(
            codename="fake_view_permission", content_type_id=1
        )
        self.add_on.permissions.set([self.fake_permission])
        self.add_on.save()

        self.add_on_api_key = AddOnAPIKey.objects.create(
            add_on=self.add_on, generated_by=self.user
        )

    def test_api_key_valid(self):
        self.add_on.is_active = True
        self.add_on.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
        )
        self.assertEqual(response.status_code, 200)
        response = response.json()
        self.assertTrue("X-Hasura-Date" in response)
        self.assertEqual(
            datetime, type(datetime.fromisoformat(response["X-Hasura-Date"]))
        )
        response.pop("X-Hasura-Date")
        self.assertDictEqual(
            response,
            {
                "X-Hasura-AddOn-Id": "test_add_on",
                "X-Hasura-Role": "add_on",
                "X-Hasura-AddOn-Permissions": "{fake_view_permission}",
            },
        )

    def test_api_key_invalid(self):
        self.add_on.is_active = True
        self.add_on.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"ApiKey ABCD",
        )
        self.assertEqual(response.status_code, 401)

    def test_api_key_valid_but_add_on_inactive(self):
        self.add_on.is_active = False
        self.add_on.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
        )
        self.assertEqual(response.status_code, 401)

        self.add_on.is_active = True
        self.add_on.save()

    def test_api_key_valid_but_provider_inactive(self):
        self.add_on.provider.is_active = False
        self.add_on.provider.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
        )
        self.assertEqual(response.status_code, 401)

        self.add_on.provider.is_active = True
        self.add_on.provider.save()


class HasuraWebhookServiceKeyTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.username = "test-user"
        self.user, _ = User.objects.get_or_create(username=self.username)
        provider, _ = Provider.objects.update_or_create(
            id="test_provider", defaults=dict(name="Test provider", is_active=True)
        )
        self.add_on_id = "test_add_on"
        self.add_on, _ = AddOn.objects.get_or_create(
            id=self.add_on_id,
            defaults=dict(
                name="Test add-on",
                provider=provider,
                auto_subscribe=False,
                is_active=True,
            ),
        )

        self.mock_view_permission_codename = "view_plot"

        self.mock_view_permission = Permission.objects.get(
            codename=self.mock_view_permission_codename
        )

        self.add_on.permissions.add(self.mock_view_permission)

    def test_service_key_valid(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service {settings.API_GATEWAY_SERVICE_KEY}",
        )
        self.assertEqual(response.status_code, 200)
        response = response.json()
        self.assertTrue("X-Hasura-Date" in response)
        self.assertEqual(
            datetime, type(datetime.fromisoformat(response["X-Hasura-Date"]))
        )
        response.pop("X-Hasura-Date")
        self.assertDictEqual(response, {"X-Hasura-Role": "service"})

    def test_service_key_valid_force_user_id_farmer(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service {settings.API_GATEWAY_SERVICE_KEY}",
            HTTP_X_HASURA_USER_ID=self.username,
            HTTP_X_HASURA_ROLE="farmer",
        )
        self.assertEqual(response.status_code, 200)
        response = response.json()
        self.assertTrue("X-Hasura-Date" in response)
        self.assertEqual(
            datetime, type(datetime.fromisoformat(response["X-Hasura-Date"]))
        )
        response.pop("X-Hasura-Date")
        self.assertDictEqual(
            response, {"X-Hasura-Role": "farmer", "X-Hasura-User-Id": self.username}
        )

    def test_service_key_valid_force_user_id_wrong_role(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service {settings.API_GATEWAY_SERVICE_KEY}",
            HTTP_X_HASURA_USER_ID=self.username,
            HTTP_X_HASURA_ROLE="wrong",
        )
        self.assertEqual(response.status_code, 401)

    def test_service_key_valid_force_user_id_farmer_user_does_not_exist(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service {settings.API_GATEWAY_SERVICE_KEY}",
            HTTP_X_HASURA_USER_ID="some-other-user",
            HTTP_X_HASURA_ROLE="farmer",
        )
        self.assertEqual(response.status_code, 401)

    def test_service_key_valid_force_add_on_id_add_on(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service {settings.API_GATEWAY_SERVICE_KEY}",
            HTTP_X_HASURA_ADDON_ID=self.add_on_id,
            HTTP_X_HASURA_ROLE="add_on",
        )
        self.assertEqual(response.status_code, 200)
        response = response.json()
        self.assertTrue("X-Hasura-Date" in response)
        self.assertEqual(
            datetime, type(datetime.fromisoformat(response["X-Hasura-Date"]))
        )
        response.pop("X-Hasura-Date")
        self.assertDictEqual(
            response,
            {
                "X-Hasura-Role": "add_on",
                "X-Hasura-AddOn-Id": self.add_on_id,
                "X-Hasura-AddOn-Permissions": f"{{{self.mock_view_permission_codename}}}",
            },
        )

    def test_service_key_valid_force_add_on_id_wrong_role(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service {settings.API_GATEWAY_SERVICE_KEY}",
            HTTP_X_HASURA_ADDON_ID=self.add_on_id,
            HTTP_X_HASURA_ROLE="wrong",
        )
        self.assertEqual(response.status_code, 401)

    def test_service_key_valid_force_add_on_id_add_on_does_not_exist(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service {settings.API_GATEWAY_SERVICE_KEY}",
            HTTP_X_HASURA_ADDON_ID="some-other-add-on",
            HTTP_X_HASURA_ROLE="add_on",
        )
        self.assertEqual(response.status_code, 401)

    def test_service_key_invalid(self):

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service ABCD",
        )
        self.assertEqual(response.status_code, 401)

    def test_service_key_invalid_force_user_id_farmer(self):

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service ABCD",
            HTTP_X_HASURA_USER_ID=self.username,
            HTTP_X_HASURA_ROLE="farmer",
        )
        self.assertEqual(response.status_code, 401)

    def test_service_key_invalid_force_add_on_id_add_on(self):

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Service ABCD",
            HTTP_X_HASURA_USER_ID=self.add_on_id,
            HTTP_X_HASURA_ROLE="add_on",
        )
        self.assertEqual(response.status_code, 401)


class HasuraCommonWebhookAPIKeyTestCase(TestCase):
    def test_no_authorization_header(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
        )
        self.assertEqual(response.status_code, 401)

    def test_empty_authorization_header(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION="",
        )
        self.assertEqual(response.status_code, 401)

    def test_wrong_authorization_header(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION="ABCD",
        )
        self.assertEqual(response.status_code, 401)

    def test_wrong_authorization_type(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"HeyImWrong {settings.API_GATEWAY_SERVICE_KEY}",
        )
        self.assertEqual(response.status_code, 401)


@override_settings(AXES_ENABLED=False)
class HasuraWebhookSessionTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.username = "test-user"
        self.password = User.objects.make_random_password()
        user, _ = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = False
        user.is_active = True
        user.save()
        self.user = user
        self.client.login(username=self.username, password=self.password)

        self.session_key = Session.objects.filter(user=self.user).first().session_key

    def test_session_key_valid(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Staff {self.session_key}",
        )
        self.assertEqual(response.status_code, 200)
        response = response.json()
        self.assertTrue("X-Hasura-Date" in response)
        self.assertEqual(
            datetime, type(datetime.fromisoformat(response["X-Hasura-Date"]))
        )
        response.pop("X-Hasura-Date")
        self.assertDictEqual(
            response, {"X-Hasura-Role": "staff", "X-Hasura-User-Id": self.username}
        )

    def test_session_key_valid_user_superuser(self):
        self.user.is_staff = False
        self.user.is_superuser = True
        self.user.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Staff {self.session_key}",
        )
        self.assertEqual(response.status_code, 200)
        response = response.json()
        self.assertTrue("X-Hasura-Date" in response)
        self.assertEqual(
            datetime, type(datetime.fromisoformat(response["X-Hasura-Date"]))
        )
        response.pop("X-Hasura-Date")
        self.assertDictEqual(
            response, {"X-Hasura-Role": "staff", "X-Hasura-User-Id": self.username}
        )

        self.user.is_staff = True
        self.user.is_superuser = False
        self.user.save()

    def test_session_key_invalid(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Staff ABCD",
        )
        self.assertEqual(response.status_code, 401)

    def test_session_key_absent(self):
        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Staff",
        )
        self.assertEqual(response.status_code, 401)

    def test_session_key_valid_user_inactive(self):
        self.user.is_active = False
        self.user.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Staff {self.session_key}",
        )
        self.assertEqual(response.status_code, 401)

        self.user.is_active = True
        self.user.save()

    def test_session_key_valid_user_inactive_even_superuser(self):
        self.user.is_active = False
        self.user.is_superuser = True
        self.user.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Staff {self.session_key}",
        )
        self.assertEqual(response.status_code, 401)

        self.user.is_active = True
        self.user.is_superuser = False
        self.user.save()

    def test_session_key_valid_user_not_staff_not_superuser(self):
        self.user.is_staff = False
        self.user.is_superuser = False
        self.user.save()

        response = self.client.get(
            HASURA_WEBHOOK_URL,
            HTTP_AUTHORIZATION=f"Staff {self.session_key}",
        )
        self.assertEqual(response.status_code, 401)

        self.user.is_staff = True
        self.user.is_superuser = False
        self.user.save()


class MediaWebhookTestCase(TestCase):
    def setUp(self):
        super().setUp()

        self.username = "test-user"
        self.user, _ = User.objects.get_or_create(username=self.username)

        self.holding_id = "test_holding"
        self.holding, _ = Holding.objects.update_or_create(
            id=self.holding_id, defaults=dict(name="Test holding")
        )
        self.related_party, _ = UserRelatedParty.objects.update_or_create(
            holding=self.holding, user=self.user, defaults=dict(role="farmer")
        )

        self.provider, _ = Provider.objects.update_or_create(
            id="test_provider", defaults=dict(name="Test provider", is_active=True)
        )
        self.add_on_id = "test_add_on"
        self.add_on, _ = AddOn.objects.get_or_create(
            id=self.add_on_id,
            defaults=dict(
                name="Test add-on",
                provider=self.provider,
                auto_subscribe=False,
                is_active=True,
            ),
        )
        self.subscription, _ = AddOnSubscription.objects.update_or_create(
            add_on=self.add_on,
            holding=self.holding,
            defaults=dict(subscribed_by=self.user),
        )
        self.add_on_api_key = AddOnAPIKey.objects.create(
            add_on=self.add_on, generated_by=self.user
        )
        self.geo_tagged_photo_permission = Permission.objects.get(
            codename="view_geotaggedphoto"
        )
        self.add_on.permissions.add(self.geo_tagged_photo_permission)

        self.oidc_client = create_fake_client("code")
        self.access_token, refresh_token, at_hash, self.token = create_token(
            self.user, self.oidc_client, []
        )
        self.token.save()

    def assertMediaWebhookReponseFormat(self, response):
        self.assertTrue(response.has_header("Authorization"))
        self.assertIn("Credential", response.get("Authorization"))
        self.assertIn("SignedHeaders", response.get("Authorization"))
        self.assertIn("Signature", response.get("Authorization"))
        self.assertTrue(response.has_header("host"))
        self.assertTrue(response.has_header("x-amz-content-sha256"))
        self.assertTrue(response.has_header("x-amz-date"))

    # Geo-tagged photos and thumbnails

    # As API key / add-on

    def test_photo_url_apikey_ok(self):
        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
                )
                self.assertEqual(response.status_code, 200)
                self.assertMediaWebhookReponseFormat(response)

    def test_photo_url_apikey_add_on_inactive(self):
        self.add_on.is_active = False
        self.add_on.save()

        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_apikey_provider_inactive(self):
        self.provider.is_active = False
        self.provider.save()

        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_apikey_no_permission(self):
        self.add_on.permissions.remove(self.geo_tagged_photo_permission)

        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_apikey_not_subscribed(self):
        self.subscription.delete()

        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_api_key_not_subscribed_but_auto_subscribe(self):
        self.subscription.delete()
        self.add_on.auto_subscribe = True
        self.add_on.save()

        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"ApiKey {self.add_on_api_key.key}",
                )
                self.assertEqual(response.status_code, 200)
                self.assertMediaWebhookReponseFormat(response)

    # As bearer / farmer

    def test_photo_url_bearer_ok(self):
        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"Bearer {self.access_token}",
                )
                self.assertEqual(response.status_code, 200)
                self.assertMediaWebhookReponseFormat(response)

    def test_photo_url_bearer_user_inactive(self):
        self.user.is_active = False
        self.user.save()

        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"Bearer {self.access_token}",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_bearer_not_related_party(self):
        self.related_party.delete()

        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"Bearer {self.access_token}",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_bearer_wrong_token(self):
        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"Bearer ABCD",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_no_authorization(self):
        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/test.jpg",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_bearer_no_filename(self):
        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/{self.holding_id}/campaign/2020/{obj}/",
                    HTTP_AUTHORIZATION=f"Bearer {self.access_token}",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_bearer_wrong_holding(self):
        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding/wrong-holding-id/campaign/2020/{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"Bearer {self.access_token}",
                )
                self.assertEqual(response.status_code, 403)

    def test_photo_url_bearer_wrong_url(self):
        for obj in ["photo", "thumbnail"]:
            with self.subTest(obj=obj):
                response = self.client.get(
                    MEDIA_WEBHOOK_URL,
                    HTTP_X_ORIGINAL_METHOD="GET",
                    HTTP_X_ORIGINAL_URI=f"https://whatever.fastplatform.eu/{settings.AWS_STORAGE_BUCKET_NAME}/{settings.AWS_LOCATION}/photos/holding//campaign//{obj}/test.jpg",
                    HTTP_AUTHORIZATION=f"Bearer {self.access_token}",
                )
                self.assertEqual(response.status_code, 403)

    #
