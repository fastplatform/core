from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AuthenticationConfig(AppConfig):
    name = 'authentication'
    verbose_name = _('Authentication & Authorization')

    help_text = _('Mechanisms and providers used by FaST to authenticate users to a FaST user account and grant them access rights')
