import json
import hashlib
import hmac
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist

from django.views.generic.base import TemplateView
from django.conf import settings
from django.contrib.auth.views import LogoutView
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.contrib.admin.views.decorators import staff_member_required

from authentication.models import (
    TermsAndConditionsConsentRequest,
    UserTermsAndConditionsConsent,
)


class TermsAndConditionsConsentRequestView(TemplateView):

    template_name = "authentication/terms_and_conditions.html"
    http_method_names = ["get", "post"]

    consent_request = None

    @method_decorator(login_required, staff_member_required)
    def dispatch(self, request, *args, **kwargs):
        consent_request_id = kwargs.pop("consent_request_id", None)
        try:
            self.consent_request = TermsAndConditionsConsentRequest.objects.get(
                pk=consent_request_id
            )
        except ObjectDoesNotExist:
            # This request does not exist, redirect
            return self._redirect_to_next(request, *args, **kwargs)

        # The user has already consented, redirect
        if UserTermsAndConditionsConsent.objects.filter(
            consent_request=self.consent_request, user=request.user
        ).exists():
            return self._redirect_to_next(request, *args, **kwargs)

        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        # Add the consent
        UserTermsAndConditionsConsent.objects.create(
            consent_request=self.consent_request, user=request.user
        )

        return self._redirect_to_next(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["consent_request"] = self.consent_request

        return context

    def _redirect_to_next(self, request, *args, **kwargs):
        if request.method == "post":
            next_url = request.POST.get("next", "/")
        else:
            next_url = request.GET.get("next", "/")

        if next_url == request.path:
            next_url = "/"

        return HttpResponseRedirect(next_url)
