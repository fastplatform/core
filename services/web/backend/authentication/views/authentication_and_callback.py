from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import View
from authentication.models import FederatedProvider
from django.conf import settings


class AuthenticationView(View):

    http_method_names = ["get"]

    def get(self, request):
        # if request.user.is_authenticated and (
        #     request.user.is_staff or request.user.is_superuser
        # ):
        #     next_url = request.session.get("next_authentication", reverse('admin:index'))
        #     return redirect(next_url)

        # Store the value of the next GET parameter
        if "next" in request.GET:
            request.session["next_authentication"] = request.GET.get("next")

        context = {
            "display_fastplatform_connector": True,
            "local_support_email_address": settings.LOCAL_SUPPORT_EMAIL_ADDRESS,
            "web_app_url": settings.WEB_APP_URL,
        }

        if "federated_provider_used" in request.session:
            context["federated_provider_used"] = request.session[
                "federated_provider_used"
            ]

        # if federated_provider parameter is present get only this federated_provider
        # else get all federated providers available
        if "federated_provider" in request.GET:
            federated_provider = FederatedProvider.objects.filter(
                id=request.GET.get("federated_provider"), is_active=True
            ).first()

            if federated_provider is not None:
                federated_providers = [federated_provider]
                context["display_fastplatform_connector"] = False
                if (
                    "force_authentication" in request.GET
                    and request.GET.get("force_authentication") == "true"
                ):
                    context["force_authentication"] = True
            else:
                federated_providers = FederatedProvider.objects.filter(is_active=True)
        else:
            federated_providers = FederatedProvider.objects.filter(is_active=True)

        context["federated_providers"] = federated_providers
        return render(request, "authentication/index.html", context)


class AuthenticationCallbackView(View):

    http_method_names = ["get"]

    def get(self, request):

        if request.user.is_authenticated:
            next_url = request.session.get("next_authentication", "/admin")

            return redirect(next_url)

        else:
            if "federated_provider" in request.GET:
                return redirect(
                    "/authentication?federated_provider="
                    + request.GET.get("federated_provider")
                )
            else:
                return redirect("/authentication")
