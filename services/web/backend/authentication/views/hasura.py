from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from authentication.views.utils import identify


class HasuraAuthWebhookView(View):
    """
    Hasura Webhook
    """

    http_method_names = ["get"]

    @method_decorator(csrf_exempt)
    def get(self, request):
        # Check if an authorization value is present
        # If not return a 401 Unauthorized

        identity = identify(request)

        if not identity:
            return JsonResponse({}, status=401)
        else:
            return JsonResponse(identity, status=200)
