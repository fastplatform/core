from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_headers
from django.contrib.auth import views as auth_views
from django.urls import path, reverse_lazy

from . import views

urlpatterns = [
    path(
        "login/",
        auth_views.LoginView.as_view(template_name="authentication/login.html"),
        name="login",
    ),
    path("logout/", views.AuthenticationLogoutView.as_view(), name="logout"),
    path("oidc_logout/", views.OIDCLogoutView.as_view(), name="oidc_logout"),
    path(
        "password_reset/",
        auth_views.PasswordResetView.as_view(
            success_url=reverse_lazy("authentication:password_reset_done"),
            email_template_name="authentication/password_reset_email.html",
        ),
        name="password_reset",
    ),
    path(
        "password_reset/done/",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(
            success_url=reverse_lazy("authentication:password_reset_complete")
        ),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    path(
        "hasura/",
        cache_page(60)(
            vary_on_headers(
                "Authorization",
                "X-Hasura-User-Id",
                "X-Hasura-Role",
                "X-Hasura-AddOn-Id",
                "X-Hasura-AddOn-Permissions",
                "X-Hasura-Language",
                "User-Language",
            )(views.HasuraAuthWebhookView.as_view())
        ),
        name="hasura",
    ),
    path("media/", views.MediaAuthWebhookView.as_view()),
    path("", views.AuthenticationView.as_view(), name="index"),
    path("callback", views.AuthenticationCallbackView.as_view(), name="callback"),
    path(
        "terms_and_conditions/<consent_request_id>/",
        views.TermsAndConditionsConsentRequestView.as_view(),
        name="terms_and_conditions",
    ),
]
