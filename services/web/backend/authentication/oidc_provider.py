from datetime import timedelta

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout as django_user_logout
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth.signals import user_logged_out
from django.http import HttpResponse
from django.utils.translation import ugettext as _
from django.utils import timezone

from oidc_provider import settings as oidc_settings
from oidc_provider.lib.claims import ScopeClaims
from oidc_provider.lib.endpoints.authorize import AuthorizeEndpoint
from oidc_provider.lib.errors import AuthorizeError
from oidc_provider.lib.utils.authorize import strip_prompt_login

from add_ons.models import AddOn, ProviderMember

OIDC_TEMPLATES = oidc_settings.get("OIDC_TEMPLATES")


def oidc_after_userlogin_hook(request, user, client):
    authorize_endpoint_class = AuthorizeEndpoint
    authorize = authorize_endpoint_class(request)
    authorize.validate_params()

    if "login" in authorize.params["prompt"]:
        if "none" in authorize.params["prompt"]:
            raise AuthorizeError(
                authorize.params["redirect_uri"], "login_required", authorize.grant_type
            )
        else:
            django_user_logout(request)
            next_page = strip_prompt_login(request.get_full_path())
            return redirect_to_login(next_page, oidc_settings.get("OIDC_LOGIN_URL"))

    if "select_account" in authorize.params["prompt"]:
        # TODO: see how we can support multiple accounts for the end-user.
        if "none" in authorize.params["prompt"]:
            raise AuthorizeError(
                authorize.params["redirect_uri"],
                "account_selection_required",
                authorize.grant_type,
            )
        else:
            django_user_logout(request)
            return redirect_to_login(
                request.get_full_path(), oidc_settings.get("OIDC_LOGIN_URL")
            )

    if {"none", "consent"}.issubset(authorize.params["prompt"]):
        raise AuthorizeError(
            authorize.params["redirect_uri"], "consent_required", authorize.grant_type
        )

    # if client is an addon, check if user has permission to
    if client.client_id.split("_")[0] == "addon":

        addon_name = client.client_id.replace("addon_", "")
        addon = AddOn.objects.get(id=addon_name)
        provider = addon.provider

        if len(ProviderMember.objects.filter(provider=provider, user=user)) == 0:
            messages.add_message(
                request,
                messages.ERROR,
                f"{_('You are not authorized to access')} {addon_name}",
            )
            django_user_logout(request)
            return redirect_to_login(
                request.get_full_path(), oidc_settings.get("OIDC_LOGIN_URL")
            )

    # Build the redirect url
    response = HttpResponse("", status=302)
    response["Location"] = authorize.create_response_uri()

    # logout user if user is using fastplatform-mobile
    if client.client_id == "fastplatform-mobile":
        user_logged_out.send(sender=user.__class__, request=request, user=user)
        request.session.flush()
        if hasattr(request, "user"):
            from django.contrib.auth.models import AnonymousUser

            request.user = AnonymousUser()

    return response


def oidc_refresh_token_alive_hook(token, issued_at, user, access_has_expired):
    return timezone.now() < issued_at + timedelta(seconds=settings.OIDC_REFRESH_TOKEN_EXPIRE)

class CustomScopeClaims(ScopeClaims):

    info_groups = (
        _("Groups"),
        _("List all groups that user is a member of"),
    )

    def scope_groups(self):

        dic = {
            "groups": [],
        }

        for group in self.user.groups.all():
            dic["groups"].append(group.name)

        # if client is an addon and if the user is member of its provider
        # then add admin group to give it access to addon's django admin
        if self.client.client_id.split("_")[0] == "addon":

            addon_name = self.client.client_id.replace("addon_", "")
            addon = AddOn.objects.get(id=addon_name)
            provider = addon.provider

            if (
                len(ProviderMember.objects.filter(provider=provider, user=self.user))
                == 1
            ):
                dic["groups"].append("admin")

        return dic

    # If you want to change the description of the profile scope, you can redefine it.
    info_profile = (
        _("Profile"),
        _("Another description."),
    )