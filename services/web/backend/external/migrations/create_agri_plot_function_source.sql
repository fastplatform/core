CREATE OR REPLACE FUNCTION public.agri_plot_function_source(
        z integer,
        x integer,
        y integer
    ) RETURNS bytea AS $$
DECLARE mvt bytea;
BEGIN
SET enable_seqscan TO off;
SELECT INTO mvt ST_AsMVT(
        tile,
        'agri_plot',
        4096,
        'geom'
    )
FROM (
        SELECT ST_AsMVTGeom(
                ST_Transform(
                    ST_SimplifyPreserveTopology(a.geometry, 1 / (2 ^ z)),
                    3857
                ),
                TileBBox(z, x, y, 3857),
                4096,
                64,
                true
            ) AS geom,
            a.id,
            a.authority_id,
            a.plant_species_id,
            a.plant_variety_id,
            a.area
        FROM agri_plot a
            INNER JOIN agri_plot_version av ON a.version_id = av.id
        WHERE av.is_active
            AND ST_Intersects(
                a.geometry,
                ST_Transform(TileBBox(z, x, y, 3857), 4258)
            )
    ) as tile
WHERE z >= 13
    AND geom IS NOT NULL;
SET enable_seqscan TO on;
RETURN mvt;
END $$ LANGUAGE plpgsql VOLATILE STRICT PARALLEL SAFE;