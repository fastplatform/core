from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [('external', '0001_initial')]

    # create function to get entities from previously views adding the possibility to add a minZoom and maxZoom
    # If the zoom is outside the minZoom and maxZoom, then doesn't display rows

    operations = [
        migrations.RunSQL((Path(__file__).parent / Path(
            'create_geometry_to_geojson_cast.sql'
        )).read_text()),
        migrations.RunSQL(
            (Path(__file__).parent /
             Path('create_tilebbox_function.sql')).read_text()),
        migrations.RunSQL(
            (Path(__file__).parent /
             Path('create_surface_water_function_source.sql')).read_text()),
        migrations.RunSQL(
            (Path(__file__).parent /
             Path('create_water_course_function_source.sql')).read_text()),
        migrations.RunSQL(
            (Path(__file__).parent /
             Path('create_soil_site_function_source.sql')).read_text()),
        migrations.RunSQL(
            (Path(__file__).parent /
             Path('create_protected_site_function_source.sql')).read_text()),
        migrations.RunSQL(
            (Path(__file__).parent /
             Path('create_management_restriction_or_regulation_zone_function_source.sql')
             ).read_text()),
    ]