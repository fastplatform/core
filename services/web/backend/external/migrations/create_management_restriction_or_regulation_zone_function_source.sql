CREATE OR REPLACE FUNCTION public.management_restriction_or_regulation_zone_function_source(
        z integer,
        x integer,
        y integer
    ) RETURNS bytea AS $$
DECLARE mvt bytea;
BEGIN
SET enable_seqscan TO off;
SELECT INTO mvt ST_AsMVT(
        tile,
        'management_restriction_or_regulation_zone',
        4096,
        'geom'
    )
FROM (
        SELECT ST_AsMVTGeom(
                ST_Transform(
                    ST_SimplifyPreserveTopology(m.geometry, 1 / (2 ^ z)),
                    3857
                ),
                TileBBox(z, x, y, 3857),
                4096,
                64,
                true
            ) AS geom,
            m.id,
            m.authority_id,
            m.name,
            STRING_AGG(DISTINCT ed.label, ', ') AS environmental_domains,
            STRING_AGG(DISTINCT ztc.label, ', ') AS zone_types
        FROM management_restriction_or_regulation_zone m
            INNER JOIN management_restriction_or_regulation_zone_version mv ON m.version_id = mv.id
            LEFT JOIN management_restriction_or_regulation_zone_environmental_domain med ON med.managementrestrictionorregulationzone_id = m.id
            LEFT JOIN environmental_domain ed ON ed.id = med.environmentaldomain_id
            LEFT JOIN management_restriction_or_regulation_zone_zone_type mzt ON mzt.managementrestrictionorregulationzone_id = m.id
            LEFT JOIN zone_type_code ztc ON ztc.id = mzt.zonetypecode_id
        WHERE mv.is_active
            AND ST_Intersects(
                m.geometry,
                ST_Transform(TileBBox(z, x, y, 3857), 4258)
            )
        GROUP BY m.id
    ) as tile
WHERE z >= 5
    AND geom IS NOT NULL;
SET enable_seqscan TO on;
RETURN mvt;
END $$ LANGUAGE plpgsql VOLATILE STRICT PARALLEL SAFE;