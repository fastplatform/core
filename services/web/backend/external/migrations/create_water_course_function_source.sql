CREATE OR REPLACE FUNCTION public.water_course_function_source(
        z integer,
        x integer,
        y integer
    ) RETURNS bytea AS $$
DECLARE mvt bytea;
BEGIN
SET enable_seqscan TO off;
SELECT INTO mvt ST_AsMVT(tile, 'water_course', 4096, 'geom')
FROM (
        SELECT ST_AsMVTGeom(
            ST_Transform(
                ST_SimplifyPreserveTopology(w.geometry, 1/(2^z)),
                3857
            ), TileBBox(z, x, y, 3857), 4096, 64, true) AS geom,
            w.id,
            w.authority_id,
	        w.geographical_name,
		    w.local_type,
            w.buffer
        FROM water_course w
        INNER JOIN water_course_version wv ON w.version_id = wv.id
        WHERE wv.is_active AND ST_Intersects(w.geometry, ST_Transform(TileBBox(z, x, y, 3857), 4258))
    ) as tile
WHERE z >= 5 AND geom IS NOT NULL;
SET enable_seqscan TO on;
RETURN mvt;
END $$ LANGUAGE plpgsql VOLATILE STRICT PARALLEL SAFE;