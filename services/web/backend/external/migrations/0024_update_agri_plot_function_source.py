from pathlib import Path

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [('external', '0023_update_agri_plot_function_source')]

    operations = [
        migrations.RunSQL((Path(__file__).parent / Path(
            'create_agri_plot_function_source.sql'
        )).read_text())
    ]