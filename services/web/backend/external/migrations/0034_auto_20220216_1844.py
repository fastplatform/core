# Generated by Django 3.2.7 on 2022-02-16 17:44

from django.db import migrations, models
import utils.models


class Migration(migrations.Migration):

    dependencies = [
        ('external', '0033_auto_20220216_1814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='designation',
            name='id',
            field=models.CharField(max_length=256, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='designationscheme',
            name='id',
            field=models.CharField(max_length=256, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='environmentaldomain',
            name='id',
            field=models.CharField(max_length=256, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='observableproperty',
            name='id',
            field=models.CharField(max_length=100, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='phenomenontype',
            name='id',
            field=models.CharField(max_length=256, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='soilinvestigationpurpose',
            name='id',
            field=models.CharField(max_length=256, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='specialisedzonetypecode',
            name='id',
            field=models.CharField(max_length=256, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='unitofmeasure',
            name='id',
            field=models.CharField(max_length=256, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
        migrations.AlterField(
            model_name='zonetypecode',
            name='id',
            field=models.CharField(max_length=256, primary_key=True, serialize=False, validators=[utils.models.validate_latin1], verbose_name='identifier'),
        ),
    ]
