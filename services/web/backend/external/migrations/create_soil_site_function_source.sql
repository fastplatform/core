CREATE OR REPLACE FUNCTION public.soil_site_function_source(
        z integer,
        x integer,
        y integer
    ) RETURNS bytea AS $$
DECLARE mvt bytea;
BEGIN
SET enable_seqscan TO off;
SELECT INTO mvt ST_AsMVT(tile, 'soil_site', 4096, 'geom')
FROM (
        SELECT ST_AsMVTGeom(
                ST_Transform(
                    ST_SimplifyPreserveTopology(s.geometry, 1 /(2 ^ z)),
                    3857
                ),
                TileBBox(z, x, y, 3857),
                4096,
                256,
                false
            ) AS geom,
            s.id,
            s.authority_id,
            sip.label AS soil_investigation_purpose,
            STRING_AGG(
                DISTINCT o.phenomenon_time || ': ' || op.label || '=' || (o.result->>'value') || ' ' || uom.name || ' (' || pt.label || ')',
                '\n'
            ) AS observations
        FROM soil_site s
            INNER JOIN soil_site_version sv ON s.version_id = sv.id
            LEFT JOIN soil_investigation_purpose sip ON sip.id = s.soil_investigation_purpose_id
            LEFT JOIN observation o ON s.id = o.soil_site_id
            LEFT JOIN observable_property op ON op.id = o.observed_property_id
            LEFT JOIN phenomenon_type pt ON pt.id = op.base_phenomenon_id
            LEFT JOIN unit_of_measure uom ON uom.id = op.uom_id
        WHERE sv.is_active
            AND ST_Intersects(
                s.geometry,
                ST_Transform(TileBBox(z, x, y, 3857), 4258)
            )
        GROUP BY s.id,
            sip.id
    ) as tile
WHERE z >= 5
    AND geom IS NOT NULL;
SET enable_seqscan TO on;
RETURN mvt;
END $$ LANGUAGE plpgsql VOLATILE STRICT PARALLEL SAFE;