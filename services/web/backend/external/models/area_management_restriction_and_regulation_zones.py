"""
Based on Inspire Annex III: Area Management Restriction, Regulation Zones and Reporting units
Should be used to map the Nitrate Vulnerable Zones

"""

from django.contrib.gis.db import models
from django.contrib.postgres.fields import DateRangeField
from django.utils.translation import gettext_lazy as _
from django.conf import settings

from common.models import AbstractCodeList
from utils.models import GetAdminURLMixin
from external.models import AbstractModelVersion


class ManagementRestrictionOrRegulationZoneVersion(AbstractModelVersion):
    """A version of the regulated zones data (NVZ)"""

    class Meta:
        db_table = "management_restriction_or_regulation_zone_version"
        verbose_name = _("management restriction or regulation zone version")
        verbose_name_plural = _("management restriction or regulation zone versions")
        help_text = _("Versions of management restriction or regulation zones objects")


class ManagementRestrictionOrRegulationZone(GetAdminURLMixin, models.Model):
    """A zone where a specific regulation or restriction applies (eg NVZ)

    Inspire - Annex III - Area Management Restriction, Regulation Zones and Reporting units
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    authority_id = models.CharField(
        null=False,
        blank=False,
        max_length=100,
        verbose_name=_("authority identifier"),
        help_text=_("Must be unique within each version"),
    )

    version = models.ForeignKey(
        to="external.ManagementRestrictionOrRegulationZoneVersion",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="items",
        verbose_name=_("version"),
    )

    geometry = models.MultiPolygonField(
        null=False, blank=False, srid=settings.DEFAULT_SRID, verbose_name=_("geometry")
    )

    zone_type = models.ManyToManyField(
        "external.ZoneTypeCode",
        blank=True,
        db_table="management_restriction_or_regulation_zone_zone_type",
        verbose_name=_("zone types"),
    )

    environmental_domain = models.ManyToManyField(
        "external.EnvironmentalDomain",
        blank=True,
        db_table="management_restriction_or_regulation_zone_environmental_domain",
        verbose_name=_("environmental domains"),
    )

    thematic_id = models.CharField(
        max_length=100, verbose_name=_("thematic identifier"), null=True, blank=True
    )

    name = models.CharField(
        max_length=100, verbose_name=_("name"), null=True, blank=True
    )

    specialized_zone_type = models.ForeignKey(
        to="external.SpecialisedZoneTypeCode",
        on_delete=models.PROTECT,
        verbose_name=_("specialized zone type"),
        null=True,
        blank=True,
    )

    designation_period = DateRangeField(
        verbose_name=_("designation period"), null=True, blank=True
    )

    begin_lifespan_version = models.DateTimeField(
        verbose_name=_("begin lifespan version"), null=True, blank=True
    )

    end_lifespan_version = models.DateTimeField(
        verbose_name=_("end lifespan version"), null=True, blank=True
    )

    source = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("source"),
        default=dict,
        help_text=_("The source data from which this object was derived."),
    )

    def __str__(self):
        return self.authority_id if self.name is None else self.name

    class Meta:
        db_table = "management_restriction_or_regulation_zone"
        verbose_name = _("management restriction or regulation zone")
        verbose_name_plural = _("management restriction or regulation zones")
        help_text = _(
            "Areas managed, restricted or regulated in accordance with a legal requirement related to an environmental policy or a policy or activity that may have an impact on the environment at any level of administration (international, European, national, regional and local)."
        )

        constraints = [
            models.UniqueConstraint(
                fields=["authority_id", "version_id"],
                name="management_restriction_or_regulation_zone_authority_id_version_id_unique",
            )
        ]


class ZoneTypeCode(AbstractCodeList):
    """A type of regulated zone

    http://inspire.ec.europa.eu/codelist/ZoneTypeCode
    """

    class Meta:
        db_table = "zone_type_code"
        verbose_name = _("zone type code")
        verbose_name_plural = _("zone type codes")


class SpecialisedZoneTypeCode(AbstractCodeList):
    """Additional classification value that defines the specialised type of zone

    https://inspire.ec.europa.eu/codelist/SpecialisedZoneTypeCode
    """

    class Meta:
        db_table = "specialized_zone_type_code"
        verbose_name = _("specialized zone type code")
        verbose_name_plural = _("specialized zone type codes")


class EnvironmentalDomain(AbstractCodeList):
    """Environmental domain, for which environmental objectives can be defined

    http://inspire.ec.europa.eu/codelist/EnvironmentalDomain
    """

    class Meta:
        db_table = "environmental_domain"
        verbose_name = _("environmental domain")
        verbose_name_plural = _("environmental domains")
