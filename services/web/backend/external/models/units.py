from django.utils.translation import gettext_lazy as _

from common.models import AbstractUnitOfMeasure


class UnitOfMeasure(AbstractUnitOfMeasure):
    class Meta:
        db_table = "unit_of_measure"
        verbose_name = _("unit of measure")
        verbose_name_plural = _("units of measure")
