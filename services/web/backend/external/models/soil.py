import numbers

from django.contrib.gis.db import models
from django.contrib.postgres.fields import ranges
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.core.exceptions import ValidationError

from common.models import AbstractCodeList
from utils.models import GetAdminURLMixin, TranslatableModel, validate_latin1
from external.models import AbstractModelVersion
from soil.models import ObservationValidateResultMixin


class SoilSiteVersion(AbstractModelVersion):
    class Meta:
        db_table = "soil_site_version"
        verbose_name = _("soil site version")
        verbose_name_plural = _("soil site versions")


class SoilSite(GetAdminURLMixin, models.Model):
    """Soil sites

    Origin: INSPIRE Annex III - Soil
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    authority_id = models.CharField(
        null=False,
        blank=False,
        max_length=100,
        verbose_name=_("authority identifier"),
        help_text=_("Must be unique within each version"),
    )

    version = models.ForeignKey(
        to="external.SoilSiteVersion",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        related_name="items",
        verbose_name=_("version"),
    )

    geometry = models.GeometryField(
        null=False, blank=False, srid=settings.DEFAULT_SRID, verbose_name=_("geometry")
    )

    soil_investigation_purpose = models.ForeignKey(
        to="external.SoilInvestigationPurpose",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        related_name="soil_sites",
        verbose_name=_("soil investigation purpose"),
    )

    valid_from = models.DateTimeField(
        null=True, blank=True, verbose_name=_("start date/time of validity")
    )

    valid_to = models.DateTimeField(
        null=True, blank=True, verbose_name=_("end date/time of validity")
    )

    begin_lifespan_version = models.DateTimeField(
        null=True, blank=True, verbose_name=_("begin date/time of version")
    )

    end_lifespan_version = models.DateTimeField(
        null=True, blank=True, verbose_name=_("end date/time of version")
    )

    source = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("source"),
        default=dict,
        help_text=_("The source data from which this object was derived."),
    )

    def __str__(self):
        return self.authority_id

    class Meta:
        verbose_name = _("soil site")
        verbose_name_plural = _("soil sites")
        db_table = "soil_site"
        help_text = _(
            "An area within a larger survey, study or monitored area, where a specific soil investigation is carried out."
        )

        constraints = [
            models.UniqueConstraint(
                fields=["authority_id", "version_id"],
                name="soil_site_authority_id_version_id_unique",
            )
        ]


def default_value_none():
    return {"value": None}


class Observation(ObservationValidateResultMixin, GetAdminURLMixin, models.Model):
    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    soil_site = models.ForeignKey(
        to="external.SoilSite",
        on_delete=models.CASCADE,
        null=False,
        blank=False,
        verbose_name=_("soil site"),
        related_name="observations",
    )

    observed_property = models.ForeignKey(
        to="external.ObservableProperty",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        verbose_name=_("observed property"),
        related_name="observations",
    )
    # NOTE OGC SWE Common provides a model suitable for describing many kinds
    #  of observation results.
    # The type of the observation result shall be consistent with the observed
    # property, and the scale or scope for the value shall be consistent with
    # the quantity or category type. If the observed property (6.2.2.8) is a
    # spatial operation or function, the type of the result may be a coverage.
    # https://www.opengeospatial.org/standards/swecommon
    result = models.JSONField(
        null=False,
        blank=False,
        verbose_name=_("result"),
        default=default_value_none,
        help_text=_(
            'Must be an JSON object with a "value" property, e.g. {"value": 123}'
        ),
    )

    phenomenon_time = models.DateTimeField(
        null=False,
        blank=False,
        verbose_name=_("phenomenon date/time"),
    )

    result_time = models.DateTimeField(
        null=True, blank=True, verbose_name=_("result date/time")
    )

    valid_time = ranges.DateTimeRangeField(
        null=True, blank=True, verbose_name=_("validity date/time")
    )

    source = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("source"),
        default=dict,
        help_text=_("The source data from which this object was derived."),
    )

    def __str__(self):
        return f'{self.phenomenon_time} {self.observed_property}={self.result.get("value", "-")}'

    class Meta:
        db_table = "observation"
        verbose_name = _("observation")
        verbose_name_plural = _("observations")
        help_text = _(
            "An act that results in the estimation of the value of a soil feature property"
        )

        constraints = (
            # There can be only one observation per property in a soil site at the same time
            # (otherwise we cannot know which one is the right one)
            models.UniqueConstraint(
                fields=["soil_site", "observed_property", "phenomenon_time"],
                name="external_observation_soil_site_observed_property_phenomenon_time_unique",
            ),
        )


class ObservableProperty(GetAdminURLMixin, TranslatableModel, models.Model):
    """Observable property

    Origin: INSPIRE Annex III - Soil
    """

    id = models.CharField(
        primary_key=True, max_length=100, verbose_name=_("identifier"),
        validators=[validate_latin1],
    )

    label = models.CharField(
        max_length=100, null=False, blank=False, verbose_name=_("label")
    )

    base_phenomenon = models.ForeignKey(
        to="external.PhenomenonType",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="observable_properties",
        verbose_name=_("base phenomenon"),
    )

    uom = models.ForeignKey(
        to="external.UnitOfMeasure",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        verbose_name=_("unit of measuree"),
    )

    validation_rules = models.JSONField(
        verbose_name=_("validation rules"), null=True, blank=True
    )

    def __str__(self):
        if self.uom and self.uom.symbol is not None and self.uom.symbol != "-":
            return f"{self.label} ({self.uom.symbol})"
        else:
            return self.label

    def clean(self, *args, **kwargs) -> None:
        super().clean(*args, **kwargs)

        if self.validation_rules:
            value_type = self.validation_rules.get("type")
            if value_type == "numeric":
                value_min = self.validation_rules.get("min")
                value_max = self.validation_rules.get("max")
                if value_min is not None and not isinstance(value_min, numbers.Number):
                    raise ValidationError(_("Min must be a number"))
                if value_max is not None and not isinstance(value_max, numbers.Number):
                    raise ValidationError(_("Max must be a number"))
                
                extra_keys = [k for k in self.validation_rules.keys() if k not in ["type", "min", "max"]]
                if extra_keys:
                    raise ValidationError(
                        _("Unknown keys in validation rules: {}. Only 'min' and 'max' keys are supported for type 'numeric'.").format(extra_keys)
                    )
            elif value_type == "category":
                value_choices = self.validation_rules.get("choices")
                if value_choices is not None:
                    if not isinstance(value_choices, list):
                        raise ValidationError(
                            _("Choices must be a list of dictionaries")
                        )
                    for choice in value_choices:
                        if not isinstance(choice, dict):
                            raise ValidationError(
                                _("Choices must be a list of dictionaries")
                            )
                        if "value" not in choice:
                            raise ValidationError(
                                _("Each choice must have a 'value' field")
                            )
                        if "label" not in choice or not isinstance(
                            choice["label"], str
                        ):
                            raise ValidationError(
                                _("Each choice must have a text 'label' field")
                            )
                        
                        extra_keys = [k for k in choice.keys() if k not in ["value", "label"]]
                        if extra_keys:
                            raise ValidationError(
                                _("Unknown keys in choice: {}. Only 'value' and 'label' keys are supported for each choice.").format(extra_keys)
                            )

                extra_keys = [k for k in self.validation_rules.keys() if k not in ["type", "choices"]]
                if extra_keys:
                    raise ValidationError(
                        _("Unknown keys in validation rules: {}. Only 'choices' key is supported for type 'category'.").format(extra_keys)
                    )
                
            elif value_type == "text":
                extra_keys = [k for k in self.validation_rules.keys() if k not in ['type']]
                if extra_keys:
                    raise ValidationError(
                        _("Unknown keys in validation rules: {}. No extra keys are supported for type 'text'.").format(extra_keys)
                    )
            elif value_type == "boolean":
                extra_keys = [k for k in self.validation_rules.keys() if k not in ['type']]
                if extra_keys:
                    raise ValidationError(
                        _("Unknown keys in validation rules: {}. No extra keys are supported for type 'boolean'.").format(extra_keys)
                    )
            else:
                raise ValidationError(_("Unknown type: '{}'. Available types are: {}").format(value_type, ["numeric", "category", "text", "boolean"]))

    class Meta:
        db_table = "observable_property"
        verbose_name = _("observable property")
        verbose_name_plural = _("observable properties")
        help_text = _("A single observable property e.g. temperature.")


class PhenomenonType(AbstractCodeList):
    """Phenomenon type

    Origin: INSPIRE Annex III - Soil

    http://inspire.ec.europa.eu/codelist/SoilSiteParameterNameValue
    """

    class Meta:
        db_table = "phenomenon_type"
        verbose_name = _("phenomenon type")
        verbose_name_plural = _("phenomenon types")
        help_text = _("A code list of phenomena (e.g. temperature, wind speed)")


class SoilInvestigationPurpose(AbstractCodeList):
    """Soil investigation purpose

    Origin: INSPIRE Annex III - Soil

    https://inspire.ec.europa.eu/codelist/SoilInvestigationPurposeValue
    """

    #
    class Meta:
        db_table = "soil_investigation_purpose"
        verbose_name = _("soil investigation purpose")
        verbose_name_plural = _("soil investigation purposes")
        help_text = _("A code list indicating the reasons for conducting a soil survey")
