"""
Based on Inspire Annex I: Protected sites
Should be used to map the Natura2000 areas
https://ec.europa.eu/environment/nature/natura2000/db_gis/index_en.htm#sites
"""
from django.contrib.gis.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import gettext_lazy as _
from django.conf import settings

from common.models import AbstractCodeList
from utils.models import GetAdminURLMixin
from external.models import AbstractModelVersion


class ProtectionClassification(models.TextChoices):
    """A protected site classification based on the purpose of protection.

    Inspire - Annex I - Protected Sites
    """

    nature_conservation = "nature_conservation", _("nature conservation")
    archaeological = "archaeological", _("archaeological")
    cultural = "cultural", _("cultural")
    ecological = "ecological", _("ecological")
    landscape = "landscape", _("landscape")
    environment = "environment", _("environment")
    geological = "geological", _("geological")


class ProtectedSiteVersion(AbstractModelVersion):
    """A version of the protected sites data"""

    class Meta:
        db_table = "protected_site_version"
        verbose_name = _("protected site version")
        verbose_name_plural = _("protected site versions")


class ProtectedSite(GetAdminURLMixin, models.Model):
    """A protected site (eg Natura 2000)

    Inspire - Annex I - Protected Site

    An area designated or managed within a framework of international,
    Community and Member States' legislation to achieve specific conservation
    objectives.
    """

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    authority_id = models.CharField(
        null=False,
        blank=False,
        max_length=100,
        verbose_name=_("authority identifier"),
        help_text=_("Must be unique within each version"),
    )

    version = models.ForeignKey(
        to="external.ProtectedSiteVersion",
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="items",
        verbose_name=_("version"),
    )

    geometry = models.GeometryField(
        null=False, blank=False, srid=settings.DEFAULT_SRID, verbose_name=_("geometry")
    )

    legal_foundation_date = models.DateTimeField(
        verbose_name=_("legal foundation date/time"),
        help_text=_(
            """The date that the protected site was legally created. This is the date that the real world object was created, not the date that its representation in an information system was created."""
        ),
        null=True,
        blank=True,
    )

    legal_foundation_document = models.CharField(
        verbose_name=_("legal foundation document"),
        help_text=_(
            """A URL or text citation referencing the legal act that created the Protected Site."""
        ),
        null=True,
        blank=True,
        max_length=512,
    )

    site_designation = models.ManyToManyField(
        "external.DesignationType",
        blank=True,
        verbose_name=_("site designations"),
        db_table="protected_site_site_designation",
    )

    site_name = models.CharField(
        null=True, blank=True, max_length=256, verbose_name=_("site name")
    )

    site_protection_classification = models.CharField(
        choices=ProtectionClassification.choices,
        verbose_name=_("site protection classification"),
        null=True,
        blank=True,
        max_length=256,
    )

    source = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("source"),
        default=dict,
        help_text=_("The source data from which this object was derived."),
    )

    def __str__(self):
        return self.authority_id if self.site_name is None else self.site_name

    class Meta:
        db_table = "protected_site"
        verbose_name = _("protected site")
        verbose_name_plural = _("protected sites")
        help_text = _(
            "An area designated or managed within a framework of international, Community and Member States' legislation to achieve specific conservation objectives."
        )

        constraints = [
            models.UniqueConstraint(
                fields=["authority_id", "version_id"],
                name="protected_site_authority_id_version_id_unique",
            )
        ]


class DesignationType(GetAdminURLMixin, models.Model):
    """A data type designed to contain a designation for the Protected Site,
    including the designation scheme used and the value within that scheme."""

    id = models.AutoField(primary_key=True, verbose_name=_("identifier"))

    designation_scheme = models.ForeignKey(
        to="external.DesignationScheme",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        verbose_name=_("designation scheme"),
        help_text=_("""The scheme from which the designation code comes."""),
    )

    designation = models.ForeignKey(
        to="external.Designation",
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        verbose_name=_("designation"),
    )

    percentage_under_designation = models.FloatField(
        null=False,
        blank=False,
        default=1.0,
        validators=[MinValueValidator(0.0), MaxValueValidator(1.0)],
        verbose_name=_("percentage under designation"),
        help_text=_(
            """The percentage of the site that falls under the designation. This is used in particular for the IUCN categorisation. If a value is not provided for this attribute, it is assumed to be 100%."""
        ),
    )

    source = models.JSONField(
        null=True,
        blank=True,
        verbose_name=_("source"),
        default=dict,
        help_text=_("The source data from which this object was derived."),
    )

    def __str__(self):
        return f"{self.designation_scheme.label} - {self.designation.label}"

    class Meta:
        verbose_name = _("designation type")
        verbose_name_plural = _("designation types")
        db_table = "designation_type"


class DesignationScheme(AbstractCodeList):
    """A scheme used to assign a designation to the Protected Sites.

    Inspire - Annex I - Protected Sites
    """

    class Meta:
        verbose_name = _("designation scheme")
        verbose_name_plural = _("designation schemes")
        db_table = "designation_scheme"


class Designation(AbstractCodeList):
    """An abstract base type for code lists containing the
    classification and desigation types under different schemes.

    Inspire - Annex I - Protected Sites
    """

    class Meta:
        verbose_name = _("designation")
        verbose_name_plural = _("designations")
        db_table = "designation"
