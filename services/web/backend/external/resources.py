from common.resources import ModelResourceWithColumnIds

from external.models import (
    UnitOfMeasure,
    ObservableProperty,
    SoilInvestigationPurpose,
    PhenomenonType,
    ZoneTypeCode,
    SpecialisedZoneTypeCode,
    EnvironmentalDomain,
    DesignationScheme,
    Designation,
    DesignationType,
    AgriPlotVersion,
    AgriPlot,
    ManagementRestrictionOrRegulationZoneVersion,
    ManagementRestrictionOrRegulationZone,
    WaterCourse,
    SurfaceWater,
    WaterCourseVersion,
    SurfaceWaterVersion,
    ProtectedSiteVersion,
    ProtectedSite,
    Observation,
    SoilSite,
    SoilSiteVersion,
)


class AgriPlotVersionResource(ModelResourceWithColumnIds):
    class Meta:
        model = AgriPlotVersion


class AgriPlotResource(ModelResourceWithColumnIds):
    class Meta:
        model = AgriPlot


class DesignationTypeResource(ModelResourceWithColumnIds):
    class Meta:
        model = DesignationType


class DesignationSchemeResource(ModelResourceWithColumnIds):
    class Meta:
        model = DesignationScheme


class DesignationResource(ModelResourceWithColumnIds):
    class Meta:
        model = Designation


class SpecialisedZoneTypeCodeResource(ModelResourceWithColumnIds):
    class Meta:
        model = SpecialisedZoneTypeCode


class ZoneTypeCodeResource(ModelResourceWithColumnIds):
    class Meta:
        model = ZoneTypeCode


class EnvironmentalDomainResource(ModelResourceWithColumnIds):
    class Meta:
        model = EnvironmentalDomain


class SoilInvestigationPurposeResource(ModelResourceWithColumnIds):
    class Meta:
        model = SoilInvestigationPurpose


class PhenomenonTypeResource(ModelResourceWithColumnIds):
    class Meta:
        model = PhenomenonType


class UnitOfMeasureResource(ModelResourceWithColumnIds):
    class Meta:
        model = UnitOfMeasure


class ObservablePropertyResource(ModelResourceWithColumnIds):
    class Meta:
        model = ObservableProperty


class ManagementRestrictionOrRegulationZoneVersionResource(ModelResourceWithColumnIds):
    class Meta:
        model = ManagementRestrictionOrRegulationZoneVersion


class ManagementRestrictionOrRegulationZoneResource(ModelResourceWithColumnIds):
    class Meta:
        model = ManagementRestrictionOrRegulationZone


class WaterCourseResource(ModelResourceWithColumnIds):
    class Meta:
        model = WaterCourse


class SurfaceWaterResource(ModelResourceWithColumnIds):
    class Meta:
        model = SurfaceWater


class WaterCourseVersionResource(ModelResourceWithColumnIds):
    class Meta:
        model = WaterCourseVersion


class SurfaceWaterVersionResource(ModelResourceWithColumnIds):
    class Meta:
        model = SurfaceWaterVersion


class ProtectedSiteVersionResource(ModelResourceWithColumnIds):
    class Meta:
        model = ProtectedSiteVersion


class ProtectedSiteResource(ModelResourceWithColumnIds):
    class Meta:
        model = ProtectedSite


class ObservationResource(ModelResourceWithColumnIds):
    class Meta:
        model = Observation


class SoilSiteResource(ModelResourceWithColumnIds):
    class Meta:
        model = SoilSite


class SoilSiteVersionResource(ModelResourceWithColumnIds):
    class Meta:
        model = SoilSiteVersion
