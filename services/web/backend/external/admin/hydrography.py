from django.utils.translation import gettext_lazy as _
from django.conf import settings

from django_object_actions import DjangoObjectActions

from fastplatform.site import admin_site

from external.models import (
    WaterCourseVersion,
    WaterCourse,
    SurfaceWaterVersion,
    SurfaceWater,
)
from external.resources import (
    WaterCourseResource,
    SurfaceWaterResource,
    WaterCourseVersionResource,
    SurfaceWaterVersionResource,
)

from external.admin.version import VersionAdmin, VersionActionsMixin
from utils.admin import CommonModelAdmin


class WaterAdmin(VersionActionsMixin, DjangoObjectActions, CommonModelAdmin):
    hide_from_dashboard = False

    list_filter = ("version__is_active",)
    list_display_links = (
        "id",
        "geographical_name",
    )
    list_display = ("id", "version", "authority_id", "geographical_name", "local_type")
    list_select_related = ("version",)

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "authority_id",
                    "hydro_id",
                    "version",
                    "local_type",
                    "geographical_name",
                )
            },
        ),
        (
            _("Lifespan"),
            {"fields": (("begin_lifespan_version", "end_lifespan_version"),)},
        ),
        (_("Geographic information"), {"fields": ("geometry",)}),
    )

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["version"]
        return []

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('version')

    list_display = ("id", "version", "geographical_name", "local_type")

    # Map parameters
    # --------------
    geometry_fields = ["geometry"]


class WaterCourseAdmin(WaterAdmin):
    model = WaterCourse
    version_model = WaterCourseVersion
    resource_class = WaterCourseResource


class SurfaceWaterAdmin(WaterAdmin):
    model = SurfaceWater
    version_model = SurfaceWaterVersion
    resource_class = SurfaceWaterResource


class WaterCourseVersionAdmin(VersionAdmin):
    hide_from_dashboard = True
    model = WaterCourseVersion
    resource_class = WaterCourseVersionResource


class SurfaceWaterVersionAdmin(VersionAdmin):
    hide_from_dashboard = True
    model = SurfaceWaterVersion
    resource_class = SurfaceWaterVersionResource


if settings.ENABLE_EXTERNAL_WATER_COURSE:
    admin_site.register(WaterCourseVersion, WaterCourseVersionAdmin)
    admin_site.register(WaterCourse, WaterCourseAdmin)

if settings.ENABLE_EXTERNAL_SURFACE_WATER:
    admin_site.register(SurfaceWaterVersion, SurfaceWaterVersionAdmin)
    admin_site.register(SurfaceWater, SurfaceWaterAdmin)
