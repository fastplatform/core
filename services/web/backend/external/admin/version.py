from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.db import models
from django import forms

from import_export.admin import ExportMixin

from utils.admin import CommonModelAdmin


def make_active(modeladmin, request, queryset):
    if len(queryset) > 1:
        messages.error(request, _("Please select a unique version to activate"))
    else:
        if queryset[0].status != "COMPLETED":
            messages.warning(
                request,
                _(
                    "You have activated a version with STATUS != 'COMPLETED'. Version objects might be incomplete."
                ),
            )

        queryset.model.objects.filter(is_active=True).update(is_active=False)
        queryset.update(is_active=True)


make_active.short_description = _("Activate selected version")


class VersionAdmin(ExportMixin, CommonModelAdmin):

    list_display = (
        "_version",
        "is_active",
        "status",
        "_count",
        "import_started_at",
        "import_completed_at",
    )
    list_filter = ("is_active", "status")
    actions = [make_active]

    formfield_overrides = {
        models.TextField: dict(widget=forms.Textarea(attrs=dict(readonly=True)))
    }

    def get_fieldsets(self, request, obj):
        if obj:
            if obj.import_started_at:
                return (
                    (
                        None,
                        {"fields": ("_version", "is_active", "_count")},
                    ),
                    (
                        _("Import log"),
                        {
                            "fields": (
                                "status",
                                ("import_started_at", "import_completed_at"),
                                "import_log",
                            )
                        },
                    ),
                )
            else:
                return (
                    (
                        None,
                        {"fields": ("_version", "is_active", "_count")},
                    ),
                )
        else:
            return (
                (
                    None,
                    {"fields": ("id",)},
                ),
            )

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = (
            "id",
            "_count",
            "_version",
            "status",
            "import_started_at",
            "import_completed_at",
        )
        return readonly_fields

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def _version(self, obj):
        if obj:
            return f"Version {obj.id}"

    _version.short_description = _("Version")

    def _count(self, obj):
        return obj.count

    _count.short_description = _("Number of objects")

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.annotate(count=models.Count("items"))
        return queryset

    def make_active(self, request, obj):
        return make_active(self, request, self.model.objects.filter(pk__in=[obj.pk]))

    def save_model(self, request, obj, form, change):
        if obj.pk:
            if obj.is_active:
                if obj.status == "COMPLETED":
                    self.model.objects.update(is_active=False)
                    obj.is_active = True
                else:
                    messages.warning(
                        request,
                        _(
                            "You have activated a version with STATUS != 'COMPLETED'. Version objects might be incomplete."
                        ),
                    )
        else:
            obj.is_active = False

        if (
            not self.model.objects.exclude(pk=obj.pk).filter(is_active=True).exists()
            and not obj.is_active
        ):
            messages.warning(
                request,
                _("No active version. No object will be displayed on the maps."),
            )

        super().save_model(request, obj, form, change)


class VersionActionsMixin:
    """A mixin to insert a "View all versions" button in the toolbar
    for model admins that support it

    Expects that the version_model attribute is set on the model admin.
    """

    def view_all_versions_action(self, *args, **kwargs):
        from django.http import HttpResponseRedirect
        from django.urls import reverse

        url = reverse(
            "admin:%s_%s_changelist"
            % (self.version_model._meta.app_label, self.version_model._meta.model_name)
        )
        return HttpResponseRedirect(url)

    view_all_versions_action.label = _("View all versions")
    changelist_actions = ("view_all_versions_action",)
