from external.admin.agricultural_and_aquaculture_facilities import *
from external.admin.area_management_restriction_and_regulation_zones import *
from external.admin.hydrography import *
from external.admin.protected_sites import *
from external.admin.soil import *
