# `core/web/pdf_generator`: PDF generation service

The [core/web/pdf_generator](./) is a [FastAPI](https://fastapi.tiangolo.com/) service whose role to generate a PDF document from a HTML template and optionnaly email it.

It is used by the fertilization services to export fertilization plans as PDF documents.

## Architecture

The service is a [FastAPI](https://fastapi.tiangolo.com/) application served behind a [uvicorn](https://www.uvicorn.org/) HTTP server.

To generate the PDF document, it takes the HTML template and data parameters provided as inputs and generates and render the HTML page as PDF using Chromium from the [pyppeteer library](https://pyppeteer.github.io/pyppeteer/). It also adds a header to the document with a logo and a title.

```plantuml
actor "User" as user
component "Farmer mobile application" as app
component "Fertilization service" as fertilization_service
component "PDF generator" as pdf_generator

user --- app
app --> fertilization_service : **[1]** Request fertilization plan PDF export
fertilization_service -> pdf_generator : **[2]** Forward request with HTML template
```

## API

### Endpoints

It exposes only one endpoint (`/generate_pdf`) but has 2 modes depending on the inputs:
- the default mode will return a PDF document from the HTML template
- the alternate mode, if the `send_by_email` parameter is set, will send the PDF document to the specified email addresse(s) (in that case it returns and empty valid response)

### Environment variables

- `TIME_ZONE`: locale timezone according to the region
- `EUROPEAN_COMMISSION_NAME`: Name of the European Commission in the region
- `EUROPEAN_COMMISSION_LOGO_SVG_BASE64`: Logo of the European Commission as a SVG base64 encoded
- `REGION_NAME`: Name of the region
- `REGION_LOGO_SVG_BASE64`: Logo of the region as a SVG base64 encoded
- `SMTP_HOST`: SMPT host for the PDF emailing
- `SMTP_PORT`: SMPT port for the PDF emailing
- `SMTP_LOGIN`: SMPT login for the PDF emailing
- `SMTP_PASSWORD`: SMPT password for the PDF emailing
- `SMTP_SENDER`: SMPT sender name for the PDF emailing
- `PYPPETEER_CHROMIUM_REVISION`: the Chromium version for the Pyppeteer library 


## Development

### Prerequisites

- Python 3.7+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)

### Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```
make start-tracing
```
Jaeger UI will be available at [http://localhost:16686]()

Start the service:
```
make start
```

The API server is now started and available at http://localhost:7010.

**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run 
```