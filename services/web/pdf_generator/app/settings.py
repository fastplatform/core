import os
import sys

from pathlib import Path, PurePath

from pydantic import BaseSettings

from typing import Dict, Union


class Settings(BaseSettings):

    API_DIR: Path = Path(__file__)

    OPENTELEMETRY_EXPORTER_ZIPKIN_ENDPOINT: str = "http://127.0.0.1:9411/api/v2/spans"
    OPENTELEMETRY_SAMPLING_RATIO: float = 0.1
    OPENTELEMETRY_SERVICE_NAME: str = "web-pdf-generator"
    
    LOG_COLORS = bool(os.getenv("LOG_COLORS", sys.stdout.isatty()))
    LOG_FORMAT = str(os.getenv("LOG_FORMAT", "uvicorn"))
    LOG_LEVEL = str(os.getenv("LOG_LEVEL", "INFO")).upper()
    LOGGING_CONFIG: Dict[str, Union[Dict, bool, int, str]] = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "class": "logging.Formatter",
                "format": "%(levelname)-10s %(message)s",
            },
            "uvicorn": {
                "()": "uvicorn.logging.DefaultFormatter",
                "format": "%(levelprefix)s %(message)s",
                "use_colors": LOG_COLORS,
            },
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": LOG_FORMAT,
                "level": LOG_LEVEL,
                "stream": "ext://sys.stdout",
            }
        },
        "root": {"handlers": ["default"], "level": LOG_LEVEL},
        "loggers": {
            "client": {"propagate": False,  "level": "INFO"},
            "fastapi": {"propagate": True},
            "pyppeteer": {"propagate": False, "level": "WARNING"},
            "uvicorn": {"propagate": True},
            "uvicorn.access": {"propagate": True},
            "uvicorn.asgi": {"propagate": True},
            "uvicorn.error": {"propagate": True},
        },
    }
    TIME_ZONE = str(os.getenv("TIME_ZONE", "UTC"))
    EUROPEAN_COMMISSION_NAME = str(os.getenv("EUROPEAN_COMMISSION_NAME"))
    EUROPEAN_COMMISSION_LOGO_SVG_BASE64 = str(os.getenv("EUROPEAN_COMMISSION_LOGO_SVG_BASE64"))
    REGION_NAME = str(os.getenv("REGION_NAME"))
    REGION_LOGO_SVG_BASE64 = str(os.getenv("REGION_LOGO_SVG_BASE64"))

    REMOTE_DEBUG: bool = False

    CHROMIUM_EXECUTABLE_PATH: str = os.getenv("CHROMIUM_EXECUTABLE_PATH", None)


config = Settings()