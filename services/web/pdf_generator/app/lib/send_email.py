import os
import aiosmtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.header import Header
from email.utils import formataddr, formatdate, make_msgid
from email.encoders import encode_base64
import logging

logger = logging.getLogger(__name__)

SMTP_HOST = os.environ.get("SMTP_HOST")
SMTP_PORT = os.environ.get("SMTP_PORT")
SMTP_SENDER = os.environ.get("SMTP_SENDER")
SMTP_LOGIN = os.environ.get("SMTP_LOGIN")
SMTP_PASSWORD = os.environ.get("SMTP_PASSWORD")

SENDER_NAME = "FaST"


async def send_email(to_addrs, subject, text_content, pdf_file, pdf_filename):
    try:
        if to_addrs and len(to_addrs) > 0:
            await _send_email(to_addrs, subject, text_content, pdf_file, pdf_filename)
    except:
        logger.exception(f"Failed to send email {subject} to {','.join(to_addrs)}")


async def _send_email(to_addrs, subject, text_content, pdf_file, pdf_filename):
    smtp = aiosmtplib.SMTP(hostname=SMTP_HOST, port=SMTP_PORT, use_tls=True)
    await smtp.connect()
    await smtp.auth_login(SMTP_LOGIN, SMTP_PASSWORD)
    message = _build_message(
        SMTP_SENDER, to_addrs, subject, text_content, pdf_file, pdf_filename
    )
    await smtp.sendmail(SMTP_SENDER, to_addrs, message.as_string())
    await smtp.quit()
    logger.info(f"Sent email {subject} to {','.join(to_addrs)}")


def _build_message(from_addr, to_addrs, subject, text_content, pdf_file, pdf_filename):
    message = MIMEMultipart("alternative")
    message["Subject"] = subject
    message["From"] = formataddr((str(Header(SENDER_NAME, "utf-8")), from_addr))
    message["To"] = ",".join(to_addrs)
    message["Date"] = formatdate(localtime=True)
    message["Message-ID"] = make_msgid(domain="fastplatform.com")

    message.attach(MIMEText(text_content, "plain"))

    attachedfile = MIMEApplication(pdf_file, _subtype="pdf", _encoder=encode_base64)
    attachedfile.add_header("content-disposition", "attachment", filename=pdf_filename)
    message.attach(attachedfile)

    return message
