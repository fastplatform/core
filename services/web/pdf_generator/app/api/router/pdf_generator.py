import logging
import pytz
import asyncio

from datetime import datetime as dt
from fastapi import APIRouter, Response
from opentelemetry import trace
from pydantic import BaseModel

from app.lib.pdf_generator import generate_pdf
from app.lib.template import (
    get_fast_logo,
    get_logo_base64,
    generate_plot_map_images,
    jinja_env,
)
from app.settings import config
from app.lib.send_email import send_email

# Log
logger = logging.getLogger(__name__)

# FastAPI
router = APIRouter()


def tracer():
    return trace.get_tracer(__name__)


class Input(BaseModel):
    template: str
    data: dict


@router.post("/generate_pdf")
async def request_generate_pdf(input: Input):
    base_template = jinja_env.from_string(input.template)
    input.data["fast_logo"] = get_fast_logo()
    input.data["european_commission_logo"] = get_logo_base64(
        "{}{}".format(
            "data:image/svg+xml;base64,", config.EUROPEAN_COMMISSION_LOGO_SVG_BASE64
        )
    )
    input.data["european_commission_name"] = config.EUROPEAN_COMMISSION_NAME
    input.data["region_logo"] = get_logo_base64(
        "{}{}".format("data:image/svg+xml;base64,", config.REGION_LOGO_SVG_BASE64)
    )
    input.data["region_name"] = config.REGION_NAME
    input.data["current_date_time"] = dt.now(tz=pytz.UTC).strftime(
        "%Y-%m-%dT%H:%M:%S.%f%z"
    )
    for fertilization_plan in input.data["fertilization_plans"]:
        with tracer().start_as_current_span("generate_plot_map_images") as span:
            generate_plot_map_images(fertilization_plan)
            span.add_event(
                "Generate a graphical plot representation for the fertilization plan: {}.".format(
                    fertilization_plan["fertilization_plan_name"]
                )
            )
    if "algorithm_logo_base64" in input.data:
        input.data["algorithm_logo"] = get_logo_base64(
            input.data["algorithm_logo_base64"]
        )

    with tracer().start_as_current_span("html_template_rendering"):
        content = base_template.render(input.data)

    if "send_by_email" in input.data and bool(input.data["send_by_email"]) is True:
        with tracer().start_as_current_span("send_pdf"):
            try:
                to_addrs = [input.data["send_by_email_address"]]
                subject = input.data["send_by_email_subject"]
                text_content = input.data["send_by_email_body"]
                pdf_filename = input.data["send_by_email_filename"]

                async def send_pdf_task():
                    pdf = await generate_pdf(content=content, data=input.data)
                    await send_email(to_addrs, subject, text_content, pdf, pdf_filename)

                asyncio.create_task(send_pdf_task())
                return Response(content="", media_type="application/pdf")

            except Exception as e:
                logger.error("Failed to send pdf")
                logger.exception(str(e))
                return Response(status_code=500)

    else:
        with tracer().start_as_current_span("generate_pdf"):
            try:
                pdf = await generate_pdf(content=content, data=input.data)

                return Response(content=pdf, media_type="application/pdf")
            except Exception as e:
                logger.error("Failed to generate pdf")
                logger.exception(str(e))
                return Response(status_code=500)
