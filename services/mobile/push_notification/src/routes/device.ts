import { Router } from 'express';
import {
  deleteUser,
  deleteUserDevice,
  getUserDevices,
  registerUserDevice,
} from '../controllers/user-device-controller'

const router = Router();

router.route('/users/:user_id')
  .get(getUserDevices)
  .delete(deleteUser)


router.route('/users/:user_id/registered-devices')
  .get(getUserDevices)
  .post(registerUserDevice)


router.route('/users/:user_id/registered-devices/:device_token')
  .delete(deleteUserDevice)

export default router
