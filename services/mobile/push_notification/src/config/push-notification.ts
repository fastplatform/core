import PushNotifications from 'node-pushnotifications';
import fs from 'fs';
import { 
    NODE_ENV,
    APNS_ENCRYPTION_KEY_PATH,
    APNS_ENCRYPTION_KEY_VALUE,
    APNS_KEY_ID,
    APNS_TEAM_ID,
    FCM_API_KEY_PATH,
    FCM_API_KEY_VALUE
} from "./env"

const settings: PushNotifications.Settings = {
    gcm: {
        id: FCM_API_KEY_VALUE ? FCM_API_KEY_VALUE: fs.readFileSync(FCM_API_KEY_PATH).toString()
    },
    apn: {
        token: {
            key: APNS_ENCRYPTION_KEY_VALUE.toString() ? APNS_ENCRYPTION_KEY_VALUE: APNS_ENCRYPTION_KEY_PATH,
            keyId: APNS_KEY_ID,
            teamId: APNS_TEAM_ID
        },
        // cert: resolve(pathToSecrets, 'apns/certs/aps_development.cer'),
        production: NODE_ENV !== 'development' // true for APN production environment, false for APN sandbox environment,
    },
    isAlwaysUseFCM: false, // true all messages will be sent through node-gcm (which actually uses FCM)
};
const pushNotifications = new PushNotifications(settings);

export {
    pushNotifications
}
