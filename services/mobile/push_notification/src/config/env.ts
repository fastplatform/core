// Node
export const NODE_ENV: string = process.env.NODE_ENV || 'production'

// Server
export const API_VERSION: string | undefined = process.env.API_VERSION || 'v1'
export const SERVER_PORT: number = parseInt(process.env.PORT || '5000')
// if true => Graphql server, if false => REST server
export const IS_GRAPHQL_SERVER_ELSE_REST = process.env.IS_GRAPHQL_SERVER_ELSE_REST ? process.env.IS_GRAPHQL_SERVER_ELSE_REST.toLowerCase() === 'true' : true

// Secrets
export const APNS_ENCRYPTION_KEY_PATH: string = process.env.APNS_ENCRYPTION_KEY_PATH || ''
export const APNS_ENCRYPTION_KEY_VALUE: Buffer = Buffer.from(process.env.APNS_ENCRYPTION_KEY_VALUE || '')
export const FCM_API_KEY_PATH: string = process.env.FCM_API_KEY_PATH || ''
export const FCM_API_KEY_VALUE: string = process.env.FCM_API_KEY_VALUE || ''

// APNs config
export const APNS_KEY_ID: string = process.env.APNS_KEY_ID || ''
export const APNS_TEAM_ID: string = process.env.APNS_TEAM_ID || ''

// Notification
export const APP_BUNDLE_ID_IOS: string = process.env.APP_BUNDLE_ID_IOS || 'eu.fastplatform.mobile.farmer-app'

// Redis
export const REDIS_CONNECT_MAX_ATTEMPT: number = parseInt(process.env.REDIS_CONNECT_MAX_ATTEMPT || '10')
export const REDIS_HOST: string = process.env.REDIS_HOST || '0.0.0.0'
export const REDIS_PORT: number = parseInt(process.env.REDIS_PORT || '6379')
export const REDIS_AUTH_PASSWORD: string | undefined = process.env.REDIS_AUTH_PASSWORD
export const REDIS_KEY_EXPIRE_USER_DEVICE: number = parseInt(process.env.REDIS_KEY_EXPIRE_USER_DEVICE || '31536000') // default expire is one year

// Role base access control
export const HASURA_ADMIN_ROLES: string[] = process.env.HASURA_ADMIN_ROLES ? process.env.HASURA_ADMIN_ROLES.split(',') : ['admin', 'service']
