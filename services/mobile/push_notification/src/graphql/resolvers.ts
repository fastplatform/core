import { HASURA_ADMIN_ROLES } from '../config/env'
import {
  graphqlDeleteUser,
  graphqlDeleteUserDevice,
  graphqlGetUserDevices,
  graphqlRegisterUserDevice
} from '../controllers/user-device-controller'
import { SendNotificationRequestBody, graphqlSendNotification } from '../controllers/notification-controller'

type UserDeviceRequestBody = {
  user_id: string,
}
type UserDeviceRequestBodyWithToken = {
  user_id: string,
  device_token: string
}

function userAuthorizationMiddleware(args: any, context: {headers: any}) {
  if (HASURA_ADMIN_ROLES.includes(context.headers['x-hasura-role'])) {
    return {
      isAuthorized: true
    }
  }
  // TODO handle the possible "service" role
  if (!context.headers['x-hasura-user-id'] || context.headers['x-hasura-user-id'] !== args.user_id) {
    return {
      isAuthorized: false,
      data: {
        status: "FORBIDDEN",
        message: "",
        data: null
      }
    }
  } else {
    return {
      isAuthorized: true
    }
  }
}
function sendNotificationMiddleware(args: any, context: {headers: any}) {
  if (HASURA_ADMIN_ROLES.includes(context.headers['x-hasura-role'])) {
    return {
      isAuthorized: true
    }
  } else {
    return {
      isAuthorized: false,
      data: {
        status: 'FORBIDDEN',
        data: null,
        error: {
          name: 'AuthorizationError',
          message: 'Forbidden'
        },
        httpCode: 403
      }
    }
  }
}

export default {
  Query: {
    user_registered_devices: async (parent: any, args: UserDeviceRequestBody, context: {headers: any}) => {
      const authorization = userAuthorizationMiddleware(args, context)
      if (authorization.isAuthorized) {
        const res = await graphqlGetUserDevices(args)
        return res
      } else {
        return authorization.data
      }
    }
  },
  Mutation: {
    register_user_device: async (parent: any, args: UserDeviceRequestBodyWithToken, context: {headers: any}) => {
      const authorization = userAuthorizationMiddleware(args, context)
      if (authorization.isAuthorized) {
        const res = await graphqlRegisterUserDevice(args)
        return res
      } else {
        return authorization.data
      }
    },
    send_notification: async (parent: any, args: SendNotificationRequestBody, context: {headers: any}) => {
      const authorization = sendNotificationMiddleware(args, context)
      if (authorization.isAuthorized) {
        const res = await graphqlSendNotification(args)
        return res
      } else {
        return authorization.data
      }
    },
    unregister_user_all_devices: async (parent: any, args: UserDeviceRequestBody, context: {headers: any}) => {
      const authorization = userAuthorizationMiddleware(args, context)
      if (authorization.isAuthorized) {
        const res = await graphqlDeleteUser(args)
        return res
      } else {
        return authorization.data
      }
    },
    unregister_user_device: async (parent: any, args: UserDeviceRequestBodyWithToken, context: {headers: any}) => {
      const authorization = userAuthorizationMiddleware(args, context)
      if (authorization.isAuthorized) {
        const res = await graphqlDeleteUserDevice(args)
        return res
      } else {
        return authorization.data
      }
    }
    
  }
}