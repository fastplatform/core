# `core/mapproxy/server`: MapProxy raster tiles server

The [core/mapproxy/server](./) service is a [Mapproxy 1.13.0](https://mapproxy.org/) server whose role is to serve all the raster map layers under a unified projection. It is running behind a [gunicorn](https://gunicorn.org/) WSGI server.

[Mapproxy](https://mapproxy.org/) is an open source proxy for geospatial data. It caches, accelerates and transforms data from existing map services and serves any desktop or web GIS client.

## MapProxy server

The service exposes its configured layers under the XYZ tile scheme, as follows:

```http
https://map.beta.be-wal.fastplatform.eu/tiles/osm_EPSG3857/EPSG3857/13/4206/2766.png
```

This will download tile column `4206`, row `2766` and zoom level `13` from the `osm` (OpenStreetMap) layer defined on the MapProxy server.

## Configuration file

The complete configuration of the MapProxy server is defined in a single configuration file that is mounted in the service through a Kubernetes ConfigMap. This configuration file is generated dynamically by the [core/mapproxy/api](../api) service, every time the definition of the layers in the Adminstration Portal is edited (see [core/mapproxy](../) for more details).

An example of the configuration file is provided below. It contains the layers, grids and caches definitions for 2 layers:
- 1 TMS layer (OpenStreetMap)
- 1 WMS layer (SPW OrthoPhotos 2021)

```yaml
services:
  demo:
  tms:
    use_grid_names: false
    origin: nw

layers:
- name: osm_EPSG31370
  title: Open Street Map
  sources:
  - cache_std_reg_osm
- name: osm_EPSG3857
  title: Open Street Map
  sources:
  - cache_src_std_osm
- name: spw_orthophotos_2021_EPSG31370
  title: Orthophotos 2021
  sources:
  - cache_std_reg_spw_orthophotos_2021
- name: spw_orthophotos_2021_EPSG3857
  title: Orthophotos 2021
  sources:
  - cache_src_std_spw_orthophotos_2021
caches:
  cache_std_reg_osm:
    grids:
    - regional-grid
    sources:
    - cache_src_std_osm
    bulk_meta_tiles: true
    meta_size:
    - 2
    - 2
    cache:
      type: s3
      bucket_name: fastplatform-be-wal-public
      directory: cache/mapproxy-server/cache_std_reg_osm
  cache_src_std_osm:
    grids:
    - GLOBAL_WEBMERCATOR
    sources:
    - source_osm
    concurrent_tile_creators: 4
    cache:
      type: s3
      bucket_name: fastplatform-be-wal-public
      directory: cache/mapproxy-server/cache_src_std_osm
  cache_src_reg_osm:
    grids:
    - regional-grid
    sources:
    - source_osm
    concurrent_tile_creators: 4
    cache:
      type: s3
      bucket_name: fastplatform-be-wal-public
      directory: cache/mapproxy-server/cache_src_reg_osm
  cache_reg_std_osm:
    grids:
    - GLOBAL_WEBMERCATOR
    sources:
    - cache_src_reg_osm
    bulk_meta_tiles: true
    meta_size:
    - 2
    - 2
    cache:
      type: s3
      bucket_name: fastplatform-be-wal-public
      directory: cache/mapproxy-server/cache_reg_std_osm
  cache_std_reg_spw_orthophotos_2021:
    grids:
    - regional-grid
    sources:
    - cache_src_std_spw_orthophotos_2021
    bulk_meta_tiles: true
    meta_size:
    - 2
    - 2
    cache:
      type: s3
      bucket_name: fastplatform-be-wal-public
      directory: cache/mapproxy-server/cache_std_reg_spw_orthophotos_2021
  cache_src_std_spw_orthophotos_2021:
    grids:
    - GLOBAL_WEBMERCATOR
    sources:
    - source_spw_orthophotos_2021
    concurrent_tile_creators: 4
    cache:
      type: s3
      bucket_name: fastplatform-be-wal-public
      directory: cache/mapproxy-server/cache_src_std_spw_orthophotos_2021
  cache_src_reg_spw_orthophotos_2021:
    grids:
    - regional-grid
    sources:
    - source_spw_orthophotos_2021
    concurrent_tile_creators: 4
    cache:
      type: s3
      bucket_name: fastplatform-be-wal-public
      directory: cache/mapproxy-server/cache_src_reg_spw_orthophotos_2021
  cache_reg_std_spw_orthophotos_2021:
    grids:
    - GLOBAL_WEBMERCATOR
    sources:
    - cache_src_reg_spw_orthophotos_2021
    bulk_meta_tiles: true
    meta_size:
    - 2
    - 2
    cache:
      type: s3
      bucket_name: fastplatform-be-wal-public
      directory: cache/mapproxy-server/cache_reg_std_spw_orthophotos_2021
sources:
  source_osm:
    type: tile
    grid: GLOBAL_WEBMERCATOR
    url: https://a.tile.openstreetmap.org/%(z)s/%(x)s/%(y)s.png
  source_spw_orthophotos_2021:
    type: wms
    req:
      url: https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_2021/MapServer/WMSServer
      layers: '0'
    supported_srs:
    - EPSG:4326
    wms_opts:
      version: 1.3.0
grids:
  webmercator:
    base: GLOBAL_WEBMERCATOR
  regional-grid:
    srs: EPSG:31370
    bbox:
    - 14637.25
    - 22608.21
    - 291015.29
    - 246424.28
globals:
  cache:
    s3:
      endpoint_url: https://oss.eu-west-0.prod-cloud-ocb.orange-business.com
```

We invite you to explore the [MapProxy documentation](https://mapproxy.org/docs/1.13.0/configuration.html#mapproxy-yaml) for more details.

## Environment variables

- `AWS_ACCESS_KEY_ID`: Cache / S3 object storage access key
- `AWS_SECRET_ACCESS_KEY`: Cache / S3 object storage secret key
- `MAPPROXY_CONFIGURATION_FILE_PATH`: Path to the MapProxy configuration file

## Development

To run the service on your local machine, ensure you have the following dependencies installed:
- [Docker](https://www.docker.com/)
- [Python 3.7+](https://www.python.org/) with the `virtualenv` package installed
- The `make` utility

Create a virtual environment and activate it:
```bash
virtualenv .venv
source .venv/bin/activate
```

Install the Python dependencies:
```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt  # optional
```

Start the development server with auto-reload enabled:
```bash
make start
```

The server is then ready to receive queries at http://localhost:7020 and uses configuration file [app/mapproxy.yaml](./app/mapproxy.yaml)

### Management commands

The following management commands are available from the `Makefile`.

- MapProxy targets:
    - `make start`: Start the MapProxy server using configuration file [app/mapproxy.yaml](./app/mapproxy.yaml)

Some additional commands are available in the `Makefile`, we encourage you to read it (or to type `make help` to get the full list of commands).
