import os, unittest
import datetime

from app.utils.mapproxy_config_parser import MapproxyConfig

from app.settings import config


def load_example_conf():
    data = (config.APP_DIR / "test/mapproxy.yaml").read_text()
    return data


class TestMapproxyConfigParser(unittest.TestCase):
    def init(self):
        configuration = load_example_conf()
        return MapproxyConfig(configuration)

    def test_create(self):
        mapp_config = self.init()
        self.assertEqual(len(mapp_config.get_layers()), 2)

    def test_layer(self):
        mapp_config = self.init()
        layer = {"name": "test", "title": "test", "sources": ["test"]}
        self.assertRaises(ValueError, mapp_config.add_layer, layer)

        layer = {"name": "test", "title": "test", "sources": ["osm_wms"]}
        mapp_config.add_layer(layer)
        self.assertEqual(len(mapp_config.get_layers()), 3)

        layer = {"name": "test", "title": "test", "sources": ["test"]}
        self.assertRaises(ValueError, mapp_config.add_layer, layer)

        mapp_config.remove_layer("osm")
        self.assertEqual(len(mapp_config.get_layers()), 2)
        mapp_config.remove_layer("maamet")
        mapp_config.remove_layer("test")
        mapp_config.remove_layer("test")
        self.assertEqual(len(mapp_config.get_layers()), 0)

    def test_source(self):
        mapp_config = self.init()
        source_name = "test"
        source = {
            "type": "wms",
            "supported_srs": ["TEST"],
            "req": {"url": "test", "layers": "test"},
        }
        mapp_config.add_source(source_name, source)
        self.assertEqual(len(mapp_config.get_sources()), 3)

        source_name = "test"
        source = {"req": {"url": "test", "layers": "test"}}
        self.assertRaises(ValueError, mapp_config.add_source, source_name, source)

        source_name = "test"
        source = {"type": "wms", "req": {"layers": "test"}}
        self.assertRaises(ValueError, mapp_config.add_source, source_name, source)

        mapp_config.remove_source("test")
        self.assertEqual(len(mapp_config.get_sources()), 2)
        mapp_config.remove_source("osm_wms")
        mapp_config.remove_source("maamet_wms")
        mapp_config.remove_source("maamet_wms")
        self.assertEqual(len(mapp_config.get_sources()), 0)
