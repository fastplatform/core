import hashlib
from kubernetes import client, config as k8s_config
from app.settings import config


class ConfigMapManager:
    """
    Use this class to interact with k8s cluster to update config map
    """

    def __init__(self):
        """Init a ConfigMapManager with its namespace"""
        self.ns = config.MAPPROXY_K8S_NAMESPACE
        self.configmap_name = config.MAPPROXY_K8S_CONFIGMAP_NAME
        self.configmap_mapproxy_key = config.MAPPROXY_K8S_CONFIGMAP_KEY

        # If environment variable is set to True, uses incluster config. Else use kube_config.
        # Warning: For production, you need to use incluster.
        if config.MAPPROXY_K8S_USE_INCLUSTER_CONFIG:
            self.incluster = True
        else:
            self.incluster = False

    def _get_api(self):
        if self.incluster:
            k8s_config.load_incluster_config()
        else:
            k8s_config.load_kube_config()

        return client.CoreV1Api()

    def _build_config_map(self, params):
        # TODO
        return None

    def create_mapproxy_config(self):
        """
        Create a configmap with mapproxy default config
        """
        # TODO

    def get_mapproxy_config(self) -> str:
        """Get the mapproxy config from k8s configmap

        Returns:
            str: The mapproxy config (yaml)
        """
        ret = self._get_api().read_namespaced_config_map(
            name=self.configmap_name, namespace=self.ns
        )
        return ret.data[self.configmap_mapproxy_key]

    def patch_mapproxy_config(self, mapproxy_configuration: str):
        """Patch the configmap with new mapproxy configuyration

        Args:
            mapproxy_configuration (str): The mapproxy configuration to update (yaml formatted)

        Returns:
            dict: The k8s api response
        """
        # Get the current configmap from cluster
        ret = self._get_api().read_namespaced_config_map(
            name=self.configmap_name, namespace=self.ns
        )
        # Update the mapproxy config
        ret.data[self.configmap_mapproxy_key] = mapproxy_configuration
        # Patch the configmap
        resp = self._get_api().patch_namespaced_config_map(
            name=self.configmap_name, namespace=self.ns, body=ret
        )

        return resp

    def patch_mapproxy_pods(
        self,
        mapproxy_configuration: str,
        mapproxy_server_labels_selector: str,
    ):
        """Patch the mapproxy-server pods annotations with the checksum of the mapproxy.yaml file to force refreshing the mounted configmap

        Args:
            mapproxy_server_labels (str): The mapproxy-server labels to track pods
            mapproxy_configuration (str): The mapproxy-server mapproxy.yaml configuration

        Returns:
            dict: Number of pods that have benn patched
        """
        sum = hashlib.sha1(mapproxy_configuration.encode())

        patch = {
            "metadata": {
                "annotations": {"mapproxy-configurations-sha1": sum.hexdigest()}
            }
        }

        pods = self._get_api().list_namespaced_pod(
            namespace=self.ns,
            label_selector=mapproxy_server_labels_selector,
        )
        for pod in pods.items:
            self._get_api().patch_namespaced_pod(
                name=pod.metadata.name, namespace=self.ns, body=patch
            )

        return len(pods.items), sum
