import logging

from app.db.graphql_clients import fastplatform
from app.utils.mapproxy_config_parser import MapproxyConfig
from app.api.k8s_interface import ConfigMapManager
from app.settings import config
from gql import gql
from opentelemetry import trace


# Log
logger = logging.getLogger(__name__)

# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


def load_clean_mapproxy_configuration():
    """Load a clean mapproxy configuration from local yaml file

    Returns:
        MapproxyConfig: the clean mapproxy configuration
    """
    with tracer().start_as_current_span("load_initial_configuration") as span:
        configuration = (config.APP_DIR / "config/mapproxy.yaml").read_text()

    return MapproxyConfig(configuration)


async def get_base_layers():
    with tracer().start_as_current_span("get_base_layers") as span:
        query = (config.APP_DIR / "graphql/query_base_layers.graphql").read_text()
        extra_args = {
            "headers": {
                "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
            }
        }
        response = await fastplatform.execute(gql(query), {}, extra_args=extra_args)
        span.add_event(f"{len(response['map_base_layer'])} base layers found")
        return response["map_base_layer"]


async def get_overlays():
    with tracer().start_as_current_span("get_overlays") as span:
        query = (config.APP_DIR / "graphql/query_overlays.graphql").read_text()
        extra_args = {
            "headers": {
                "Authorization": f"Service {config.API_GATEWAY_SERVICE_KEY}",
            }
        }
        response = await fastplatform.execute(gql(query), {}, extra_args=extra_args)
        span.add_event(f"{len(response['map_overlay'])} overlays found")
        return response["map_overlay"]


async def init_config_map():
    """
    Init the configmap by getting layers from Hasura and apply it to system
    """
    # Get layers from Hasura
    base_layers = await get_base_layers()
    overlays = await get_overlays()

    # Generate mapproxy config and return yaml
    mapproxy_config = load_clean_mapproxy_configuration()

    # Get the yaml description of the mapproxy configuration
    with tracer().start_as_current_span("parse_layers_to_mapproxy_config") as span:
        mapproxy_config.parse_layers(base_layers, overlays)
        configmap = mapproxy_config.to_yaml()

    # Export configuration to k8s or to local file
    decoded_configmap = configmap.decode("utf-8")
    decoded_configmap = decoded_configmap.replace("' ", "'")

    if config.MAPPROXY_CONF_FROM_K8S:
        with tracer().start_as_current_span("export_mapproxy_config_to_k8s") as span:
            logger.info("Export MAPPROXY configuration to K8S")
            cmm = ConfigMapManager()
            resp = cmm.patch_mapproxy_config(decoded_configmap)
            logger.debug(resp)
            nb_patched_pods, sum = cmm.patch_mapproxy_pods(
                mapproxy_configuration=decoded_configmap,
                mapproxy_server_labels_selector=config.MAPPROXY_SERVER_LABELS_SELECTOR,
            )
            logger.debug(
                f"{nb_patched_pods} pod(s) annotated with mapproxy.yaml sha1='{sum}'"
            )
    else:
        with tracer().start_as_current_span(
            "export_mapproxy_config_to_local_file"
        ) as span:
            logger.info("Export MAPPROXY configuration to local file")

            text_file = open((config.APP_DIR / config.MAPPROXY_CONF_FILE_PATH), "w")
            text_file.write(decoded_configmap)
            text_file.close()

    return configmap
