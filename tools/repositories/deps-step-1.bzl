load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@build_bazel_rules_nodejs//:index.bzl", "node_repositories", "npm_install")
load("@distroless//package_manager:dpkg.bzl", "dpkg_list", "dpkg_src")
load("@io_bazel_rules_docker//container:container.bzl", "container_pull")
load("@io_bazel_rules_docker//nodejs:image.bzl", _nodejs_image_repos = "repositories")
load("@io_bazel_rules_docker//python3:image.bzl", _py_image_repos = "repositories")
load("@io_bazel_rules_docker//repositories:deps.bzl", container_deps = "deps")
load("@rules_python_external//:defs.bzl", "pip_install")

_py_build_file_content = """
exports_files(["python_bin"])
filegroup(
    name = "files",
    srcs = glob(["bazel_install/**"], exclude = ["**/* *"]),
    visibility = ["//visibility:public"],
)
"""

def transitive_deps():
    _nodejs_image_repos()
    _py_image_repos()

def docker_deps():
    container_deps()
    maybe(
        container_pull,
        name = "distroless_debug_image_base",
        registry = "gcr.io",
        repository = "distroless/base-debian10",
        digest = "sha256:faeef0829d9736cf18ea3113898634ced46542c9d4445cbe15af8a19dda9ce0c",
    )
    maybe(
        container_pull,
        name = "py3.7_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:025b77e95e701917434c478e7fd267f3d894db5ca74e5b2362fe37ebe63bbeb0",
    )
    maybe(
        container_pull,
        name = "py3.7_debug_image_base",
        registry = "gcr.io",
        repository = "distroless/python3-debian10",
        digest = "sha256:ebd73d8f4da293c9826e8646137a05260ecd1b7ee103cb1f62ebf010fda7c7f9",
    )
    maybe(
        container_pull,
        name = "hasura_graphql_engine_image_base",
        registry = "index.docker.io",
        repository = "hasura/graphql-engine",
        digest = "sha256:3db179db8cfeb2d6add0d6a726ca5feee6bcbbdafd8798270c87889f3f7aa8b9",
    )
    maybe(
        container_pull,
        name = "memcached_1.6.7_image_base",
        registry = "index.docker.io",
        repository = "bitnami/memcached",
        digest = "sha256:e0ab91aac3269a102f30ab9cf47898439268c97aaba0aedfce3479585b624a71", # debian
    )
    maybe(
        container_pull,
        name = "osgeo_gdal_image_base",
        registry = "registry.gitlab.com",
        repository = "fastplatform/ops/ci/osgeo-gdal",
        digest = "sha256:77b88c4c0e30a4aeb2c785051447e436597daee96be993edc81070c4b0ab7da7", # ubuntu-small-latest
    )
    maybe(
        container_pull,
        name = "pg_tileserv_image_base",
        registry = "index.docker.io",
        repository = "pramsey/pg_tileserv",
        digest = "sha256:c6fe609d5b1ba8040dd37abdc545aa666e528d39e68817c1a287105580fab8f8", 
    )
    maybe(
        container_pull,
        name = "nginx_1.19.1_image_base",
        registry = "index.docker.io",
        repository = "nginx",
        digest = "sha256:966f134cf5ddeb12a56ede0f40fff754c0c0a749182295125f01a83957391d84", # alpine
    )
    maybe(
        container_pull,
        name = "redis_5.0.8_image_base",
        registry = "index.docker.io",
        repository = "bitnami/redis",
        digest = "sha256:e987d0d88e6c5d08ba0541e22f25614454661b9b5f64a309ae139a12867862d8", # debian
    )

def dpkg_deps():
    dpkg_src(
        name = "debian10_snap",
        arch = "amd64",
        distro = "buster",
        sha256 = "f251129edc5e5b31dadd7bb252e5ce88b3fdbd76de672bc0bbcda4f667d5f47f",
        snapshot = "20200612T083553Z",
        url = "https://snapshot.debian.org/archive",
    )
    dpkg_src(
        name = "debian10_updates_snap",
        arch = "amd64",
        distro = "buster-updates",
        sha256 = "24b35fcd184d71f83c3f553a72e6636954552331adfbbc694f0f70bd33e1a2b4",
        snapshot = "20200612T083553Z",
        url = "https://snapshot.debian.org/archive",
    )
    dpkg_src(
        name = "debian10_security_snap",
        package_prefix = "https://snapshot.debian.org/archive/debian-security/20200612T105246Z/",
        packages_gz_url = "https://snapshot.debian.org/archive/debian-security/20200612T105246Z/dists/buster/updates/main/binary-amd64/Packages.gz",
        sha256 = "c0ae35609f2d445e73ca8d3c03dc843f5ddae50f474cee10e79c4c1284ce2a2d",
    )
    dpkg_list(
        name = "packages_debian10",
        packages = [
            # web/pdf-generator/pyppeteer
            "adwaita-icon-theme",
            "chromium",
            "chromium-common",
            "coreutils",
            "dbus-x11",
            "dconf-gsettings-backend",
            "dconf-service",
            "debconf",
            "dbus-user-session",
            "dpkg",
            "fontconfig",
            "fontconfig-config",
            "fonts-dejavu-core",
            "fonts-liberation",
            "gcc-8-base",
            "glib-networking",
            "glib-networking-common",
            "glib-networking-services",
            "gsettings-desktop-schemas",
            "gtk-update-icon-cache",
            "hicolor-icon-theme",
            "libacl1",
            "libaom0",
            "libasound2",
            "libasound2-data",
            "libasyncns0",
            "libatk-bridge2.0-0",
            "libatk1.0-0",
            "libatk1.0-data",
            "libatomic1",
            "libatspi2.0-0",
            "libattr1",
            "libavahi-client3",
            "libavahi-common-data",
            "libavahi-common3",
            "libavcodec58",
            "libavformat58",
            "libavutil56",
            "libblkid1",
            "libbluray2",
            "libbsd0",
            "libbz2-1.0",
            "libc6",
            "libcairo-gobject2",
            "libcairo2",
            "libcap2",
            "libchromaprint1",
            "libcodec2-0.8.1",
            "libcolord2",
            "libcom-err2",
            "libcroco3",
            "libcrystalhd3",
            "libcups2",
            "libdatrie1",
            "libdbus-1-3",
            "libdconf1",
            "libdrm-amdgpu1",
            "libdrm-common",
            "libdrm-intel1",
            "libdrm-nouveau2",
            "libdrm-radeon1",
            "libdrm2",
            "libedit2",
            "libelf1",
            "libepoxy0",
            "libevent-2.1-6",
            "libexpat1",
            "libffi6",
            "libflac8",
            "libfontconfig1",
            "libfontenc1",
            "libfreetype6",
            "libfribidi0",
            "libgbm1",
            "libgcc1",
            "libgcrypt20",
            "libgdk-pixbuf2.0-0",
            "libgdk-pixbuf2.0-common",
            "libgl1",
            "libgl1-mesa-dri",
            "libglapi-mesa",
            "libglib2.0-0",
            "libglvnd0",
            "libglx-mesa0",
            "libglx0",
            "libgme0",
            "libgmp10",
            "libgnutls30",
            "libgomp1",
            "libgpg-error0",
            "libgraphite2-3",
            "libgsm1",
            "libgssapi-krb5-2",
            "libgtk-3-0",
            "libgtk-3-common",
            "libharfbuzz0b",
            "libhogweed4",
            "libice6",
            "libicu63",
            "libidn2-0",
            "libjbig0",
            "libjpeg62-turbo",
            "libjson-glib-1.0-0",
            "libjson-glib-1.0-common",
            "libjsoncpp1",
            "libk5crypto3",
            "libkeyutils1",
            "libkrb5-3",
            "libkrb5support0",
            "liblcms2-2",
            "libllvm7",
            "liblz4-1",
            "liblzma5",
            "libminizip1",
            "libmount1",
            "libmp3lame0",
            "libmpg123-0",
            "libnettle6",
            "libnspr4",
            "libnss3",
            "libnuma1",
            "libogg0",
            "libopenjp2-7",
            "libopenmpt0",
            "libopus0",
            "libp11-kit0",
            "libpango-1.0-0",
            "libpangocairo-1.0-0",
            "libpangoft2-1.0-0",
            "libpci3",
            "libpciaccess0",
            "libpcre3",
            "libpixman-1-0",
            "libpng16-16",
            "libproxy1v5",
            "libpsl5",
            "libpulse0",
            "libre2-5",
            "librest-0.7-0",
            "librsvg2-2",
            "librsvg2-common",
            "libselinux1",
            "libsensors-config",
            "libsensors5",
            "libshine3",
            "libsm6",
            "libsnappy1v5",
            "libsndfile1",
            "libsoup-gnome2.4-1",
            "libsoup2.4-1",
            "libsoxr0",
            "libspeex1",
            "libsqlite3-0",
            "libssh-gcrypt-4",
            "libssl1.1",
            "libstdc++6",
            "libswresample3",
            "libsystemd0",
            "libtasn1-6",
            "libthai-data",
            "libthai0",
            "libtheora0",
            "libtiff5",
            "libtinfo6",
            "libtwolame0",
            "libudev1",
            "libunistring2",
            "libuuid1",
            "libva-drm2",
            "libva-x11-2",
            "libva2",
            "libvdpau1",
            "libvorbis0a",
            "libvorbisenc2",
            "libvorbisfile3",
            "libvpx5",
            "libwavpack1",
            "libwayland-client0",
            "libwayland-cursor0",
            "libwayland-egl1",
            "libwayland-server0",
            "libwebp6",
            "libwebpdemux2",
            "libwebpmux3",
            "libwrap0",
            "libx11-6",
            "libx11-data",
            "libx11-xcb1",
            "libx264-155",
            "libx265-165",
            "libxau6",
            "libxaw7",
            "libxcb-dri2-0",
            "libxcb-dri3-0",
            "libxcb-glx0",
            "libxcb-present0",
            "libxcb-render0",
            "libxcb-shape0",
            "libxcb-shm0",
            "libxcb-sync1",
            "libxcb1",
            "libxcomposite1",
            "libxcursor1",
            "libxdamage1",
            "libxdmcp6",
            "libxext6",
            "libxfixes3",
            "libxft2",
            "libxi6",
            "libxinerama1",
            "libxkbcommon0",
            "libxml2",
            "libxmu6",
            "libxmuu1",
            "libxpm4",
            "libxrandr2",
            "libxrender1",
            "libxshmfence1",
            "libxslt1.1",
            "libxss1",
            "libxt6",
            "libxtst6",
            "libxv1",
            "libxvidcore4",
            "libxxf86dga1",
            "libxxf86vm1",
            "libzstd1",
            "libzvbi-common",
            "libzvbi0",
            "lsb-base",
            "perl-base",
            "sensible-utils",
            "shared-mime-info",
            "tar",
            "ttf-bitstream-vera",
            "ucf",
            "x11-common",
            "x11-utils",
            "xdg-utils",
            "xkb-data",
            "zlib1g",

            #curl
            "curl",
            # "libc6",
            # "libgcc1",
            # "gcc-8-base",
            "libcurl4",
            # "libcom-err2",
            # "libgssapi-krb5-2",
            # "libk5crypto3",
            # "libkeyutils1",
            # "libkrb5support0",
            # "libkrb5-3",
            # "libssl1.1",
            # "debconf",
            # "perl-base",
            # "dpkg",
            # "tar",
            # "libacl1",
            # "libattr1",
            # "libselinux1",
            # "libpcre3",
            # "libbz2-1.0",
            # "liblzma5",
            # "zlib1g",
            # "libidn2-0",
            # "libunistring2",
            "libldap-2.4-2",
            # "libgnutls30",
            # "libgmp10",
            # "libhogweed4",
            # "libnettle6",
            # "libp11-kit0",
            # "libffi6",
            # "libtasn1-6",
            "libldap-common",
            "libsasl2-2",
            "libsasl2-modules-db",
            "libdb5.3",
            "libnghttp2-14",
            # "libpsl5",
            "librtmp1",
            "libssh2-1",
            # "libgcrypt20",
            # "libgpg-error0",
        ],
        # Takes the first package found: security updates should go first
        sources = [
            "@debian10_security_snap//file:Packages.json",
            "@debian10_updates_snap//file:Packages.json",
            "@debian10_snap//file:Packages.json",
        ],
    )

def python_deps():
    # Special logic for building python interpreter with OpenSSL from homebrew.
    # See https://devguide.python.org/setup/#macos-and-os-x
    _py_configure = """
    if [[ "$OSTYPE" == "darwin"* ]]; then
        ./configure --prefix=$(pwd)/bazel_install --with-openssl=$(brew --prefix openssl)
    else
        ./configure --prefix=$(pwd)/bazel_install
    fi
    """
    maybe(
        http_archive,
        name = "py37_interpreter",
        urls = ["https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tgz"],
        sha256 = "39b018bc7d8a165e59aa827d9ae45c45901739b0bbb13721e4f973f3521c166a",
        strip_prefix = "Python-3.7.9",
        patch_cmds = [
            "mkdir $(pwd)/bazel_install",
            _py_configure,
            "make",
            "make install",
            "ln -s bazel_install/bin/python3 python_bin",
        ],
        build_file_content = _py_build_file_content,
    )
    maybe(
        pip_install,
        name = "event_farm_pip",
        requirements = "//services/event/farm:requirements.txt",
    )
    maybe(
        pip_install,
        name = "event_farm_pip_dev",
        requirements = "//services/event/farm:requirements-dev.txt",
    )
    maybe(
        pip_install,
        name = "event_messaging_pip",
        requirements = "//services/event/messaging:requirements.txt",
    )
    maybe(
        pip_install,
        name = "mapproxy_api_pip",
        requirements = "//services/mapproxy/api:requirements.txt",
    )
    maybe(
        pip_install,
        name = "mapproxy_api_pip_dev",
        requirements = "//services/mapproxy/api:requirements-dev.txt",
    )
    maybe(
        pip_install,
        name = "mapproxy_server_pip",
        requirements = "//services/mapproxy/server:requirements.txt",
    )
    maybe(
        pip_install,
        name = "web_backend_pip",
        requirements = "//services/web/backend:requirements.txt",
    )
    # TODO: Remove this workaround to fetch pip packages for Python 3.8
    #       to match the one of osgeo_gdal_image_base
    maybe(
        pip_install,
        name = "web_backend_pip_image",
        python_interpreter = "python3.8", # Can't use a Python toolchain in a repository rule
        requirements = "//services/web/backend:requirements.txt",
    )
    maybe(
        pip_install,
        name = "web_backend_pip_dev",
        requirements = "//services/web/backend:requirements-dev.txt",
    )
    maybe(
        pip_install,
        name = "web_pdf_generator_pip",
        requirements = "//services/web/pdf_generator:requirements.txt",
    )
    maybe(
        pip_install,
        name = "web_pdf_generator_pip_dev",
        requirements = "//services/web/pdf_generator:requirements-dev.txt",
    )
    
def npm_deps():
    node_repositories(
        node_version = "12.18.3",
    )
    maybe(
        npm_install,
        name = "mobile_push_notification_npm",
        package_json = "//services/mobile/push_notification:package.json",
        package_lock_json = "//services/mobile/push_notification:package-lock.json",
    )

def deps_step_1():
    transitive_deps()
    docker_deps()
    dpkg_deps()
    python_deps()
    npm_deps()
