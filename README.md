# `core`: FaST Platform core services

Core module that implements the common backend services that each region inherits from.

This repository includes the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

## Services

### API Gateway

- [core/api-gateway](services/api_gateway)

### Event handlers

- [core/event/farm](services/event/farm)
- [core/event/messaging](services/event/messaging)

### Mobile services & apps

- [core/mobile/push-notification](services/mobile/push_notification)

### Web services

- [core/mapproxy](services/mapproxy)
- [core/web/backend](services/web/backend)
- [core/web/media](services/web/media)
- [core/web/pdf-generator](services/web/pdf_generator)
- [core/web/vector-tiles](services/web/vector_tiles)
